package utils;

public class Helper {
	public enum SearchOptions { FRIENDS, USERS, QUIZZES; }
	
	public static String banner() {
		return 
		"<div id=\"header\"><a href=\"home.jsp\" id=\"logo\">Quizzle</a>"
		+ "<span id=\"nav\"><a href=\"home.jsp\">Home</a> | <a href=\"quizzes.jsp\">Quizzes</a> | "
		+ "<a href=\"inbox.jsp\">Inbox</a> | <a href=\"index.jsp?logout=true\">Logout</a></span>"
		+ "</div>";
	}
	
	public static String searchBar() {
		String s = 
		"<div id=\"search\" align=\"right\">" +
		"<form action=\"SearchServlet\" method=\"get\">" +
		"<p>" +
		"<select name=\"searchType\" id=\"searchType\" >" +
		"<option value=\"" + SearchOptions.FRIENDS.toString() + "\">Friends</option>" +
		"<option value=\"" + SearchOptions.USERS.toString() +"\">Users</option>" +
		"<option value=\"" + SearchOptions.QUIZZES.toString() +"\">Quizzes</option>" +
		"</select>" +
		
		"<input type=\"text\" name=\"searchString\" id=\"searchString\"/>" + 
		
		"<noscript>" +
		"	<input type=\"submit\" value=\"Search\" />" +
		"</noscript>" +
		
		"</p>" +
		
		"<script>" +
		"document.writeln(\"<input type=\"button\" value=\"Search\" onclick=\"validateSearchString(this.form);\" />\");" +
		"</script>" +
		
		"</form>" +
		"</div>";
		
		return s;
		
		/*
		"<div id=\"search\" align=\"right\">" +
		"<form action=\"SearchServlet\" method=\"get\">" +
		"<p><select name=\"searchType\">" +
		"<option value=\"" + SearchOptions.FRIENDS.toString() + "\">Friends</option>" +
		"<option value=\"" + SearchOptions.USERS.toString() +"\">Users</option>" +
		"<option value=\"" + SearchOptions.QUIZZES.toString() +"\">Quizzes</option>" +
		"</select>" +
		"<input type=\"text\" name=\"searchString\" id=\"searchString\"/>" +
		"<input type=\"submit\" value=\"Search\" /></p>" +
		"</form>" +
		"</div>";
		*/
	}
}
