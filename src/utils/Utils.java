package utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

import javax.servlet.http.Cookie;

public class Utils {
	private static final TimeZone tz;
	private static final DateFormat dateFormat;
	
	static {
        tz = TimeZone.getTimeZone("America/Los_Angeles");
        dateFormat = new SimpleDateFormat("MM/dd/yy");
        dateFormat.setTimeZone(tz);
    }
	
	public static TimeZone getTimeZone() {
	        return tz;
    }

    public static DateFormat getDateFormat() {
        return dateFormat;
    }
	   
    public static String getCookieValue(Cookie[] cookies, String cookieName) {
    	String cookieValue = null;
    	Cookie cookie;
    	if(cookies != null) {
    		for (int i=0; i<cookies.length; i++) {
    			cookie = cookies[i];
    			if(cookieName.equals(cookie.getName())) {
    				cookieValue = cookie.getValue();
    			}
    		}
    	}
    	return cookieValue;
    }
	    
	/**
	 * Method to check string is an Integer
	 * 
	 * @param s	Input string
	 * @return True is string is a number, False if not.
	 */
	public static boolean isIntNumber(String s) {
		try {
			Integer.parseInt(s);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	public static boolean isLongNumber(String s) {
		try {
			Long.parseLong(s);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	/**
	 * Method to convert string representation of a number into a number
	 * @param  s Input string
	 * @return int value representing the value of string.  Returns 0 if conversion fails
	 */
	public static int toInt(String s) {
		try {
			int number = Integer.parseInt(s);	//valid integer (neg, zero, pos)
			return number;
		} catch (IllegalArgumentException iae) {	//String could not be parsed into number
			return 0;
		} 
	}

	public static Long toLong(String s) {
		try {
			Long number = Long.parseLong(s);	//valid integer (neg, zero, pos)
			return number;
		} catch (IllegalArgumentException iae) {	//String could not be parsed into number
			return null;
		} 
	}
	
}
