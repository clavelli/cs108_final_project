package dao;

import java.util.List;

/**
 * Object store response status and list of data objects from database SELECT 
 *
 * @param <T>	Generic data object to store in list
 */
public class SelectListResponse<T> extends DBResponse {
	private List<T> dtoList;	//List of objects
	
	public SelectListResponse(List<T> dtoList, Status status, Cause cause, String message) {
		super(status, cause, message);
		this.dtoList = dtoList;
	}
	
	public List<T> getDTOList() {
		return dtoList;
	}
}
