package dao;

/**
 * Object store response status and single data object from database SELECT 
 *
 * @param <T>	Generic data object
 */
public class SelectResponse<T> extends DBResponse {
	private T dto;
	
	public SelectResponse(T dto, Status status, Cause cause, String message) {
		super(status, cause, message);
		this.dto = dto;
	}
	
	public T getDTO() {
		return dto;
	}
}
