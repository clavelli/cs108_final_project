package dao;

/**
 * Object store response status database CREATE 
 *
 */
public class InsertResponse<T> extends DBResponse {
	private T dto;		//Holds inserted object.  Used by application
	
	public InsertResponse(T dto, Status status, Cause cause, String message) {
		super(status, cause, message);
		this.dto = dto;
	}

	public T getDTO() {
		return dto;
	}	
}
