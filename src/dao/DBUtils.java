package dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;

import utils.Utils;


public class DBUtils {

	
	/**
	 * Close a JDBC connection
	 * @return true if connection successfully closed, false otherwise
	 */
	public static boolean closeConnection(Connection connection) {
		if (connection != null) {
			try {
				connection.close();
				return true;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return false;
	}
	
	/**
	 * Close a JDBC ResultSet
	 * @param rs ResultSet object to close
	 * @return true if successfully closed, false otherwise
	 */
	public static boolean closeResultSet(ResultSet rs) {
		if (rs != null) {
			try {
				rs.close();
				return true;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return false;
	}
	
	/**
	 * Close a JDBC SQL Statement
	 * @param stmt Statement object to close
	 * @return true if successfully closed, false otherwise
	 */
	public static boolean closeStatement(Statement stmt) {
		if (stmt != null) {
			try {
				stmt.close();
				return true;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return false;
	}
	
	/** 
     *  Retrieves the value of a the given field in the current row
     *  of the ResultSet object. If the returned value is null or it is
     *  an empty string, the defaultValue (third argument) is returned.
     *
	 * @param rs ResultSet object to retrieve field value from
	 * @param fieldName DB field name to retrieve value from
	 * @param defaultValue default value if DB field is not populated
	 * @return String value of DB field
	 */
    public static String getString(ResultSet rs, String fieldName, String defaultValue) throws SQLException {
        String value = rs.getString(fieldName);
        if (value == null || value.length() < 1) {
        	return defaultValue;
        }
        return value.trim();
    }
    
    /** 
     *  Retrieves the Integer value of a the given field in the current row
     *  of the ResultSet object. If the returned value is null, the defaultValue 
     *  (third argument) is returned.
     *
	 * @param rs ResultSet object to retrieve field value from
	 * @param fieldName DB field name to retrieve value from
	 * @param defaultValue default value if DB field is not populated
	 * @return Integer value of DB field
	 */
    public static Integer getInteger(ResultSet rs, String fieldName, Integer defaultValue) throws SQLException {
        Integer value = (Integer)rs.getObject(fieldName);
        if (value == null) {  
        	return defaultValue;
        }
        return value;
    }    

    /** 
     *  Retrieves the Double value of a the given field in the current row
     *  of the ResultSet object. If the returned value is null, the defaultValue 
     *  (third argument) is returned.
     *
	 * @param rs ResultSet object to retrieve field value from
	 * @param fieldName DB field name to retrieve value from
	 * @param defaultValue default value if DB field is not populated
	 * @return Double value of DB field
	 */
    public static Double getDouble(ResultSet rs, String fieldName, Double defaultValue) throws SQLException {
        Object value = rs.getObject(fieldName);
        if (value == null) {  
        	return defaultValue;
        } else if ( value instanceof BigDecimal ) {
        	return new Double(((BigDecimal)value).floatValue());
        } else if ( value instanceof Float ) {
        	return new Double((Float)value);
        } else if ( value instanceof Double ) {
        	return (Double)value;
        } else {
        	throw new SQLException("unknown data type");
        }
    }        
    
    
    public static Boolean getBoolean(ResultSet rs, String fieldName, Boolean defaultValue) throws SQLException {
        Boolean value = (Boolean)rs.getObject(fieldName);
        if (value == null) {  
        	return defaultValue;
        }
        return value;
    } 
    
    
    public static Long getLong(ResultSet rs, String fieldName, Long defaultValue) throws SQLException {
    	Long value = (Long)rs.getObject(fieldName);
        if (value == null) {  
        	return defaultValue;
        }
        return value;
    }    
    
    
    /**
	 * Set a Double value or null for a PreparedStatement
	 * @param cstat target PreparedStatement
	 * @param paramIndex parameter position
	 * @param value The Double value to set
	 */
	public static void setDoubleOrNull(PreparedStatement pstmt, int paramIndex, Double value) throws SQLException {
		if ( value == null ) {
			pstmt.setNull(paramIndex, Types.DOUBLE);
		} else {
			pstmt.setDouble(paramIndex, value);
		}
	}	
	
	/**
	 * Set a Float value or null for a PreparedStatement
	 * @param cstat target PreparedStatement
	 * @param paramIndex parameter position
	 * @param value The Float value to set
	 */
	public static void setFloatOrNull(PreparedStatement pstmt, int paramIndex, Float value) throws SQLException {
		if ( value == null ) {
			pstmt.setNull(paramIndex, Types.FLOAT);
		} else {
			pstmt.setFloat(paramIndex, value);
		}
	}	
	
	/**
	 * Set an Integer value or null for a PreparedStatement
	 * @param cstat target PreparedStatement
	 * @param paramIndex parameter position
	 * @param value The Integer value to set
	 */
	public static void setIntegerOrNull(PreparedStatement pstmt, int paramIndex, Integer value) throws SQLException {
		if ( value == null ) {
			pstmt.setNull(paramIndex, Types.INTEGER);
		} else {
			pstmt.setInt(paramIndex, value);
		}
	}	
	
	public static void setStringToLong(PreparedStatement pstmt, int paramIndex, String value) throws SQLException {
		if(Utils.isLongNumber(value)) {
			pstmt.setLong(paramIndex, Utils.toLong(value));
		} else {
			throw new SQLException("String id value not a valid long number representation");
		}
	}	
	
	/**
	 * Set a "Double converted to Float" value or null for a PreparedStatement
	 * @param cstat target PreparedStatement
	 * @param paramIndex parameter position
	 * @param value The "Double converted to Float" value to set
	 */
	public static void setDoubleToFloatOrNull(PreparedStatement pstmt, int paramIndex, Double value) throws SQLException {
		if ( value == null ) {
			pstmt.setNull(paramIndex, Types.FLOAT);
		} else {
			pstmt.setFloat(paramIndex, value.floatValue());
		}
	}		
	
	public static void setNullToString(PreparedStatement pstmt, int paramIndex, String value, String defaultValue) throws SQLException{
		if ( value == null ) {
			pstmt.setString(paramIndex, defaultValue);
		} else {
			pstmt.setString(paramIndex, value);
		}
	}
	
	
	public static String getLongAutoGeneratedKey(Statement stmt) throws SQLException {
		ResultSet rs = null;
		try {
			rs = stmt.getGeneratedKeys(); 
			if(rs.next()) {
				return String.valueOf(rs.getLong(1));
			}
			return null;
		} finally {
			DBUtils.closeResultSet(rs);
		}
	}
}
