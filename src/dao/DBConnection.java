package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Class to get database connection
 *
 */
public class DBConnection {
	private static final String MYSQL_USERNAME = "ccs108anhdang";
	private static final String MYSQL_PASSWORD = "phahvana";
	private static final String MYSQL_DATABASE_SERVER = "mysql-user.stanford.edu";
	private static final String MYSQL_DATABASE_NAME = "c_cs108_anhdang";
	
	private static final boolean IS_OFFLINE = false;
	
	private static Connection connection;

	/**
	 * Returns database connection.  
	 * There could only be one connection.  But if that connection is closed (null)
	 *  then it would get another connection and assign to the class instance variable
	 */
	
	public static synchronized Connection getConnection() {
		try {
			if ((connection == null) || (connection.isClosed())){	//Get new connection
				Class.forName("com.mysql.jdbc.Driver");
				
				if(IS_OFFLINE) {	//for testing when offline
					String url = "jdbc:mysql://" + "localhost:3306" + "/" + "c_cs108_anhdang";
					connection = DriverManager.getConnection(url, "root", "root");
				} else {
					String url = "jdbc:mysql://" + MYSQL_DATABASE_SERVER + "/" + MYSQL_DATABASE_NAME;
					connection = DriverManager.getConnection(url, MYSQL_USERNAME, MYSQL_PASSWORD);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			System.err.println("CS108 student: Update the MySQL constants to correct values!");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			System.err.println("CS108 student: Add the MySQL jar file to your build path!");
		}
		return connection;
	}
	
	
	/**
	 * Returns database connection.  This implementation allows more than one 
	 *  connections to be created (unlike in class MyDB.java, where there was
	 *  a static instance that was initialized with a static block).  So there
	 *  could only be one connection.  So you couldn't close that until your
	 *  application was done.  With this method, you need to ensure that you
	 *  close the connection when done.
	 * @return
	 */
	/*
	public static synchronized Connection getConnection() {
		Connection connection = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			String url = "jdbc:mysql://" + MYSQL_DATABASE_SERVER + "/" + MYSQL_DATABASE_NAME;
			connection = DriverManager.getConnection(url, MYSQL_USERNAME, MYSQL_PASSWORD);
		} catch (SQLException e) {
			e.printStackTrace();
			System.err.println("CS108 student: Update the MySQL constants to correct values!");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			System.err.println("CS108 student: Add the MySQL jar file to your build path!");
		}
		return connection;
	}
	*/
	

	
}
