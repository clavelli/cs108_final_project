package dao;


/**
 * Object store response status database DELETE 
 *
 */
public class DeleteResponse extends DBResponse  {
	public DeleteResponse(Status status, Cause cause, String message) {
		super(status, cause, message);
	}
}
