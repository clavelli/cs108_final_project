package dao;

public class PreviousIds {
	private String prevQuizId;
	private String prevQuestionId;
	private String prevAnswerId;
	
	public PreviousIds() {
		setPrevQuizId(null);
		setPrevQuestionId(null);
		setPrevAnswerId(null);
	}

	public void setPrevQuestionId(String prevQuestionId) {
		this.prevQuestionId = prevQuestionId;
	}

	public String getPrevQuestionId() {
		return prevQuestionId;
	}

	public void setPrevAnswerId(String prevAnswerId) {
		this.prevAnswerId = prevAnswerId;
	}

	public String getPrevAnswerId() {
		return prevAnswerId;
	}

	public void setPrevQuizId(String prevQuizId) {
		this.prevQuizId = prevQuizId;
	}

	public String getPrevQuizId() {
		return prevQuizId;
	}
	
	
}
