package dao.business;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import model.Creation;
import model.Question;
import model.Quiz;
import model.Result;
import utils.Utils;
import dao.DBConnection;
import dao.DBResponse;
import dao.DBUtils;
import dao.DeleteResponse;
import dao.InsertResponse;
import dao.PreviousIds;
import dao.SelectListResponse;
import dao.SelectResponse;
import dao.UpdateResponse;

public class QuizDB {
	
	// **************
	// Get methods
	// **************
	
	public static SelectListResponse<Quiz> searchQuizzes(String searchString) {
		ResultSet rs = null;
		PreparedStatement ps = null;
		Connection connection = null;
		
		String sql = "SELECT quiz.quizId AS quizId, description, title, " +
					"	hits, likes, quiz.isRandom, isSinglePage, isImmediateCorrect, " +
					" 	activity.date,	activity.username as creatorUsername " +
					" FROM quiz " +
					" JOIN creation" +
					"	ON quiz.quizId = creation.quizId " +
					" JOIN activity" +
					"	ON creation.activityId = activity.activityId" +
					" WHERE LOWER(quiz.title) LIKE ? ";// +
					//"	OR quiz.description LIKE ?";
		connection = DBConnection.getConnection();
		
		try {
			ps = connection.prepareStatement(sql);
			ps.setString(1, "%"+searchString+"%");
			//ps.setString(2, "%"+searchString+"%");
			
			rs = ps.executeQuery();

			//Assemble 
			List<Quiz> quizzes = new ArrayList<Quiz>();
			int errorCount = assembleQuizList(rs, quizzes);
			
			if(errorCount == 0) {
				return new SelectListResponse<Quiz>
					(quizzes, DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
			} else if (errorCount == -1){
				return new SelectListResponse<Quiz>
					(null, DBResponse.Status.FAILURE, DBResponse.Cause.NO_RESULT_FOUND, null);
			} else	{	//For returning lists: errorCount > 0, only some objects were able to be composed
				return new SelectListResponse<Quiz>
					(quizzes, DBResponse.Status.PARTIAL_SUCCESS, DBResponse.Cause.PARTIAL_DATA_ACCESS_FAILURE, null);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			DBUtils.closeResultSet(rs);
			DBUtils.closeStatement(ps);
			DBUtils.closeConnection(connection);
		}	
		return new SelectListResponse<Quiz>
			(null, DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, null);
	}
	
	
	/* done */
	public static SelectResponse<Quiz> getQuiz(String quizId) {
		ResultSet rs = null;
		PreparedStatement ps = null;
		Connection connection = null;
		
		String message = null;
		
		//Query string
		String sql ="SELECT quiz.quizId AS quizId, description, title, " +
					"	hits, likes, quiz.isRandom, isSinglePage, isImmediateCorrect, " +
					" 	activity.date,	activity.username as creatorUsername, " +
					"	question.questionId AS questionId, questionType, questionNumber, picUrl, " +
					"	questionresponse.question, orderAnswers, questionresponse.numAnswers, " +
					"	partOne, partTwo, fillinblank.numAnswers, " +
					"	multiplechoice.numAnswers, isCheckAll, multiplechoice.isRandom, multiplechoice.question," +
					"	choiceId, isAnswer, choiceNumber, choice.text," +
					"	answer.answerId AS answerId, answerNumber, answervariation.text" +
					" FROM quiz " +
					" JOIN question " +
					"	ON quiz.quizId = question.quizId" +
					" LEFT OUTER JOIN questionresponse" +
					"	ON question.questionId = questionresponse.questionId" +
					" LEFT OUTER JOIN multiplechoice" +
					"	ON question.questionId = multiplechoice.questionId" +
					" LEFT OUTER JOIN fillinblank" +
					"	ON question.questionId = fillinblank.questionId" +
					" LEFT OUTER JOIN choice" +
					"	ON question.questionId = choice.questionId" +
					" LEFT OUTER JOIN answer" +
					"	ON question.questionId = answer.questionId" +
					" LEFT OUTER JOIN answervariation" +
					"	ON answer.answerId = answervariation.answerId" +
					" LEFT OUTER JOIN creation" +
					"	ON quiz.quizId = creation.quizId " +
					" LEFT OUTER JOIN activity" +
					"	ON creation.activityId = activity.activityId" +
					" WHERE question.quizId = ?" +
					" ORDER BY question.questionId ASC, " +
					"			answer.answerId ASC," +
					"			choice.choiceId ASC";	
		connection = DBConnection.getConnection();
		
		long longQuizId;
		if(Utils.isLongNumber(quizId)) {
			longQuizId = Utils.toLong(quizId);
		} else {
			message = "QuizId not a valid integer";
			return new SelectResponse<Quiz>
				(null, DBResponse.Status.FAILURE, DBResponse.Cause.INVALID_QUERY_PARAMETER, message);
		}
		
		try {
			ps = connection.prepareStatement(sql);
			ps.setLong(1, longQuizId);		
	
			rs = ps.executeQuery();

			//Assemble 
			Quiz quiz = new Quiz();
			PreviousIds ids = new PreviousIds();	//Memory for last id encountered
			int errorCount = -1;
			while (rs.next()) {	
				if(assembleQuiz(rs, ids, quiz)) {
					if (errorCount == -1) errorCount = 0;	//Only set the first time
				} else {
					if (errorCount == -1) errorCount = 0;  //Only set the first time
					errorCount++;
				}
			} 
			
			if(errorCount == 0) {
				return new SelectResponse<Quiz>
					(quiz, DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
			} else if (errorCount == -1){
				return new SelectResponse<Quiz>
					(null, DBResponse.Status.FAILURE, DBResponse.Cause.NO_RESULT_FOUND, null);
			} else	{	//For returning lists: errorCount > 0, only some objects were able to be composed
				return new SelectResponse<Quiz>
					(null, DBResponse.Status.FAILURE, DBResponse.Cause.PARTIAL_DATA_ACCESS_FAILURE, null);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			DBUtils.closeResultSet(rs);
			DBUtils.closeStatement(ps);
			DBUtils.closeConnection(connection);
		}	
		return new SelectResponse<Quiz>
			(null, DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, null);
	}
	
	/**
	 * Applicable for assembling single quiz only.  Use different method for list
	 * @param rs
	 * @param ids
	 * @param quiz
	 * @return
	 */
	/* done */
	private static boolean assembleQuiz(ResultSet rs, PreviousIds ids, Quiz quiz) {
		try {
			String id = (DBUtils.getLong(rs, "quizId", -1l)).toString();
			
			if (id.equals(ids.getPrevQuizId())) {	//same object record as last record
				//Get Questions
				if(!getQuestions(rs, ids, quiz)) return false;
			} else {  //New quiz.  For this method, it only happens once
				ids.setPrevQuizId(id);
				
				//Get common fields
				if(!setQuizFields(rs, quiz)) return false;
				
				//Get Questions
				if(!getQuestions(rs, ids, quiz)) return false;
			}
			return true;
		} catch (SQLException se) {
			se.printStackTrace();
		}
		return false;
	}

	/* done */
	private static boolean setQuizFields(ResultSet rs, Quiz quiz) {
		try {
			quiz.setQuizID(DBUtils.getLong(rs, "quizId", -1l).toString());
			quiz.setCreateDate(new Timestamp(rs.getTimestamp("activity.date").getTime()));
			quiz.setCreatorName(DBUtils.getString(rs, "creatorUsername", null));
			quiz.setDescription(DBUtils.getString(rs, "description", null));
			quiz.setTitle(DBUtils.getString(rs, "title", null));
			quiz.setNumHits(DBUtils.getLong(rs, "hits", 0l));
			quiz.setNumLikes(DBUtils.getLong(rs, "likes", 0l));
			quiz.setRandom(DBUtils.getBoolean(rs, "isRandom", false));
			quiz.setSinglePage(DBUtils.getBoolean(rs, "isSinglePage", true));
			quiz.setImmCorrection(DBUtils.getBoolean(rs, "isImmediateCorrect", false));
			return true;
		} catch (SQLException se) {
			se.printStackTrace();
		}
		return false;
	}
	
	/* done */
	private static boolean getQuestions(ResultSet rs, PreviousIds ids, Quiz quiz) {
		LinkedList<Question> questions;
		
		if(quiz.getQuestions() instanceof LinkedList<?>) {
			questions = (LinkedList<Question>)quiz.getQuestions();
			
			//Assemble questions
			return QuestionDB.assembleQuestion(rs, ids, questions);
		} 
		return false;
	}
	
	/* done */
	public static SelectListResponse<Quiz> getRecentQuizzes(int minutes, int count) {
		ResultSet rs = null;
		PreparedStatement ps = null;
		Connection connection = null;
		String sql = "SELECT quiz.quizId AS quizId, activity.username as creatorUsername, " + 
					" 		description, title, hits, likes, " +
					" 		quiz.isRandom, isSinglePage, isImmediateCorrect, " +
					" 		activity.date, activity.username" +
					" FROM quiz" +
					" JOIN creation" +
					"	ON quiz.quizId = creation.quizId " +
					" JOIN activity" +
					"	ON creation.activityId = activity.activityId " +
					" WHERE  " +
					"		activity.date BETWEEN DATE_SUB(CURRENT_TIMESTAMP(), INTERVAL ? MINUTE) AND CURRENT_TIMESTAMP()" +
					" ORDER BY activity.date DESC" +
					" LIMIT ? ";
		connection = DBConnection.getConnection();
		try {
			ps = connection.prepareStatement(sql);
			ps.setInt(1, minutes);	
			ps.setInt(2, count);		
			
			rs = ps.executeQuery();

			//Assemble 
			List<Quiz> quizzes = new ArrayList<Quiz>();
			int errorCount = assembleQuizList(rs, quizzes);
			
			if(errorCount == 0) {
				return new SelectListResponse<Quiz>
					(quizzes, DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
			} else if (errorCount == -1){
				return new SelectListResponse<Quiz>
					(null, DBResponse.Status.FAILURE, DBResponse.Cause.NO_RESULT_FOUND, null);
			} else	{	//For returning lists: errorCount > 0, only some objects were able to be composed
				return new SelectListResponse<Quiz>
					(quizzes, DBResponse.Status.PARTIAL_SUCCESS, DBResponse.Cause.PARTIAL_DATA_ACCESS_FAILURE, null);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			DBUtils.closeResultSet(rs);
			DBUtils.closeStatement(ps);
			DBUtils.closeConnection(connection);
		}	
		return new SelectListResponse<Quiz>
			(null, DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, null);
	}
	
	private static int assembleQuizList(ResultSet rs, List<Quiz> quizzes) throws SQLException {
		int errorCount = -1;
		while (rs.next()) {	
			Quiz quiz = new Quiz();
			if(setQuizFields(rs, quiz)) {
				if (errorCount == -1) errorCount = 0;	//Only set the first time
				quizzes.add(quiz);
			} else {
				if (errorCount == -1) errorCount = 0;  //Only set the first time
				errorCount++;
			}
		} 
		return errorCount;
	}
	
	
	/*
	public static SelectListResponse<Quiz> getRecentForUser(String username, String quizId, int count) {
		return new SelectListResponse<Quiz>
			(null, DBResponse.Status.FAILURE, DBResponse.Cause.INVALID_QUERY_PARAMETER, null);
	}
	*/
	
	/* done */
	public static SelectListResponse<Quiz> getQuizzesByLikes(int count) {
		ResultSet rs = null;
		PreparedStatement ps = null;
		Connection connection = null;
		String sql = "SELECT quiz.quizId AS quizId, activity.username as creatorUsername, " + 
					" 		description, title, hits, likes, " +
					" 		quiz.isRandom, isSinglePage, isImmediateCorrect, " +
					" 		activity.date, activity.username" +
					" FROM quiz" +
					" JOIN creation" +
					"	ON quiz.quizId = creation.quizId " +
					" JOIN activity" +
					"	ON creation.activityId = activity.activityId " +
					" ORDER BY quiz.likes DESC" +
					" LIMIT ? ";
		connection = DBConnection.getConnection();
		try {
			ps = connection.prepareStatement(sql);
			ps.setInt(1, count);		
	
			rs = ps.executeQuery();

			//Assemble 
			List<Quiz> quizzes = new ArrayList<Quiz>();
			int errorCount = assembleQuizList(rs, quizzes);
			
			if(errorCount == 0) {
				return new SelectListResponse<Quiz>
					(quizzes, DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
			} else if (errorCount == -1){
				return new SelectListResponse<Quiz>
					(null, DBResponse.Status.FAILURE, DBResponse.Cause.NO_RESULT_FOUND, null);
			} else	{	//For returning lists: errorCount > 0, only some objects were able to be composed
				return new SelectListResponse<Quiz>
					(quizzes, DBResponse.Status.PARTIAL_SUCCESS, DBResponse.Cause.PARTIAL_DATA_ACCESS_FAILURE, null);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			DBUtils.closeResultSet(rs);
			DBUtils.closeStatement(ps);
			DBUtils.closeConnection(connection);
		}	
		return new SelectListResponse<Quiz>
			(null, DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, null);
	}
	
	/* done */
	public static SelectListResponse<Quiz> getQuizzesByHits(int count) {
		ResultSet rs = null;
		PreparedStatement ps = null;
		Connection connection = null;
		String sql = "SELECT quiz.quizId AS quizId, activity.username as creatorUsername, " + 
					" 		description, title, hits, likes, " +
					" 		quiz.isRandom, isSinglePage, isImmediateCorrect, " +
					" 		activity.date, activity.username" +
					" FROM quiz" +
					" JOIN creation" +
					"	ON quiz.quizId = creation.quizId " +
					" JOIN activity" +
					"	ON creation.activityId = activity.activityId " +
					" ORDER BY quiz.hits DESC" +
					" LIMIT ? ";
		connection = DBConnection.getConnection();
		try {
			ps = connection.prepareStatement(sql);
			ps.setInt(1, count);		
	
			rs = ps.executeQuery();

			//Assemble 
			List<Quiz> quizzes = new ArrayList<Quiz>();
			int errorCount = assembleQuizList(rs, quizzes);
			
			if(errorCount == 0) {
				return new SelectListResponse<Quiz>
					(quizzes, DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
			} else if (errorCount == -1){
				return new SelectListResponse<Quiz>
					(null, DBResponse.Status.FAILURE, DBResponse.Cause.NO_RESULT_FOUND, null);
			} else	{	//For returning lists: errorCount > 0, only some objects were able to be composed
				return new SelectListResponse<Quiz>
					(quizzes, DBResponse.Status.PARTIAL_SUCCESS, DBResponse.Cause.PARTIAL_DATA_ACCESS_FAILURE, null);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			DBUtils.closeResultSet(rs);
			DBUtils.closeStatement(ps);
			DBUtils.closeConnection(connection);
		}	
		return new SelectListResponse<Quiz>
			(null, DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, null);
	}
	
	public static SelectResponse<Long> getHits(String quizId) {
		ResultSet rs = null;
		PreparedStatement ps = null;
		Connection connection = null;
		String sql = "SELECT quiz.hits" +
					" FROM quiz" +
					" WHERE quiz.quizId = ?";
		connection = DBConnection.getConnection();

		long longQuizId;
		if(Utils.isLongNumber(quizId)) {
			longQuizId = Utils.toLong(quizId);
		} else {
			String message = "QuizId not a valid integer";
			return new SelectResponse<Long>
				(null, DBResponse.Status.FAILURE, DBResponse.Cause.INVALID_QUERY_PARAMETER, message);
		}
		
		try {
			ps = connection.prepareStatement(sql);
			ps.setLong(1, longQuizId);		
	
			rs = ps.executeQuery();
			
			Long hits = null;
			if(rs.next()) { hits = DBUtils.getLong(rs, "quiz.hits", null); }
			
			if(hits != null) {
				return new SelectResponse<Long>
					(hits, DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
			} 
			return new SelectResponse<Long>
				(null, DBResponse.Status.FAILURE, DBResponse.Cause.NO_RESULT_FOUND, null);
		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			DBUtils.closeResultSet(rs);
			DBUtils.closeStatement(ps);
			DBUtils.closeConnection(connection);
		}	
		return new SelectResponse<Long>
			(null, DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, null);
	}
	
	public static SelectResponse<Long> getLikes(String quizId) {
		ResultSet rs = null;
		PreparedStatement ps = null;
		Connection connection = null;
		String sql = "SELECT quiz.likes" +
					" FROM quiz" +
					" WHERE quiz.quizId = ?";
		connection = DBConnection.getConnection();

		long longQuizId;
		if(Utils.isLongNumber(quizId)) {
			longQuizId = Utils.toLong(quizId);
		} else {
			String message = "QuizId not a valid integer";
			return new SelectResponse<Long>
				(null, DBResponse.Status.FAILURE, DBResponse.Cause.INVALID_QUERY_PARAMETER, message);
		}
		
		try {
			ps = connection.prepareStatement(sql);
			ps.setLong(1, longQuizId);		
	
			rs = ps.executeQuery();
			
			Long likes = null;
			if(rs.next()) { likes = DBUtils.getLong(rs, "quiz.likes", null); }
			
			if(likes != null) {
				return new SelectResponse<Long>
					(likes, DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
			} 
			return new SelectResponse<Long>
				(null, DBResponse.Status.FAILURE, DBResponse.Cause.NO_RESULT_FOUND, null);
		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			DBUtils.closeResultSet(rs);
			DBUtils.closeStatement(ps);
			DBUtils.closeConnection(connection);
		}	
		return new SelectResponse<Long>
			(null, DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, null);
	}
	
	/**
	 * Returns List of top quiz results for a particular quiz, no time limit, sorted by highest score first
	 * @param quizId Quiz identifier of interest
	 * @param count Max number of records returned
	 * @return
	 */
	/* done */
	public static SelectListResponse<Result> getTopResults(String quizId, int count) {
		ResultSet rs = null;
		PreparedStatement ps = null;
		Connection connection = null;
		String sql = "SELECT  activity.activityId, activity.date, activity.username, activity.activityType, " +
				"			result.score, result.elapsedTime," +
				"			result.quizId, quizresult.title " +
				"	FROM result" +
				"	JOIN quiz quizresult" +
				"		ON result.quizId = quizresult.quizId " +
				"	JOIN activity" +
				"		ON result.activityId = activity.activityId " +
				"	WHERE result.quizId = ? " +
				"	ORDER BY result.score DESC, result.elapsedTime ASC" +
				"	LIMIT ? ";
		connection = DBConnection.getConnection();
		
		long longQuizId;
		if(Utils.isLongNumber(quizId)) {
			longQuizId = Utils.toLong(quizId);
		} else {
			String message = "QuizId not a valid integer";
			return new SelectListResponse<Result>
				(null, DBResponse.Status.FAILURE, DBResponse.Cause.INVALID_QUERY_PARAMETER, message);
		}
		
		try {
			ps = connection.prepareStatement(sql);
			ps.setLong(1, longQuizId);		
			ps.setInt(2, count);		
	
			rs = ps.executeQuery();

			//Assemble 
			List<Result> results = new ArrayList<Result>();
			int errorCount = ActivityDB.assembleResultList(rs, results);
			
			if(errorCount == 0) {
				return new SelectListResponse<Result>
					(results, DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
			} else if (errorCount == -1){
				return new SelectListResponse<Result>
					(null, DBResponse.Status.FAILURE, DBResponse.Cause.NO_RESULT_FOUND, null);
			} else	{	//For returning lists: errorCount > 0, only some objects were able to be composed
				return new SelectListResponse<Result>
					(results, DBResponse.Status.PARTIAL_SUCCESS, DBResponse.Cause.PARTIAL_DATA_ACCESS_FAILURE, null);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			DBUtils.closeResultSet(rs);
			DBUtils.closeStatement(ps);
			DBUtils.closeConnection(connection);
		}	
		return new SelectListResponse<Result>
			(null, DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, null);
	}
	
	
	/**
	 * Returns List of top quiz results for a particular quiz within last specified number of minutes, highest score first
	 * @param quizId Quiz identifier of interest
	 * @param minutes Number of minutes in the past
	 * @param count Max number of records returned
	 * @return
	 */
	/* done */
	public static SelectListResponse<Result> getTopRecentResults(String quizId, int minutes, int count) {
		ResultSet rs = null;
		PreparedStatement ps = null;
		Connection connection = null;
		String sql = "SELECT  activity.activityId, activity.date, activity.username, activity.activityType, " +
				"			result.score, result.elapsedTime," +
				"			result.quizId, quizresult.title " +
				"	FROM result" +
				"	JOIN quiz quizresult" +
				"		ON result.quizId = quizresult.quizId " +
				"	JOIN activity" +
				"		ON result.activityId = activity.activityId " +
				"	WHERE (result.quizId = ?) AND " +
				"		(activity.date BETWEEN DATE_SUB(CURRENT_TIMESTAMP(), INTERVAL ? MINUTE) AND CURRENT_TIMESTAMP())" +
				"	ORDER BY result.score DESC, result.elapsedTime ASC" +
				"	LIMIT ? ";
		connection = DBConnection.getConnection();
		
		long longQuizId;
		if(Utils.isLongNumber(quizId)) {
			longQuizId = Utils.toLong(quizId);
		} else {
			String message = "QuizId not a valid integer";
			return new SelectListResponse<Result>
				(null, DBResponse.Status.FAILURE, DBResponse.Cause.INVALID_QUERY_PARAMETER, message);
		}
		
		try {
			ps = connection.prepareStatement(sql);
			ps.setLong(1, longQuizId);	
			ps.setInt(2, minutes);
			ps.setInt(3, count);		
	
			rs = ps.executeQuery();

			//Assemble 
			List<Result> results = new ArrayList<Result>();
			int errorCount = ActivityDB.assembleResultList(rs, results);
			
			if(errorCount == 0) {
				return new SelectListResponse<Result>
					(results, DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
			} else if (errorCount == -1){
				return new SelectListResponse<Result>
					(null, DBResponse.Status.FAILURE, DBResponse.Cause.NO_RESULT_FOUND, null);
			} else	{	//For returning lists: errorCount > 0, only some objects were able to be composed
				return new SelectListResponse<Result>
					(results, DBResponse.Status.PARTIAL_SUCCESS, DBResponse.Cause.PARTIAL_DATA_ACCESS_FAILURE, null);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			DBUtils.closeResultSet(rs);
			DBUtils.closeStatement(ps);
			DBUtils.closeConnection(connection);
		}	
		return new SelectListResponse<Result>
			(null, DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, null);
	}
	
	/**
	 * Recent result for particular Quiz
	 * @param quizId
	 * @param minutes
	 * @param count
	 * @return
	 */
	public static SelectListResponse<Result> getRecentResults(String quizId, int minutes, int count) {
		ResultSet rs = null;
		PreparedStatement ps = null;
		Connection connection = null;
		String sql = "SELECT  activity.activityId, activity.date, activity.username, activity.activityType, " +
				"			result.score, result.elapsedTime," +
				"			result.quizId, quizresult.title " +
				"	FROM result" +
				"	JOIN quiz quizresult" +
				"		ON result.quizId = quizresult.quizId " +
				"	JOIN activity" +
				"		ON result.activityId = activity.activityId " +
				"	WHERE (result.quizId = ?) AND " +
				"		(activity.date BETWEEN DATE_SUB(CURRENT_TIMESTAMP(), INTERVAL ? MINUTE) AND CURRENT_TIMESTAMP())" +
				"	ORDER BY activity.date DESC" +
				"	LIMIT ? ";
		connection = DBConnection.getConnection();
		
		long longQuizId;
		if(Utils.isLongNumber(quizId)) {
			longQuizId = Utils.toLong(quizId);
		} else {
			String message = "QuizId not a valid integer";
			return new SelectListResponse<Result>
				(null, DBResponse.Status.FAILURE, DBResponse.Cause.INVALID_QUERY_PARAMETER, message);
		}
		
		try {
			ps = connection.prepareStatement(sql);
			ps.setLong(1, longQuizId);	
			ps.setInt(2, minutes);
			ps.setInt(3, count);		
	
			rs = ps.executeQuery();

			//Assemble 
			List<Result> results = new ArrayList<Result>();
			int errorCount = ActivityDB.assembleResultList(rs, results);
			
			if(errorCount == 0) {
				return new SelectListResponse<Result>
					(results, DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
			} else if (errorCount == -1){
				return new SelectListResponse<Result>
					(null, DBResponse.Status.FAILURE, DBResponse.Cause.NO_RESULT_FOUND, null);
			} else	{	//For returning lists: errorCount > 0, only some objects were able to be composed
				return new SelectListResponse<Result>
					(results, DBResponse.Status.PARTIAL_SUCCESS, DBResponse.Cause.PARTIAL_DATA_ACCESS_FAILURE, null);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			DBUtils.closeResultSet(rs);
			DBUtils.closeStatement(ps);
			DBUtils.closeConnection(connection);
		}	
		return new SelectListResponse<Result>
			(null, DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, null);
	}
	
	public static SelectResponse<Double> getAverageScore(String quizId) {
		ResultSet rs = null;
		PreparedStatement ps = null;
		Connection connection = null;
		String sql = "SELECT AVG(result.score)" +
					" FROM quiz" +
					" JOIN result" +
					" 	ON quiz.quizId = result.quizId" +
					" WHERE quiz.quizId = ?";
		connection = DBConnection.getConnection();

		long longQuizId;
		if(Utils.isLongNumber(quizId)) {
			longQuizId = Utils.toLong(quizId);
		} else {
			String message = "QuizId not a valid integer";
			return new SelectResponse<Double>
				(null, DBResponse.Status.FAILURE, DBResponse.Cause.INVALID_QUERY_PARAMETER, message);
		}
		
		try {
			ps = connection.prepareStatement(sql);
			ps.setLong(1, longQuizId);		
	
			rs = ps.executeQuery();
			
			Double avgScore = null;
			if(rs.next()) { avgScore = DBUtils.getDouble(rs, "AVG(result.score)", null); }
			
			if(avgScore != null) {
				return new SelectResponse<Double>
					(avgScore, DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
			} 
			return new SelectResponse<Double>
				(null, DBResponse.Status.FAILURE, DBResponse.Cause.NO_RESULT_FOUND, null);
		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			DBUtils.closeResultSet(rs);
			DBUtils.closeStatement(ps);
			DBUtils.closeConnection(connection);
		}	
		return new SelectResponse<Double>
			(null, DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, null);
	}
	
	public static SelectResponse<BigDecimal> getAverageTime(String quizId) {
		ResultSet rs = null;
		PreparedStatement ps = null;
		Connection connection = null;
		String sql = "SELECT AVG(result.elapsedTime)" +
					" FROM quiz" +
					" JOIN result" +
					" 	ON quiz.quizId = result.quizId" +
					" WHERE quiz.quizId = ?";
		connection = DBConnection.getConnection();

		long longQuizId;
		if(Utils.isLongNumber(quizId)) {
			longQuizId = Utils.toLong(quizId);
		} else {
			String message = "QuizId not a valid integer";
			return new SelectResponse<BigDecimal>
				(null, DBResponse.Status.FAILURE, DBResponse.Cause.INVALID_QUERY_PARAMETER, message);
		}
		
		try {
			ps = connection.prepareStatement(sql);
			ps.setLong(1, longQuizId);		
	
			rs = ps.executeQuery();
			
			BigDecimal avgTime = null;
			if(rs.next()) { avgTime = rs.getBigDecimal("AVG(result.elapsedTime)"); }
			
			if(avgTime != null) {
				return new SelectResponse<BigDecimal>
					(avgTime, DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
			} 
			return new SelectResponse<BigDecimal>
				(null, DBResponse.Status.FAILURE, DBResponse.Cause.NO_RESULT_FOUND, null);
		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			DBUtils.closeResultSet(rs);
			DBUtils.closeStatement(ps);
			DBUtils.closeConnection(connection);
		}	
		return new SelectResponse<BigDecimal>
			(null, DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, null);
	}
	
	
	// **************
	// Insert methods
	// **************
	/**
	 * Insert a Quiz object and all of the associated members within the objects.
	 * If Quiz has list of Questions, those will be decomposed and inserted into
	 * respective tables.  If Questions have Choices and Answers, those will be
	 * decomposed and inserted as well.
	 * If a SQL Exception occurs during insert, the transaction will rollback to
	 * before insert.  This method allows partial list of records to be inserted.
	 * However, for those records that are inserted, the whole chain of children are also inserted.
	 * @return Data transfer object containing the string value of newly insert Quiz record primary database id
	 */
	public static InsertResponse<String> addQuiz(Quiz quiz) {
		PreparedStatement ps = null;
		Connection connection = null;		
		String message = null;

		connection = DBConnection.getConnection();
		
		try {
			connection.setAutoCommit(false);	//Turn auto-commit off for transaction commit
			
			//Quiz table
			String sql = 
				"INSERT INTO quiz(`description`, `title`, `hits`, `likes`, `isRandom`, `isSinglePage`, `isImmediateCorrect`)" +
				" VALUES (?, ?, ?, ?, ?, ?, ?)";
			ps = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
	
			ps.setString(1, quiz.getDescription());
			ps.setString(2, quiz.getTitle());
			ps.setLong(3, quiz.getHits());
			ps.setLong(4, quiz.getLikes());
			ps.setBoolean(5, quiz.getRandom());
			ps.setBoolean(6, quiz.getSinglePage());
			ps.setBoolean(7, quiz.getImmCorretion());
			
			int rows = ps.executeUpdate();
			String quizId = DBUtils.getLongAutoGeneratedKey(ps);
			
			InsertResponse<Integer> questionInsertResult;
			if(quizId != null) {			//Key successfully generated
				quiz.setQuizID(quizId);
				
				//Insert Creation record
				if(!insertCreation(quiz, connection)) 
					throw new SQLException("Unable to insert creation record");
				
				//Insert Questions, Answers, and Choices
				List<Question> questions = quiz.getQuestions();
				questionInsertResult = QuestionDB.addQuestions(questions, connection, quizId);
			} else {
				throw new SQLException("Unable to get primary key for quiz.");
			}
			
			if (questionInsertResult.getStatus() == DBResponse.Status.SUCCESS) {
				connection.commit();	//commit transaction
				return new InsertResponse<String>
					(quizId, DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
			} else {
				throw new SQLException("Some portion of insert fail");
			}
			
		} catch (SQLException se) {
			se.printStackTrace();
			if(connection != null) {
				try {
					System.err.print("Transaction is being rolled back");
					connection.rollback();
				} catch (SQLException sqe) {
					se.printStackTrace();
				}
			}
			return new InsertResponse<String>
				(null, DBResponse.Status.FAILURE, DBResponse.Cause.UNABLE_INSERT_OBJECT, null);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				connection.setAutoCommit(true);
			} catch (SQLException sqe) {
				sqe.printStackTrace();
			}
			DBUtils.closeStatement(ps);
			DBUtils.closeConnection(connection);
		}	
		return new InsertResponse<String>
			(null, DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, message);
	}
	
	
	private static boolean insertCreation(Quiz quiz, Connection connection) throws SQLException {
		//Compose activity
		Creation creation = new Creation();
		creation.setDate(quiz.getCreateDate());
		creation.setUsername(quiz.getCreatorName());
		creation.setQuizID(quiz.getQuizID());
		
		//Activity table
		String activityId = ActivityDB.insertActivity(connection, creation);
		if(activityId != null) {
			creation.setActivityID(activityId);

			//Creation table
			if(ActivityDB.insertCreation(connection, creation)) return true;
		} 
		return false;
	}
	
	
	/**
	 * Insert data fields into the Quiz table
	 * @param creator
	 * @param description
	 * @param title
	 * @param isRandom
	 * @param isSinglePage
	 * @param isImmediateCorrect
	 * @return Data transfer object containing the string value of newly insert Quiz record primary database id
	 */
	public static InsertResponse<String> addQuiz(String creator, String description, 
			String title, boolean isRandom, boolean isSinglePage, boolean isImmediateCorrect) {
		ResultSet rsNewIds = null;
		PreparedStatement ps = null;
		Connection connection = null;		
		String message = null;

		//Query string
		String sql = 
			"INSERT INTO quiz(`creatorUsername`, `description`, `title`, `hits`, `likes`, `isRandom`, `isSinglePage`, `isImmediateCorrect`)" +
			" VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
		
		connection = DBConnection.getConnection();
		try {
			long hits = 0l;
			long likes = 0l;
			
			ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, creator);
			ps.setString(2, description);
			ps.setString(3, title);
			ps.setLong(4, hits);
			ps.setLong(5, likes);
			ps.setBoolean(6, isRandom);
			ps.setBoolean(7, isSinglePage);
			ps.setBoolean(8, isImmediateCorrect);
			
			int rows = ps.executeUpdate();
			
			//Get auto-generated id
			if (rows >= 1) {	
				String newId = DBUtils.getLongAutoGeneratedKey(ps);
				if(newId != null) {
					return new InsertResponse<String>
						(newId, DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
				} else {
					return new InsertResponse<String>
						(null, DBResponse.Status.FAILURE, DBResponse.Cause.UNABLE_TO_GET_KEY, null);
				}
			} else {
				return new InsertResponse<String>
					(null, DBResponse.Status.FAILURE, DBResponse.Cause.UNABLE_INSERT_OBJECT, null);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			DBUtils.closeResultSet(rsNewIds);
			DBUtils.closeStatement(ps);
			DBUtils.closeConnection(connection);
		}	
		return new InsertResponse<String>
			(null, DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, message);
	}

	
	/* done */
	public static InsertResponse<String> addResult (Result result) {
		//Redirect to handle more general case
		return ActivityDB.addActivity(result);
	}
	
	/* optional */
	public static InsertResponse<String> addResult(String username, Date date, 
			String quizId, double score, long elapsedTime) {
		//ActivityType type = ActivityType.RESULT;
		return new InsertResponse<String>
			(null, DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
	}

	// **************
	// Update methods
	// **************
	public static UpdateResponse addHit(String quizId) {
		PreparedStatement ps = null;
		Connection connection = null;		

		//Query string
		String sql ="UPDATE quiz SET hits = hits + 1 WHERE quiz.quizId = ?";
		connection = DBConnection.getConnection();
		
		long longQuizId;
		if(Utils.isLongNumber(quizId)) {
			longQuizId = Utils.toLong(quizId);
		} else {
			String message = "QuizId not a valid integer";
			return new UpdateResponse
				(DBResponse.Status.FAILURE, DBResponse.Cause.INVALID_QUERY_PARAMETER, message);
		}
		
		try {
			ps = connection.prepareStatement(sql);
			ps.setLong(1, longQuizId);	
			
			int rows = ps.executeUpdate();
			
			if(rows > 0) {
				return new UpdateResponse
					(DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
			}
			return new UpdateResponse
				(DBResponse.Status.FAILURE, DBResponse.Cause.NO_RESULT_FOUND, null);
		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			DBUtils.closeStatement(ps);
			DBUtils.closeConnection(connection);
		}	
		return new UpdateResponse
			(DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, null);
	}
	
	public static UpdateResponse resetHits(String quizId) {
		PreparedStatement ps = null;
		Connection connection = null;		

		//Query string
		String sql ="UPDATE quiz SET hits = 0 WHERE quiz.quizId = ?";
		connection = DBConnection.getConnection();
		
		long longQuizId;
		if(Utils.isLongNumber(quizId)) {
			longQuizId = Utils.toLong(quizId);
		} else {
			String message = "QuizId not a valid integer";
			return new UpdateResponse
				(DBResponse.Status.FAILURE, DBResponse.Cause.INVALID_QUERY_PARAMETER, message);
		}
		
		try {
			ps = connection.prepareStatement(sql);
			ps.setLong(1, longQuizId);	
			int rows = ps.executeUpdate();
			
			if(rows > 0) {
				return new UpdateResponse
					(DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
			}
			return new UpdateResponse
				(DBResponse.Status.FAILURE, DBResponse.Cause.NO_RESULT_FOUND, null);
		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			DBUtils.closeStatement(ps);
			DBUtils.closeConnection(connection);
		}	
		return new UpdateResponse
			(DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, null);
	}
	
	public static UpdateResponse addLike(String quizId) {
		PreparedStatement ps = null;
		Connection connection = null;		

		//Query string
		String sql ="UPDATE quiz SET likes = likes + 1 WHERE quiz.quizId = ?";
		connection = DBConnection.getConnection();
		
		long longQuizId;
		if(Utils.isLongNumber(quizId)) {
			longQuizId = Utils.toLong(quizId);
		} else {
			String message = "QuizId not a valid integer";
			return new UpdateResponse
				(DBResponse.Status.FAILURE, DBResponse.Cause.INVALID_QUERY_PARAMETER, message);
		}
		
		try {
			ps = connection.prepareStatement(sql);
			ps.setLong(1, longQuizId);	
			
			int rows = ps.executeUpdate();
			
			if(rows > 0) {
				return new UpdateResponse
					(DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
			}
			return new UpdateResponse
				(DBResponse.Status.FAILURE, DBResponse.Cause.NO_RESULT_FOUND, null);
		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			DBUtils.closeStatement(ps);
			DBUtils.closeConnection(connection);
		}	
		return new UpdateResponse
			(DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, null);
	}
	
	public static UpdateResponse resetLikes(String quizId) {
		PreparedStatement ps = null;
		Connection connection = null;		

		//Query string
		String sql ="UPDATE quiz SET likes = 0 WHERE quiz.quizId = ?";
		connection = DBConnection.getConnection();
		
		long longQuizId;
		if(Utils.isLongNumber(quizId)) {
			longQuizId = Utils.toLong(quizId);
		} else {
			String message = "QuizId not a valid integer";
			return new UpdateResponse
				(DBResponse.Status.FAILURE, DBResponse.Cause.INVALID_QUERY_PARAMETER, message);
		}
		
		try {
			ps = connection.prepareStatement(sql);
			ps.setLong(1, longQuizId);	
			int rows = ps.executeUpdate();
			
			if(rows > 0) {
				return new UpdateResponse
					(DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
			}
			return new UpdateResponse
			(DBResponse.Status.FAILURE, DBResponse.Cause.NO_RESULT_FOUND, null);
		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			DBUtils.closeStatement(ps);
			DBUtils.closeConnection(connection);
		}	
		return new UpdateResponse
			(DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, null);
	}
	
	// **************
	// Delete methods
	// **************
	public static DeleteResponse deleteQuiz(String quizId) {
		ActivityDB.deleteCreation(quizId);
		ActivityDB.deleteResult(quizId);
		PreparedStatement ps = null;
		Connection connection = null;		

		//Query string
		String sql ="DELETE FROM quiz WHERE quiz.quizId = ? ";
		connection = DBConnection.getConnection();
		
		long longQuizId;
		if(Utils.isLongNumber(quizId)) {
			longQuizId = Utils.toLong(quizId);
		} else {
			String message = "QuizId not a valid integer";
			return new DeleteResponse
				(DBResponse.Status.FAILURE, DBResponse.Cause.INVALID_QUERY_PARAMETER, message);
		}
		
		try {
			ps = connection.prepareStatement(sql);
			ps.setLong(1, longQuizId);			
			
			int rows = ps.executeUpdate();
			
			if(rows > 0) {
				return new DeleteResponse
					(DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
			}
			return new DeleteResponse
				(DBResponse.Status.FAILURE, DBResponse.Cause.NO_RESULT_FOUND, null);
		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			DBUtils.closeStatement(ps);
			DBUtils.closeConnection(connection);
		}	
		return new DeleteResponse
		(DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, null);
	}
}
