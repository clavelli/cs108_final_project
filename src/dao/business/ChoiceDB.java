package dao.business;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Choice;
import utils.Utils;
import dao.DBConnection;
import dao.DBResponse;
import dao.DBUtils;
import dao.InsertResponse;
import dao.SelectListResponse;

public class ChoiceDB {

	// **************
	// Get methods
	// **************
	public static SelectListResponse<Choice> getChoices(String QuestionId) {
		ResultSet rs = null;
		Connection connection = null;
		PreparedStatement ps = null;
		String message = null;
		
		//Query string
		String sql ="SELECT choiceId, isAnswer, choiceNumber, choice.text" +
					" FROM choice " + 
					" WHERE questionId = ?";		
	
		connection = DBConnection.getConnection();
		
		
		long longQuestionId;
		if(Utils.isLongNumber(QuestionId)) {
			longQuestionId = Utils.toLong(QuestionId);
		} else {
			message = "QuestionId not a valid integer";
			return new SelectListResponse<Choice>
				(null, DBResponse.Status.FAILURE, DBResponse.Cause.INVALID_QUERY_PARAMETER, message);
		}
		
		try {
			ps = connection.prepareStatement(sql);
			ps.setLong(1, longQuestionId);	
			
			rs = ps.executeQuery();	
			
			//Assemble result
			List<Choice> choices = new ArrayList<Choice>();
			int errorCount = -1;
			while (rs.next()) {	
				Choice choice = new Choice();
				if(assembleChoice(rs, choice)) {
					if (errorCount == -1) errorCount = 0;	//Only set the first time
					choices.add(choice);
				} else {
					//For list returns
					if (errorCount == -1) errorCount = 0;  //Only set the first time
					errorCount++;
				}
			} 
			
			//Return result
			if(errorCount == 0) {
				return new SelectListResponse<Choice>
					(choices, DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
			} else if (errorCount == -1){
				return new SelectListResponse<Choice>
					(null, DBResponse.Status.FAILURE, DBResponse.Cause.NO_RESULT_FOUND, null);
			} else	{	//For returning lists: errorCount > 0, only some objects were able to be composed
				return new SelectListResponse<Choice>
					(choices, DBResponse.Status.PARTIAL_SUCCESS, DBResponse.Cause.PARTIAL_DATA_ACCESS_FAILURE, null);
			}
	
		} catch (SQLException se) {
			message = "Class: ChoiceDB, ExceptionType: SQLException, Message: " + se.getMessage();
			se.printStackTrace();
		} catch (Exception ex) {
			message = "Class: ChoiceDB, ExceptionType: Exception, Message: " + ex.getMessage();
			ex.printStackTrace();
		} finally {
			DBUtils.closeResultSet(rs);
			DBUtils.closeStatement(ps);
			DBUtils.closeConnection(connection);
		}	
		return new SelectListResponse<Choice>
			(null, DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, message);
	}
	
	/**
	 * Sets the state of an Choice object with returned ResultSet data object 
	 *  when passed in a list of choices
	 * @param rs
	 * @param choices List of choices that will append newly assembled Choice
	 * @return
	 */
	public static boolean assembleChoice(ResultSet rs, List<Choice> choices) {
		try {
			Choice choice = new Choice();
			choice.setChoiceID(DBUtils.getLong(rs, "choiceId", -1l).toString());
			choice.setCorrect(DBUtils.getBoolean(rs, "isAnswer", false));
			choice.setChoiceNumber(DBUtils.getInteger(rs, "choiceNumber", 0));
			choice.setText(DBUtils.getString(rs, "choice.text", ""));
			
			choices.add(choice);
			return true;
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * Sets the state of an Choice object with returned ResultSet data object 
	 * @param rs
	 * @param choice instance object to be assembled
	 * @return
	 */
	private static boolean assembleChoice(ResultSet rs, Choice choice) {
		try {
			choice.setChoiceID(DBUtils.getLong(rs, "choiceId", -1l).toString());
			choice.setCorrect(DBUtils.getBoolean(rs, "isAnswer", false));
			choice.setChoiceNumber(DBUtils.getInteger(rs, "choiceNumber", 0));
			choice.setText(DBUtils.getString(rs, "text", ""));
			
			return true;
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	// **************
	// Insert methods
	// **************
	public static InsertResponse<String> addChoice(Choice c) {
		return new InsertResponse<String>
			(null, DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
	}
	

	/* optional */
	public static InsertResponse<String> addChoice(String questionId, boolean isAnswer, int choiceNumber, String text) {
		return new InsertResponse<String>
			(null, DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
	}
	
	
	//dto = count of rows successfully inserted
	public static InsertResponse<Integer> addChoices(List<Choice> choices, Connection con, String questionId) {
		PreparedStatement ps = null;	
		try {
			String sql = 
				"INSERT INTO choice (questionId, text, choiceNumber, isAnswer)" + 
				" VALUES (?, ?, ?, ?)";
			ps = con.prepareStatement(sql);
			
			for(Choice c: choices) {
				DBUtils.setStringToLong(ps, 1, questionId);	
				ps.setString(2, c.getText());
				ps.setInt(3, c.getChoiceNumber());
				ps.setBoolean(4, c.isCorrect());
				
				ps.addBatch();
			}
			
			int[] updateCounts = ps.executeBatch();
			int rowCount = updateCounts.length;
			
			//Success metrics
			if(rowCount == choices.size()) {	
				return new InsertResponse<Integer>
					(rowCount, DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
			} else if(rowCount == 0) {
				return new InsertResponse<Integer>
					(null, DBResponse.Status.FAILURE, DBResponse.Cause.UNABLE_INSERT_OBJECT, null);
			} else {
				return new InsertResponse<Integer>
					(rowCount, DBResponse.Status.PARTIAL_SUCCESS, DBResponse.Cause.UNABLE_INSERT_OBJECT, null);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBUtils.closeStatement(ps);
		}
		return new InsertResponse<Integer>
			(null, DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, null);
	}
	

	
	// **************
	// Update methods
	// **************
	
	
	// **************
	// Delete methods
	// **************
	
}
