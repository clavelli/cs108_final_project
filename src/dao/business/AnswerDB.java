package dao.business;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import model.Answer;
import utils.Utils;
import dao.DBConnection;
import dao.DBResponse;
import dao.DBUtils;
import dao.InsertResponse;
import dao.PreviousIds;
import dao.SelectListResponse;

public class AnswerDB {
	
	// **************
	// Get methods
	// **************
	public static SelectListResponse<Answer> getAnswers(String questionId) {
		ResultSet rs = null;
		PreparedStatement ps = null;
		Connection connection = null;
		
		String message = null;
		
		//Query string
		String sql ="SELECT answer.answerId AS answerId, answerNumber, answervariation.text" +
					" FROM answer" +
					" JOIN answervariation" +
					" ON answer.answerId = answervariation.answerId" +
					" WHERE questionId = ?";	
		connection = DBConnection.getConnection();
		
		long longQuestionId;
		if(Utils.isLongNumber(questionId)) {
			longQuestionId = Utils.toLong(questionId);
		} else {
			message = "QuestionId not a valid integer";
			return new SelectListResponse<Answer>
				(null, DBResponse.Status.FAILURE, DBResponse.Cause.INVALID_QUERY_PARAMETER, message);
		}
		
		try {
			ps = connection.prepareStatement(sql);
			ps.setLong(1, longQuestionId);	
			
			rs = ps.executeQuery();	
			
			//Assemble result
			List<Answer> answers = new LinkedList<Answer>();
			PreviousIds ids = new PreviousIds();	//Memory for last id encountered
			int errorCount = -1;
			while (rs.next()) {	
				if(assembleAnswer(rs, ids, (LinkedList<Answer>)answers)) {
					if (errorCount == -1) errorCount = 0;	//Only set the first time
				} else {
					//For list returns
					if (errorCount == -1) errorCount = 0;  //Only set the first time
					errorCount++;
				}
			} 
			
			//Return result
			if(errorCount == 0) {
				return new SelectListResponse<Answer>
					(answers, DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
			} else if (errorCount == -1){
				return new SelectListResponse<Answer>
					(null, DBResponse.Status.FAILURE, DBResponse.Cause.NO_RESULT_FOUND, null);
			} else	{	//For returning lists: errorCount > 0, only some objects were able to be composed
				return new SelectListResponse<Answer>
					(answers, DBResponse.Status.PARTIAL_SUCCESS, DBResponse.Cause.PARTIAL_DATA_ACCESS_FAILURE, null);
			}
	
		} catch (SQLException se) {
			message = "Class: AnswerDB, ExceptionType: SQLException, Message: " + se.getMessage();
			se.printStackTrace();
		} catch (Exception ex) {
			message = "Class: AnswerDB, ExceptionType: Exception, Message: " + ex.getMessage();
			ex.printStackTrace();
		} finally {
			DBUtils.closeResultSet(rs);
			DBUtils.closeStatement(ps);
			DBUtils.closeConnection(connection);
		}	
		return new SelectListResponse<Answer>
			(null, DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, message);
	}
	
	/**
	 * Sets the state of an Answer object with returned ResultSet data object 
	 *  if it needs to be determined whether a new Answer instance is needed
	 * @param rs ResultSet object contain query results
	 * @param prevId Previous Id of Answer instance in the last data record. 
	 * 			Used to determine if current data record belong to current
	 * 			Answer instance or start of a new Answer instance
	 * @param answers List of answers.  Last Answer element of list is used if
	 * 			data belongs to the same Answer instance
	 * @return
	 */
	public static boolean assembleAnswer(ResultSet rs, PreviousIds ids, LinkedList<Answer> answers) {
		try {
			String id = DBUtils.getLong(rs, "answerId", -1l).toString();
			if (id.equals(ids.getPrevAnswerId())) {
				Answer answer = answers.getLast();
				
				//Get answer variations
				answer.getVariations().add(DBUtils.getString(rs, "answervariation.text", null));
			} else {	//Start of new object record
				Answer answer = new Answer();
				ids.setPrevAnswerId(id);
				
				//Common field
				setAnswerCommonFields(rs, answer);
				
				//Get answer variations
				answer.getVariations().add(DBUtils.getString(rs, "answervariation.text", null));
				
				//Add to list
				answers.addLast(answer);
			}
			return true;
		} catch(SQLException se) { se.printStackTrace();	
		} catch(Exception e) { e.printStackTrace(); }
		return false;
	}
	
	
	private static void setAnswerCommonFields(ResultSet rs, Answer answer) throws SQLException {
		answer.setAnswerId(DBUtils.getLong(rs, "answerId", null).toString());
		answer.setAnswerNumber(DBUtils.getInteger(rs, "answerNumber", 0));
	}
	
	
	/**
	 * Get all the answer variations for a particular answer
	 * An answer can have one or more answer variations
	 * @param answerId
	 * @return
	 */
	public static SelectListResponse<String> getAnswerVariations(String answerId) {
		ResultSet rs = null;
		Connection connection = null;
		PreparedStatement ps = null;
		String message = null;
		
		//Query string
		String sql ="SELECT text" +
					" FROM answervariation " + 
					" WHERE answerId = ?";		
	
		connection = DBConnection.getConnection();
			
		long longAnswerId;
		if(Utils.isLongNumber(answerId)) {
			longAnswerId = Utils.toLong(answerId);
		} else {
			message = "AnswerId not a valid integer";
			return new SelectListResponse<String>
				(null, DBResponse.Status.FAILURE, DBResponse.Cause.INVALID_QUERY_PARAMETER, message);
		}
		
		try {
			ps = connection.prepareStatement(sql);
			ps.setLong(1, longAnswerId);	
			
			rs = ps.executeQuery();	
			
			//Assemble result
			int errorCount = -1;
			List<String> variations = new ArrayList<String>();
			while (rs.next()) {	
				String answerVariation = (DBUtils.getString(rs, "text", null));
		
				if(answerVariation != null) {
					if (errorCount == -1) errorCount = 0;	//Only set the first time
					variations.add(answerVariation);
				} else {
					//For list returns
					if (errorCount == -1) errorCount = 0;  //Only set the first time
					errorCount++;
				}
			} 
			
			//Return result
			if(errorCount == 0) {
				return new SelectListResponse<String>
					(variations, DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
			} else if (errorCount == -1){
				return new SelectListResponse<String>
					(null, DBResponse.Status.FAILURE, DBResponse.Cause.NO_RESULT_FOUND, null);
			} else	{	//For returning lists: errorCount > 0, only some objects were able to be composed
				return new SelectListResponse<String>
					(variations, DBResponse.Status.PARTIAL_SUCCESS, DBResponse.Cause.PARTIAL_DATA_ACCESS_FAILURE, null);
			}
	
		} catch (SQLException se) {
			message = "Class: AnswerDB, ExceptionType: SQLException, Message: " + se.getMessage();
			se.printStackTrace();
		} catch (Exception ex) {
			message = "Class: AnswerDB, ExceptionType: Exception, Message: " + ex.getMessage();
			ex.printStackTrace();
		} finally {
			DBUtils.closeResultSet(rs);
			DBUtils.closeStatement(ps);
			DBUtils.closeConnection(connection);
		}	
		return new SelectListResponse<String>
			(null, DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, message);
	}
	
	
	// **************
	// Insert methods
	// **************
	public static InsertResponse<String> addAnswer(Answer a) {
		return new InsertResponse<String>
			(null, DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
	}
	
	//dto = count of rows successfully inserted
	public static InsertResponse<Integer> addAnswers(List<Answer> answers, Connection con, String questionId) {
		PreparedStatement ps = null;	
		try {
			String sql = 
				"INSERT INTO answer (questionId, answerNumber)" + 
				" VALUES (?, ?)";
			ps = con.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
			
			int rowCount = 0;
			int errorCount = 0;
			
			for(Answer a: answers) {
				DBUtils.setStringToLong(ps, 1, questionId);	
				ps.setInt(2, a.getAnswerNumber());

				int rows = ps.executeUpdate();
				String answerId = DBUtils.getLongAutoGeneratedKey(ps);
				
				if(answerId != null) {
					rowCount = rowCount + rows;		//Running count of answers successful inserted
					
					addAnswerVariations(a.getVariations(), con, answerId);
				} else {
					errorCount++;
				}
			}
			
			//CONDITIONS FOR SUCCESS
			if (errorCount == 0) {	
					return new InsertResponse<Integer>
						(rowCount, DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
			} else {
				if(rowCount == 0) {
					return new InsertResponse<Integer>
						(null, DBResponse.Status.FAILURE, DBResponse.Cause.UNABLE_INSERT_OBJECT, null);
				}
				return new InsertResponse<Integer>
					(rowCount, DBResponse.Status.PARTIAL_SUCCESS, DBResponse.Cause.UNABLE_INSERT_OBJECT, null);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBUtils.closeStatement(ps);
		}
		return new InsertResponse<Integer>
			(null, DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, null);
	}

	//dto = count of rows successfully inserted
	public static InsertResponse<Integer> addAnswerVariations(List<String> variations, Connection con, String answerId) {
		PreparedStatement ps = null;	
		try {
			String sql = 
				"INSERT INTO answervariation (answerId, text)" + 
				" VALUES (?, ?)";
			ps = con.prepareStatement(sql);
			
			for(String s: variations) {
				DBUtils.setStringToLong(ps, 1, answerId);	
				ps.setString(2, s);
				
				ps.addBatch();
			}
			
			int[] updateCounts = ps.executeBatch();
			int rowCount = updateCounts.length;
			
			//Success metrics
			if(rowCount == variations.size()) {	
				return new InsertResponse<Integer>
					(rowCount, DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
			} else if(rowCount == 0) {
				return new InsertResponse<Integer>
					(null, DBResponse.Status.FAILURE, DBResponse.Cause.UNABLE_INSERT_OBJECT, null);
			} else {
				return new InsertResponse<Integer>
					(rowCount, DBResponse.Status.PARTIAL_SUCCESS, DBResponse.Cause.UNABLE_INSERT_OBJECT, null);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBUtils.closeStatement(ps);
		}
		return new InsertResponse<Integer>
			(null, DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, null);
	}
	
	/* optional */
	public static InsertResponse<String> addAnswer(String questionId, String answer) {
		return new InsertResponse<String>
			(null, DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
	}
	
	/* optional */
	public static InsertResponse<String> addAnswerVariation(String answerId, String anAnswer) {
		return new InsertResponse<String>
			(null, DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
	}
	
	// **************
	// Update methods
	// **************
	
	
	// **************
	// Delete methods
	// **************
	
}
