package dao.business;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import model.Announcement;
import model.Challenge;
import model.Message;
import model.Message.MessageType;
import utils.Utils;
import dao.DBConnection;
import dao.DBResponse;
import dao.DBUtils;
import dao.InsertResponse;
import dao.SelectListResponse;
import dao.UpdateResponse;

public class MessageDB {
	// **************
	// Get methods
	// **************
	public static SelectListResponse<Message> getMessages(String username) {
		ResultSet rs = null;
		PreparedStatement ps = null;
		Connection connection = null;

		String sql = 
			" SELECT message.sender, message.receiver, message.date, message.body, message.read, message.messageType, " +
			"			challenge.quizId " +
			" FROM message " +
			"LEFT OUTER JOIN challenge " +
			"	ON message.messageId = challenge.messageId " +
			" WHERE  message.receiver = ?" +
			" ORDER BY message.date DESC ";

		connection = DBConnection.getConnection();
		try {
			ps = connection.prepareStatement(sql);
			ps.setString(1, username);
	
			rs = ps.executeQuery();

			//Assemble 
			List<Message> messages = new ArrayList<Message>();
			int errorCount = MessageDB.assembleMessageList(rs, messages);
			
			if(errorCount == 0) {
				return new SelectListResponse<Message>
					(messages, DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
			} else if (errorCount == -1){
				return new SelectListResponse<Message>
					(null, DBResponse.Status.FAILURE, DBResponse.Cause.NO_RESULT_FOUND, null);
			} else	{	//For returning lists: errorCount > 0, only some objects were able to be composed
				return new SelectListResponse<Message>
					(messages, DBResponse.Status.PARTIAL_SUCCESS, DBResponse.Cause.PARTIAL_DATA_ACCESS_FAILURE, null);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			DBUtils.closeResultSet(rs);
			DBUtils.closeStatement(ps);
			DBUtils.closeConnection(connection);
		}	
		return new SelectListResponse<Message>
			(null, DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, null);
	}
	
	public static SelectListResponse<Message> getUnreadMessages(String username) {
		ResultSet rs = null;
		PreparedStatement ps = null;
		Connection connection = null;

		String sql = 
			" SELECT message.sender, message.receiver, message.date, message.body, message.read, message.messageType, " +
			"			challenge.quizId " +
			" FROM message " +
			"LEFT OUTER JOIN challenge " +
			"	ON message.messageId = challenge.messageId " +
			" WHERE  message.receiver = ? " +
			"	AND message.read = 0" +
			" ORDER BY message.date DESC ";

		connection = DBConnection.getConnection();
		try {
			ps = connection.prepareStatement(sql);
			ps.setString(1, username);
	
			rs = ps.executeQuery();

			//Assemble 
			List<Message> messages = new ArrayList<Message>();
			int errorCount = MessageDB.assembleMessageList(rs, messages);
			
			if(errorCount == 0) {
				return new SelectListResponse<Message>
					(messages, DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
			} else if (errorCount == -1){
				return new SelectListResponse<Message>
					(null, DBResponse.Status.FAILURE, DBResponse.Cause.NO_RESULT_FOUND, null);
			} else	{	//For returning lists: errorCount > 0, only some objects were able to be composed
				return new SelectListResponse<Message>
					(messages, DBResponse.Status.PARTIAL_SUCCESS, DBResponse.Cause.PARTIAL_DATA_ACCESS_FAILURE, null);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			DBUtils.closeResultSet(rs);
			DBUtils.closeStatement(ps);
			DBUtils.closeConnection(connection);
		}	
		return new SelectListResponse<Message>
			(null, DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, null);
	}
	
	private static SelectListResponse<Message> getMessages_OLD(String username) {
		ResultSet rs = null;
		PreparedStatement ps = null;
		Connection connection = null;
		
		String sql = 
			" SELECT 	message.sender, message.receiver, message.date, message.body, message.read, message.messageType" +
			" FROM message " +
			" WHERE  message.sender = ?" +
			" ORDER BY message.date DESC ";

	
		connection = DBConnection.getConnection();
		try {
			ps = connection.prepareStatement(sql);
			ps.setString(1, username);
	
			rs = ps.executeQuery();

			//Assemble 
			List<Message> messages = new ArrayList<Message>();
			int errorCount = MessageDB.assembleMessageList(rs, messages);
			
			if(errorCount == 0) {
				return new SelectListResponse<Message>
					(messages, DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
			} else if (errorCount == -1){
				return new SelectListResponse<Message>
					(null, DBResponse.Status.FAILURE, DBResponse.Cause.NO_RESULT_FOUND, null);
			} else	{	//For returning lists: errorCount > 0, only some objects were able to be composed
				return new SelectListResponse<Message>
					(messages, DBResponse.Status.PARTIAL_SUCCESS, DBResponse.Cause.PARTIAL_DATA_ACCESS_FAILURE, null);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			DBUtils.closeResultSet(rs);
			DBUtils.closeStatement(ps);
			DBUtils.closeConnection(connection);
		}	
		return new SelectListResponse<Message>
			(null, DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, null);
	}
	
	public static SelectListResponse<Announcement> getAnnoucements(int count) {
		ResultSet rs = null;
		PreparedStatement ps = null;
		Connection connection = null;
		
		String sql = 
			" SELECT 	announcement.username, announcement.text, announcement.date" +
			" FROM announcement " +
			" ORDER BY announcement.date DESC " +
			" LIMIT ?";
		connection = DBConnection.getConnection();
		try {
			ps = connection.prepareStatement(sql);
			ps.setInt(1, count);
	
			rs = ps.executeQuery();

			//Assemble 
			List<Announcement> announcements = new ArrayList<Announcement>();
			int errorCount = MessageDB.assembleAnnouncementList(rs, announcements);
			
			if(errorCount == 0) {
				return new SelectListResponse<Announcement>
					(announcements, DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
			} else if (errorCount == -1){
				return new SelectListResponse<Announcement>
					(null, DBResponse.Status.FAILURE, DBResponse.Cause.NO_RESULT_FOUND, null);
			} else	{	//For returning lists: errorCount > 0, only some objects were able to be composed
				return new SelectListResponse<Announcement>
					(announcements, DBResponse.Status.PARTIAL_SUCCESS, DBResponse.Cause.PARTIAL_DATA_ACCESS_FAILURE, null);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			DBUtils.closeResultSet(rs);
			DBUtils.closeStatement(ps);
			DBUtils.closeConnection(connection);
		}	
		return new SelectListResponse<Announcement>
			(null, DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, null);
	}
	
	// **************
	// Insert methods
	// **************
	/* done */
	public static InsertResponse<String> addMessage(Message m) {
		PreparedStatement ps = null;
		Connection connection = null;		

		connection = DBConnection.getConnection();
		
		try {
			connection.setAutoCommit(false);	//Turn auto-commit off for transaction commit
			
			//Message table
			String messageId = insertMessage(connection, m);
			
			//Message type
			InsertResponse<Integer> insertResult;
			if(messageId != null) {
				m.setMessageID(messageId);
				int errorCount = 0;
				
				if (m.getType() == MessageType.CHALLENGE) {
					if(!insertChallenge(connection, (Challenge)m)) errorCount++;
				} else if (m.getType() == MessageType.FRIEND_REQUEST) {
				} else if (m.getType() == MessageType.NOTE) {
				} else {
					throw new Exception("Invalid Activty Type Value");
				}
				
				//Result
				if (errorCount == 0) {
					connection.commit();	//commit transaction
					return new InsertResponse<String>
						(messageId, DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
				} else {
					throw new SQLException("Unable to insert record");
				}
			} else {
				throw new SQLException("Unable to get primary key for activity.");
			}
		} catch (SQLException se) {
			se.printStackTrace();
			if(connection != null) {
				try {
					System.err.print("Transaction is being rolled back");
					connection.rollback();
				} catch (SQLException sqe) {
					se.printStackTrace();
				}
			}
			return new InsertResponse<String>
				(null, DBResponse.Status.FAILURE, DBResponse.Cause.UNABLE_INSERT_OBJECT, null);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				connection.setAutoCommit(true);
			} catch (SQLException sqe) {
				sqe.printStackTrace();
			}
			DBUtils.closeStatement(ps);
			DBUtils.closeConnection(connection);
		}	
		return new InsertResponse<String>
			(null, DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, null);
	}
	
	
	private static String insertMessage(Connection con, Message m) throws SQLException {
		PreparedStatement ps = null;	
		try {
			String sql = 
				"INSERT INTO message (sender, receiver, date, body, message.read, messageType) " +
				" VALUES (?, ?, ?, ?, ?, ?)";
			ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			
			ps.setString(1, m.getSenderName());		
			ps.setString(2, m.getReceiverName());	
			ps.setTimestamp(3, new Timestamp(m.getDate().getTime()));
			ps.setString(4, m.getBody());
			ps.setBoolean(5, m.getRead());
			ps.setString(6, m.getType().toString());
			
			int rows = ps.executeUpdate();
			
			//Result
			if(rows > 0) {
				return DBUtils.getLongAutoGeneratedKey(ps);
			}
			return null;
		} finally {
			DBUtils.closeStatement(ps);
		}
	}
	
	private static boolean insertChallenge(Connection con, Challenge c) throws SQLException {
		PreparedStatement ps = null;	
		try {
			String sql = 
				"INSERT INTO challenge (messageId, quizId) " +
				" VALUES (?, ?)";
			ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			
			ps.setString(1, c.getMessageID());		
			ps.setString(2, c.getQuizID());	
			
			int rows = ps.executeUpdate();
			
			//Result
			if(rows > 0) {
				return true;
			}
			return false;
		} finally {
			DBUtils.closeStatement(ps);
		}
	}
	
	private static InsertResponse<String> addMessage_OLD(Message m) {
		PreparedStatement ps = null;
		Connection connection = null;		

		String sql = 
			"INSERT INTO message (sender, receiver, date, body, read, messageType) " +
			" VALUES (?, ?, ?, ?, ?, ?)";
		connection = DBConnection.getConnection();
		
		try {
			ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, m.getSenderName());		
			ps.setString(2, m.getReceiverName());	
			ps.setTimestamp(3, new Timestamp(m.getDate().getTime()));
			ps.setString(4, m.getBody());
			ps.setBoolean(5, m.getRead());
			ps.setString(6, m.getType().toString());
			
			int rows = ps.executeUpdate();
			if(rows > 0) {
				String id = DBUtils.getLongAutoGeneratedKey(ps);
				
				return new InsertResponse<String>
					(id, DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
			}
			return new InsertResponse<String>
				(null, DBResponse.Status.FAILURE, DBResponse.Cause.UNABLE_INSERT_OBJECT, null);
		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			DBUtils.closeStatement(ps);
			DBUtils.closeConnection(connection);
		}	
		return new InsertResponse<String>
			(null, DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, null);
	}
	
	/* done */
	public static InsertResponse<String> addAnnouncement(Announcement a) {
		PreparedStatement ps = null;
		Connection connection = null;		
		String message = null;

		//Query string
		String sql ="INSERT INTO announcement (username, text, date) " + 
					" VALUES (?, ?, ?)";
		connection = DBConnection.getConnection();
		try {
			ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, a.getCreatorID());		
			ps.setString(2, a.getText());	
			ps.setTimestamp(3, new Timestamp(a.getDate().getTime()));	
			
			int rows = ps.executeUpdate();
			if(rows > 0) {
				String id = DBUtils.getLongAutoGeneratedKey(ps);
				
				return new InsertResponse<String>
					(id, DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
			}
			return new InsertResponse<String>
				(null, DBResponse.Status.FAILURE, DBResponse.Cause.UNABLE_INSERT_OBJECT, null);
		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			DBUtils.closeStatement(ps);
			DBUtils.closeConnection(connection);
		}	
		return new InsertResponse<String>
			(null, DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, null);
	}
	
	// **************
	// Update methods
	// **************
	public static UpdateResponse markMessageRead(String messageId) {
		PreparedStatement ps = null;
		Connection connection = null;		

		//Query string
		String sql ="UPDATE message SET message.read = 1 WHERE messageId = ?";
		connection = DBConnection.getConnection();
		
		long longMessageId;
		if(Utils.isLongNumber(messageId)) {
			longMessageId = Utils.toLong(messageId);
		} else {
			String message = "messageId not a valid integer";
			return new UpdateResponse
				(DBResponse.Status.FAILURE, DBResponse.Cause.INVALID_QUERY_PARAMETER, message);
		}
		
		try {
			ps = connection.prepareStatement(sql);
			ps.setLong(1, longMessageId);	
			
			int rows = ps.executeUpdate();
			
			if(rows > 0) {
				return new UpdateResponse
					(DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
			}
			return new UpdateResponse
				(DBResponse.Status.FAILURE, DBResponse.Cause.NO_RESULT_FOUND, null);
		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			DBUtils.closeStatement(ps);
			DBUtils.closeConnection(connection);
		}	
		return new UpdateResponse
			(DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, null);
	}
	
	// **************
	// Delete methods
	// **************
	
	
	// **************
	// Assemble methods
	// **************
	protected static int assembleMessageList_OLD(ResultSet rs, List<Message> messages) throws SQLException {
		int errorCount = -1;
		while (rs.next()) {	
			Message message= new Message();
			if(setMessageFields(rs, message)) {
				if (errorCount == -1) errorCount = 0;	//Only set the first time
				messages.add(message);
			} else {
				if (errorCount == -1) errorCount = 0;  //Only set the first time
				errorCount++;
			}
		} 
		return errorCount;
	}
	
	protected static int assembleMessageList(ResultSet rs, List<Message> messages) throws SQLException {
		int errorCount = -1;
		Message message;
		while (rs.next()) {	
			switch(MessageType.valueOf(DBUtils.getString(rs, "message.messageType", null))) {
				case CHALLENGE:
					message = new Challenge();
					if(setChallengeFields(rs, (Challenge)message)) {
						if (errorCount == -1) errorCount = 0;	//Only set the first time
						messages.add(message);
					} else {
						if (errorCount == -1) errorCount = 0;  //Only set the first time
						errorCount++;
					}
					break;
				case NOTE:
					message = new Message();
					if(setNoteFields(rs, message)) {
						if (errorCount == -1) errorCount = 0;	//Only set the first time
						messages.add(message);
					} else {
						if (errorCount == -1) errorCount = 0;  //Only set the first time
						errorCount++;
					}
					break;
				case FRIEND_REQUEST:
					message = new Message();
					if(setFriendRequestFields(rs, message)) {
						if (errorCount == -1) errorCount = 0;	//Only set the first time
						messages.add(message);
					} else {
						if (errorCount == -1) errorCount = 0;  //Only set the first time
						errorCount++;
					}
					break;
				default:
					break;
			}
		}
		return errorCount;
	}
	
	protected static int assembleAnnouncementList(ResultSet rs, List<Announcement> annoucements) throws SQLException {
		int errorCount = -1;
		while (rs.next()) {	
			Announcement annoucement= new Announcement();
			if(setAnnouncementFields(rs, annoucement)) {
				if (errorCount == -1) errorCount = 0;	//Only set the first time
				annoucements.add(annoucement);
			} else {
				if (errorCount == -1) errorCount = 0;  //Only set the first time
				errorCount++;
			}
		} 
		return errorCount;
	}
	
	// **************
	// Set fields methods (to compose objects)
	// **************
	private static boolean setChallengeFields(ResultSet rs, Challenge c) {
		setMessageFields(rs, c);
		
		try {
			c.setQuizID(DBUtils.getString(rs, "challenge.quizId", null));
			return true;
		}catch (SQLException se) {
			se.printStackTrace();
		}
		return false;
	}
	
	private static boolean setNoteFields(ResultSet rs, Message m) {
		setMessageFields(rs, m);
		
		try {
			m.setType(MessageType.valueOf(DBUtils.getString(rs, "message.messageType", null)));
			return true;
		}catch (SQLException se) {
			se.printStackTrace();
		}
		return false;
	}
	
	private static boolean setFriendRequestFields(ResultSet rs, Message m) {
		setMessageFields(rs, m);
		
		try {
			m.setType(MessageType.valueOf(DBUtils.getString(rs, "message.messageType", null)));
			return true;
		}catch (SQLException se) {
			se.printStackTrace();
		}
		return false;
	}
	private static boolean setMessageFields(ResultSet rs, Message message) {
		try {
			message.setSenderName(DBUtils.getString(rs, "message.sender", null));
			message.setReceiverName(DBUtils.getString(rs, "message.receiver", null));
			message.setDate(rs.getTimestamp("message.date"));
			message.setBody(DBUtils.getString(rs, "message.body", null));
			message.setRead(rs.getBoolean("message.read"));
	
			return true;
		} catch (SQLException se) {
			se.printStackTrace();
		}
		return false;
	}
	
	private static boolean setAnnouncementFields(ResultSet rs, Announcement a) {
		try {
			a.setCreatorID(DBUtils.getString(rs, "announcement.username", null));
			a.setText(DBUtils.getString(rs, "announcement.text", null));
			a.setDate(rs.getTimestamp("announcement.date"));
			return true;
		} catch (SQLException se) {
			se.printStackTrace();
		}
		return false;
	}
}
