package dao.business;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import model.Answer;
import model.Choice;
import model.FillInBlank;
import model.MultipleChoice;
import model.Question;
import model.Question.QuestionType;
import model.QuestionResponse;
import utils.Utils;
import dao.DBConnection;
import dao.DBResponse;
import dao.DBUtils;
import dao.DeleteResponse;
import dao.InsertResponse;
import dao.PreviousIds;
import dao.SelectListResponse;

public class QuestionDB {

	// **************
	// Get methods
	// **************
	/* Done */
	public static SelectListResponse<Question> getQuestions(String quizId) {
		ResultSet rs = null;
		PreparedStatement ps = null;
		Connection connection = null;
		
		String message = null;
		
		//Query string: IMPORTANT!!  HAS TO BE ORDERED BY PRIMARY KEYS (ALL PKS)!
		// Can use LEFT OUT JOIN because of total specialization 
		// (i.e. all subtypes belong to one and only one super-type
		String sql ="SELECT question.questionId AS questionId, questionType, questionNumber, picUrl, " +
					"	questionresponse.question, orderAnswers, questionresponse.numAnswers," +
					"	partOne, partTwo, fillinblank.numAnswers," +
					"	multiplechoice.numAnswers, isCheckAll, multiplechoice.isRandom, multiplechoice.question," +
					"	choiceId, isAnswer, choiceNumber, choice.text," +
					"	answer.answerId AS answerId, answerNumber, answervariation.text" +
					" FROM question" +
					" LEFT OUTER JOIN questionresponse" +
					"	ON question.questionId = questionresponse.questionId" +
					" LEFT OUTER JOIN multiplechoice" +
					"	ON question.questionId = multiplechoice.questionId" +
					" LEFT OUTER JOIN fillinblank" +
					"	ON question.questionId = fillinblank.questionId" +
					" LEFT OUTER JOIN choice" +
					"	ON question.questionId = choice.questionId" +
					" LEFT OUTER JOIN answer" +
					"	ON question.questionId = answer.questionId" +
					" LEFT OUTER JOIN answervariation" +
					"	ON answer.answerId = answervariation.answerId" +
					" WHERE question.quizId = ?" +
					" ORDER BY question.questionId ASC, " +
					"			answer.answerId ASC," +
					"			choice.choiceId ASC";
		connection = DBConnection.getConnection();
		
		long longQuizId;
		if(Utils.isLongNumber(quizId)) {
			longQuizId = Utils.toLong(quizId);
		} else {
			message = "QuizId not a valid integer";
			return new SelectListResponse<Question>
				(null, DBResponse.Status.FAILURE, DBResponse.Cause.INVALID_QUERY_PARAMETER, message);
		}
		
		try {
			ps = connection.prepareStatement(sql);
			ps.setLong(1, longQuizId);	
			
			rs = ps.executeQuery();

			//Assemble result
			List<Question> questions = new LinkedList<Question>();
			PreviousIds ids = new PreviousIds();	//Memory for last id encountered
			int errorCount = -1;
			while (rs.next()) {	
				if(assembleQuestion(rs, ids, (LinkedList<Question>)questions)) {
					if (errorCount == -1) errorCount = 0;	//Only set the first time
				} else {
					//For list returns
					if (errorCount == -1) errorCount = 0;  //Only set the first time
					errorCount++;
				}
			} 
			
			//Return result
			if(errorCount == 0) {
				return new SelectListResponse<Question>
					(questions, DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
			} else if (errorCount == -1){
				return new SelectListResponse<Question>
					(null, DBResponse.Status.FAILURE, DBResponse.Cause.NO_RESULT_FOUND, null);
			} else	{	//For returning lists: errorCount > 0, only some objects were able to be composed
				return new SelectListResponse<Question>
					(questions, DBResponse.Status.PARTIAL_SUCCESS, DBResponse.Cause.PARTIAL_DATA_ACCESS_FAILURE, null);
			}
	
		} catch (SQLException se) {
			message = "Class: QuestionDB, ExceptionType: SQLException, Message: " + se.getMessage();
			se.printStackTrace();
		} catch (Exception ex) {
			message = "Class: QuestionDB, ExceptionType: Exception, Message: " + ex.getMessage();
			ex.printStackTrace();
		} finally {
			DBUtils.closeResultSet(rs);
			DBUtils.closeStatement(ps);
			DBUtils.closeConnection(connection);
		}	
		return new SelectListResponse<Question>
			(null, DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, message);
	}
	
	/**
	 * Sets the state of an Question object with returned ResultSet data object 
	 *  if it needs to be determined whether a new Question instance is needed
	 */
	protected static boolean assembleQuestion(ResultSet rs, PreviousIds ids, LinkedList<Question> questions) {
		try {
			String id = DBUtils.getLong(rs, "questionId", -1l).toString();
			
			if (id.equals(ids.getPrevQuestionId())) {	//same object record as last record
				Question question = questions.getLast();
				
				//Question type
				if (question.getType() == QuestionType.QUESTION_RESPONSE) {
					getQuestionResponseAnswers(rs, ids, (QuestionResponse)question);
				} else if (question.getType() == QuestionType.FILL_IN_BLANK) {
					getFillInBlankAnswers(rs, ids, (FillInBlank)question);
				} else if (question.getType() == QuestionType.MULTIPLE_CHOICE) {
					getMultipleChoices(rs, ids, (MultipleChoice)question);
				} else {
					throw new Exception("Invalid Question Type Value");
				}
			} else {	//Start of new object record
				ids.setPrevQuestionId(id);
				
				//Question type
				String type = (DBUtils.getString(rs, "questionType", null));
				if (type.equals(QuestionType.QUESTION_RESPONSE.toString())) {
					QuestionResponse question  = new QuestionResponse();
					
					setQuestionResponseFields(rs, ids, question);
					getQuestionResponseAnswers(rs, ids, (QuestionResponse)question);
					
					questions.addLast(question);
				} else if (type.equals(QuestionType.FILL_IN_BLANK.toString())) {
					FillInBlank question = new FillInBlank();
					
					setFillInBlankFields(rs, ids, question);
					getFillInBlankAnswers(rs, ids, (FillInBlank)question);
					
					questions.addLast(question);
				} else if (type.equals(QuestionType.MULTIPLE_CHOICE.toString())) {
					MultipleChoice question = new MultipleChoice();

					setMultipleChoiceFields(rs, ids, question);
					getMultipleChoices(rs, ids, (MultipleChoice)question);
					
					questions.addLast(question);
				} else {
					throw new Exception("Invalid Question Type Value");
				}	
			}
			
			return true;
		} catch(SQLException se) { se.printStackTrace();	
		} catch(Exception e) { e.printStackTrace(); }
		return false;
	}
	
	private static void getQuestionResponseAnswers(ResultSet rs, PreviousIds ids, QuestionResponse question) throws Exception {
		//Get answers
		LinkedList<Answer> answers;
		if(question.getAnswers() instanceof LinkedList<?>) {
			answers = (LinkedList<Answer>)(question.getAnswers());
		} else {
			throw new Exception("Answers list is not a LinkedList<Answer> datatype");
		}
		
		if(!AnswerDB.assembleAnswer(rs, ids, answers)) {
			throw new Exception("Errors in assembling answer in question " + question.getQuestionID());
		}
	}
	
	private static void getFillInBlankAnswers(ResultSet rs, PreviousIds ids, FillInBlank question) throws Exception {
		//Get answers
		LinkedList<Answer> answers;
		if(question.getAnswers() instanceof LinkedList<?>) {
			answers = (LinkedList<Answer>)(question.getAnswers());
		} else {
			throw new Exception("Answers list is not a LinkedList<Answer> datatype");
		}
		
		if(!AnswerDB.assembleAnswer(rs, ids, answers)) {
			throw new Exception("Errors in assembling answer in question " + question.getQuestionID());
		}
	}
	
	private static void getMultipleChoices(ResultSet rs, PreviousIds ids, MultipleChoice question) throws Exception {
		//Get choices
		LinkedList<Choice> choices;
		if(question.getChoices() instanceof LinkedList<?>) {
			choices = (LinkedList<Choice>)question.getChoices();
		} else {
			throw new Exception("Choices list is not a LinkedList<Choice> datatype");
		}
		
		if(!ChoiceDB.assembleChoice(rs, choices)) {
			throw new Exception("Errors in assembling choices in question " + question.getQuestionID());
		}
	}
	
	private static void setQuestionResponseFields(ResultSet rs, PreviousIds ids, QuestionResponse question) throws Exception {
		//Fields common to all questions
		setCommonQuestionFields(rs, question);	
		
		//Fields specific to question-response
		question.setText(DBUtils.getString(rs, "questionresponse.question", null));
		question.setOrdered(DBUtils.getBoolean(rs, "orderAnswers", false));
		question.setNumAnswers(DBUtils.getInteger(rs, "questionresponse.numAnswers", null));
		
	}	
	
	private static void setFillInBlankFields(ResultSet rs, PreviousIds ids, FillInBlank question) throws SQLException {
		//Fields common to all questions
		setCommonQuestionFields(rs, question);
		
		//Fields specific to fill in blank
		question.setText1(DBUtils.getString(rs, "partOne", null));
		question.setText2(DBUtils.getString(rs, "partTwo", null));
		question.setNumAnswers(DBUtils.getInteger(rs, "fillinblank.numAnswers", null));
	}
	
	private static void setMultipleChoiceFields(ResultSet rs, PreviousIds ids, MultipleChoice question) throws SQLException {
		//Fields common to all questions
		setCommonQuestionFields(rs, question);
		
		//Fields specific to multiple choice
		question.setText(DBUtils.getString(rs, "multiplechoice.question", null));
		question.setNumAnswers(DBUtils.getInteger(rs, "multiplechoice.numAnswers", null));
		question.setSingleAnswer(DBUtils.getBoolean(rs, "isCheckAll", false));
		question.setRandomized(DBUtils.getBoolean(rs, "multiplechoice.isRandom", true));
	}
	
	private static void setCommonQuestionFields(ResultSet rs, Question question) throws SQLException {
			question.setQuestionID((DBUtils.getLong(rs, "questionid", -1l)).toString());
			question.setQuestionNumber((DBUtils.getLong(rs, "questionNumber", 0l)).toString());
			question.setPictureUrl((DBUtils.getString(rs, "picUrl", null)));
	}


	// **************
	// Insert methods
	// **************

	/**
	 * Insert list of questions in a transaction and batch
	 * @param questions list of questions to add to database
	 * @param con JDBC connection object holding the transaction lock
	 * @param quizId Foreign key to the questions
	 * @return count of rows successfully inserted
	 */
	/* Done */
	public static InsertResponse<Integer> addQuestions(List<Question> questions, Connection con, String quizId) {
		PreparedStatement ps = null;			//Question
		
		try {
			//Common to question fields
			String sql = 
				"INSERT INTO question (quizId, questionType, questionNumber, picUrl)" + 
				" VALUES (?, ?, ?, ?)";
			ps = con.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
			
			int rowCount = 0;
			int errorCount = 0;
			
			for(Question question: questions) {
				DBUtils.setStringToLong(ps, 1, quizId);	
				ps.setString(2, question.getType().toString());
				DBUtils.setStringToLong(ps, 3, question.getQuestionNumber());
				ps.setString(4, question.getPictureURL());
				
				int rows = ps.executeUpdate();
				String questionId = DBUtils.getLongAutoGeneratedKey(ps);
				
				InsertResponse<Integer> insertResult;
				if(questionId != null) {			//Key successfully generated
					rowCount = rowCount + rows;		//Running count of question successful inserted
					question.setQuestionID(questionId);
					
					//Question type
					if (question.getType() == QuestionType.QUESTION_RESPONSE) {
						if(!insertQuestionResponse(con, (QuestionResponse)question)) errorCount++;
						
						//Insert Answers
						insertResult = 
							AnswerDB.addAnswers(((QuestionResponse)question).getAnswers(), con, questionId);
					} else if (question.getType() == QuestionType.FILL_IN_BLANK) {
						if(!insertFillInBlank(con, (FillInBlank)question)) errorCount++;
						
						//Insert Answers
						insertResult = 
							AnswerDB.addAnswers(((FillInBlank)question).getAnswers(), con, questionId);
					} else if (question.getType() == QuestionType.MULTIPLE_CHOICE) {
						if(!insertMultipleChoice(con, (MultipleChoice)question)) errorCount++;
						
						//Insert Choices
						insertResult = 
							ChoiceDB.addChoices(((MultipleChoice)question).getChoices(), con, questionId);
					} else {
						throw new Exception("Invalid Question Type Value");
					}
					
					//Process insert answers and choices response
					if (insertResult.getStatus() != DBResponse.Status.SUCCESS) {
						if(insertResult.getStatus() == DBResponse.Status.FAILURE) {
							throw new Exception("Insert not successful");
						} else if(insertResult.getStatus() == DBResponse.Status.PARTIAL_SUCCESS) {
							errorCount++;
						}
					}
				} else {
					errorCount++;	//row wasn't inserted
				}
				
			}  //loop questions
			
			//CONDITIONS FOR SUCCESS
			if (errorCount == 0) {	
					return new InsertResponse<Integer>
						(rowCount, DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
			} else {
				if(rowCount == 0) {
					return new InsertResponse<Integer>
						(null, DBResponse.Status.FAILURE, DBResponse.Cause.UNABLE_INSERT_OBJECT, null);
				}
				return new InsertResponse<Integer>
					(rowCount, DBResponse.Status.PARTIAL_SUCCESS, DBResponse.Cause.UNABLE_INSERT_OBJECT, null);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			DBUtils.closeStatement(ps);
		}	
		return new InsertResponse<Integer>
			(null, DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, null);
	}
	
	private static boolean insertQuestionResponse(Connection con, QuestionResponse q) throws SQLException {
		PreparedStatement ps = null;	
		try {
			String sql = 
				"INSERT INTO questionresponse (questionId, question, numAnswers, orderAnswers)" + 
				" VALUES (?, ?, ?, ?)";
			ps = con.prepareStatement(sql);
			
			DBUtils.setStringToLong(ps, 1, q.getQuestionID());	
			ps.setString(2, q.getText());
			ps.setInt(3, q.getNumAnswers());
			ps.setBoolean(4, q.isOrdered());
			
			int rows= ps.executeUpdate();
			if(rows > 0) {
				return true;
			}
			return false;
		} finally {
			DBUtils.closeStatement(ps);
		}
	}
	
	private static boolean insertFillInBlank(Connection con, FillInBlank q) throws SQLException {
		PreparedStatement ps = null;	
		try {
			String sql = 
				"INSERT INTO fillinblank (questionId, partOne, partTwo, numAnswers)" + 
				" VALUES (?, ?, ?, ?)";
			ps = con.prepareStatement(sql);
			
			DBUtils.setStringToLong(ps, 1, q.getQuestionID());	
			ps.setString(2, q.getText1());
			ps.setString(3, q.getText2());
			ps.setInt(4, q.getNumAnswers());
			
			int rows= ps.executeUpdate();
			if(rows > 0) {
				return true;
			}
			return false;
		} finally {
			DBUtils.closeStatement(ps);
		}
	}
	
	private static boolean insertMultipleChoice(Connection con, MultipleChoice q) throws SQLException {
		PreparedStatement ps = null;	
		try {
			String sql = 
				"INSERT INTO multiplechoice (questionId, question, numAnswers, isCheckAll, isRandom)" + 
				" VALUES (?, ?, ?, ?, ?)";
			ps = con.prepareStatement(sql);
			
			DBUtils.setStringToLong(ps, 1, q.getQuestionID());	
			ps.setString(2, q.getText());
			ps.setInt(3, q.getNumAnswers());
			ps.setBoolean(4, q.isSingleAnswer());
			ps.setBoolean(5, q.isRandomized());
			
			int rows= ps.executeUpdate();
			if(rows > 0) {
				return true;
			}
			return false;
		} finally {
			DBUtils.closeStatement(ps);
		}
	}
	
	/* Done */
	public static InsertResponse<String> addQuestion(String quizId, String questionType, long questionNumber, String picUrl) {
		PreparedStatement ps = null;
		Connection connection = null;		

		//Query string
		String sql = 
			"INSERT INTO question (quizId, questionType, questionNumber, picUrl)" + 
			" VALUES (?, ?, ?, ?)";
		
		connection = DBConnection.getConnection();
		try {
			ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, quizId);		
			ps.setString(2, questionType);
			ps.setLong(3, questionNumber);
			ps.setString(4, picUrl);
			
			int rows = ps.executeUpdate();
			String newId = DBUtils.getLongAutoGeneratedKey(ps);
			
			//Assemble result
			if (rows >= 1) {	
				if(newId != null) {
					return new InsertResponse<String>
						(newId, DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
				} else {
					return new InsertResponse<String>
						(null, DBResponse.Status.FAILURE, DBResponse.Cause.UNABLE_TO_GET_KEY, null);
				}
			} else {
				return new InsertResponse<String>
					(null, DBResponse.Status.FAILURE, DBResponse.Cause.UNABLE_INSERT_OBJECT, null);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			DBUtils.closeStatement(ps);
			DBUtils.closeConnection(connection);
		}	
		return new InsertResponse<String>
			(null, DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, null);
	}
	
	/* optional */
	public static InsertResponse<String> addQuestion(Question question) {
		return new InsertResponse<String>
			(null, DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, null);
	}
	
	// **************
	// Update methods
	// **************
	
	
	// **************
	// Delete methods
	// **************
	public static DeleteResponse removeQuestion(String questionId) {
		return new DeleteResponse(DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
	}
	
	
	// **************
	// Assemble methods
	// **************
	
	
	
	// **************
	// Set fields methods (to compose objects)
	// **************
}
