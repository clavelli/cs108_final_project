package dao.business;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.List;

import utils.Utils;

import model.Achievement;
import model.Activity;
import model.Activity.ActivityType;
import model.Creation;
import model.Result;
import dao.DBConnection;
import dao.DBResponse;
import dao.DBUtils;
import dao.DeleteResponse;
import dao.InsertResponse;

public class ActivityDB {

	public static DeleteResponse deleteCreation(String quizId) {
		PreparedStatement ps = null;
		Connection connection = null;		

		//Query string
		String sql ="DELETE FROM creation WHERE creation.quizId = ? ";
		connection = DBConnection.getConnection();
		
		long longQuizId;
		if(Utils.isLongNumber(quizId)) {
			longQuizId = Utils.toLong(quizId);
		} else {
			String message = "QuizId not a valid integer";
			return new DeleteResponse
				(DBResponse.Status.FAILURE, DBResponse.Cause.INVALID_QUERY_PARAMETER, message);
		}
		
		try {
			ps = connection.prepareStatement(sql);
			ps.setLong(1, longQuizId);			
			
			int rows = ps.executeUpdate();
			
			if(rows > 0) {
				return new DeleteResponse
					(DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
			}
			return new DeleteResponse
				(DBResponse.Status.FAILURE, DBResponse.Cause.NO_RESULT_FOUND, null);
		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			DBUtils.closeStatement(ps);
			DBUtils.closeConnection(connection);
		}	
		return new DeleteResponse
		(DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, null);
	}
	
	public static DeleteResponse deleteResult(String quizId) {
		PreparedStatement ps = null;
		Connection connection = null;		

		//Query string
		String sql ="DELETE FROM result WHERE result.quizId = ? ";
		connection = DBConnection.getConnection();
		
		long longQuizId;
		if(Utils.isLongNumber(quizId)) {
			longQuizId = Utils.toLong(quizId);
		} else {
			String message = "QuizId not a valid integer";
			return new DeleteResponse
				(DBResponse.Status.FAILURE, DBResponse.Cause.INVALID_QUERY_PARAMETER, message);
		}
		
		try {
			ps = connection.prepareStatement(sql);
			ps.setLong(1, longQuizId);			
			
			int rows = ps.executeUpdate();
			
			if(rows > 0) {
				return new DeleteResponse
					(DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
			}
			return new DeleteResponse
				(DBResponse.Status.FAILURE, DBResponse.Cause.NO_RESULT_FOUND, null);
		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			DBUtils.closeStatement(ps);
			DBUtils.closeConnection(connection);
		}	
		return new DeleteResponse
		(DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, null);
	}
	
	
	
	// **************
	// Get methods
	// **************
	
	
	// **************
	// Insert methods
	// **************
	public static InsertResponse<String> addActivity (Activity activity) {
		PreparedStatement ps = null;			
		Connection connection = null;		
		String message = null;

		connection = DBConnection.getConnection();
		
		try {
			connection.setAutoCommit(false);	//Turn auto-commit off for transaction commit
			
			//Activity table
			String activityId = insertActivity(connection, activity);
			
			//Activity type
			InsertResponse<Integer> insertResult;
			if(activityId != null) {
				activity.setActivityID(activityId);
				int errorCount = 0;
				
				if (activity.getType() == ActivityType.ACHIEVEMENT) {
					if(!insertAchievement(connection, (Achievement)activity)) errorCount++;
				} else if (activity.getType() == ActivityType.CREATION) {
					if(!insertCreation(connection, (Creation)activity)) errorCount++;
				} else if (activity.getType() == ActivityType.RESULT) {
					if(!insertResult(connection, (Result)activity)) errorCount++;
				} else {
					throw new Exception("Invalid Activty Type Value");
				}
				
				//Result
				if (errorCount == 0) {
					connection.commit();	//commit transaction
					return new InsertResponse<String>
						(activityId, DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
				} else {
					throw new SQLException("Unable to insert record");
				}
			} else {
				throw new SQLException("Unable to get primary key for activity.");
			}
		} catch (SQLException se) {
			se.printStackTrace();
			if(connection != null) {
				try {
					System.err.print("Transaction is being rolled back");
					connection.rollback();
				} catch (SQLException sqe) {
					se.printStackTrace();
				}
			}
			return new InsertResponse<String>
				(null, DBResponse.Status.FAILURE, DBResponse.Cause.UNABLE_INSERT_OBJECT, null);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				connection.setAutoCommit(true);
			} catch (SQLException sqe) {
				sqe.printStackTrace();
			}
			DBUtils.closeStatement(ps);
			DBUtils.closeConnection(connection);
		}	
		return new InsertResponse<String>
			(null, DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, message);
	}
	
	
	public static String insertActivity(Connection con, Activity activity) throws SQLException {
		PreparedStatement ps = null;	
		try {
			String sql = 
				"INSERT INTO activity (username, date, activityType)" + 
				" VALUES (?, ?, ?)";
			ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			
			ps.setString(1, activity.getUsername());
			ps.setTimestamp(2, new Timestamp(activity.getDate().getTime()));
			ps.setString(3, activity.getType().toString());
			
			int rows = ps.executeUpdate();
			
			//Result
			if(rows > 0) {
				return DBUtils.getLongAutoGeneratedKey(ps);
			}
			return null;
		} finally {
			DBUtils.closeStatement(ps);
		}
	}
	
	public static boolean insertAchievement(Connection con, Achievement achievement) throws SQLException {
		PreparedStatement ps = null;	
		try {
			String sql = 
				"INSERT INTO achievement (activityId, name, description)" + 
				" VALUES (?, ?, ?)";
			ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			
			DBUtils.setStringToLong(ps, 1, achievement.getActivityId());
			ps.setString(2, achievement.getText());
			ps.setString(3, achievement.getDescription());
			
			int rows = ps.executeUpdate();
			
			//Result
			if(rows > 0) {
				return true;
			}
			return false;
		} finally {
			DBUtils.closeStatement(ps);
		}
	}
	
	public static boolean insertCreation(Connection con, Creation creation) throws SQLException {
		PreparedStatement ps = null;	
		try {
			String sql = 
				"INSERT INTO creation (activityId, quizId)" + 
				" VALUES (?, ?)";
			ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			
			DBUtils.setStringToLong(ps, 1, creation.getActivityId());
			DBUtils.setStringToLong(ps, 2, creation.getQuizID());
	
			int rows = ps.executeUpdate();
			
			//Result
			if(rows > 0) {
				return true;
			}
			return false;
		} finally {
			DBUtils.closeStatement(ps);
		}
	}
	
	private static boolean insertResult(Connection con, Result result) throws SQLException {
		PreparedStatement ps = null;	
		try {
			String sql = 
				"INSERT INTO result (activityId, quizId, score, elapsedTime)" + 
				" VALUES (?, ?, ?, ?)";
			ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			
			DBUtils.setStringToLong(ps, 1, result.getActivityId());
			ps.setString(2, result.getQuizID());
			ps.setDouble(3, result.getScore());
			ps.setLong(4, result.getElapsedTime());
			
			int rows = ps.executeUpdate();
			
			//Result
			if(rows > 0) {
				return true;
			}
			return false;
		} finally {
			DBUtils.closeStatement(ps);
		}
	}

	// **************
	// Assemble methods
	// **************
	protected static int assembleActivityList(ResultSet rs, List<Activity> activities) throws SQLException {
		int errorCount = -1;
		Activity activity;
		while (rs.next()) {	
			switch(ActivityType.valueOf(DBUtils.getString(rs, "activity.activityType", null))) {
				case ACHIEVEMENT:
					activity = new Achievement();
					if(setAchievementFields(rs, (Achievement)activity)) {
						if (errorCount == -1) errorCount = 0;	//Only set the first time
						activities.add(activity);
					} else {
						if (errorCount == -1) errorCount = 0;  //Only set the first time
						errorCount++;
					}
					break;
				case CREATION:
					activity = new Creation();
					if(setCreationFields(rs, (Creation)activity)) {
						if (errorCount == -1) errorCount = 0;	//Only set the first time
						activities.add(activity);
					} else {
						if (errorCount == -1) errorCount = 0;  //Only set the first time
						errorCount++;
					}
					
					break;
				case RESULT:
					activity = new Result();
					if(setResultFields(rs, (Result)activity)) {
						if (errorCount == -1) errorCount = 0;	//Only set the first time
						activities.add(activity);
					} else {
						if (errorCount == -1) errorCount = 0;  //Only set the first time
						errorCount++;
					}
					break;
				default:
					break;
			}
		}
		return errorCount;
	}
	
	
	protected static int assembleResultList(ResultSet rs, List<Result> results) throws SQLException {
		int errorCount = -1;
		while (rs.next()) {	
			Result result= new Result();
			if(setResultFields(rs, result)) {
				if (errorCount == -1) errorCount = 0;	//Only set the first time
				results.add(result);
			} else {
				if (errorCount == -1) errorCount = 0; 	//Only set the first time
				errorCount++;
			}
		} 
		return errorCount;
	}
	
	
	protected static int assembleAchievementList(ResultSet rs, List<Achievement> achievements) throws SQLException {
		int errorCount = -1;
		while (rs.next()) {	
			Achievement achievement= new Achievement();
			if(setAchievementFields(rs, achievement)) {
				if (errorCount == -1) errorCount = 0;	//Only set the first time
				achievements.add(achievement);
			} else {
				if (errorCount == -1) errorCount = 0;  //Only set the first time
				errorCount++;
			}
		} 
		return errorCount;
	}
	
	
	protected static int assembleCreationList(ResultSet rs, List<Creation> creations) throws SQLException {
		int errorCount = -1;
		while (rs.next()) {	
			Creation creation= new Creation();
			if(setCreationFields(rs, creation)) {
				if (errorCount == -1) errorCount = 0;	//Only set the first time
				creations.add(creation);
			} else {
				if (errorCount == -1) errorCount = 0;  //Only set the first time
				errorCount++;
			}
		} 
		return errorCount;
	}
	
	
	// **************
	// Set fields methods (to compose objects)
	// **************
	private static boolean setResultFields(ResultSet rs, Result result) {
		try {
			setActivityFields(rs, result);
			
			result.setQuizID(DBUtils.getLong(rs, "result.quizId", -1l).toString());
			result.setScore(DBUtils.getDouble(rs, "result.score", null));
			result.setElapsedTime(DBUtils.getLong(rs, "result.elapsedTime", null));
			
			result.setQuizTitle(DBUtils.getString(rs, "quizresult.title", null));
			return true;
		} catch (SQLException se) {
			se.printStackTrace();
		}
		return false;
	}
	
	
	private static boolean setAchievementFields(ResultSet rs, Achievement achievement) {
		try {
			setActivityFields(rs, achievement);
			
			achievement.setName(DBUtils.getString(rs, "achievement.name", null));
			achievement.setDescription(DBUtils.getString(rs, "achievement.description", null));

			return true;
		} catch (SQLException se) {
			se.printStackTrace();
		}
		return false;
	}
	
	
	private static boolean setCreationFields(ResultSet rs, Creation creation) {
		try {
			setActivityFields(rs, creation);
			
			creation.setQuizTitle(DBUtils.getString(rs, "quizcreation.title", null));
			creation.setQuizID(DBUtils.getLong(rs, "creation.quizId", -1l).toString());
			return true;
		} catch (SQLException se) {
			se.printStackTrace();
		}
		return false;
	}
	
	private static boolean setActivityFields(ResultSet rs, Activity activity) {
		try {
			activity.setActivityID(DBUtils.getLong(rs, "activity.activityId", -1l).toString());
			activity.setDate(new Timestamp(rs.getTimestamp("activity.date").getTime()));
			activity.setUsername(DBUtils.getString(rs, "activity.username", null));
			activity.setActivityType(ActivityType.valueOf(DBUtils.getString(rs, "activity.activityType", null)));
			return true;
		} catch (SQLException se) {
			se.printStackTrace();
		}
		return false;
	}
}
