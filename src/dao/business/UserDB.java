package dao.business;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import model.Achievement;
import model.Activity;
import model.Creation;
import model.Result;
import model.User;
import utils.Utils;
import dao.DBConnection;
import dao.DBResponse;
import dao.DBUtils;
import dao.DeleteResponse;
import dao.InsertResponse;
import dao.SelectListResponse;
import dao.SelectResponse;
import dao.UpdateResponse;

public class UserDB {
	
	// **************
	// Get methods
	// **************
	
	/**  
	 * Get the use identified by user id
	 * @param username of the use
	 * @return SelectResponse object with User in the data transfer object
	 */
	/* Done */
	public static SelectResponse<User> getUser(String username) {
		ResultSet rs = null;
		PreparedStatement ps = null;
		Connection connection = null;
		
		String message = null;
	
		//Query string
		String sql ="SELECT username, password, isAdmin " +
					" FROM user " +
					" WHERE username = ?";		
		connection = DBConnection.getConnection();
		try {
			ps = connection.prepareStatement(sql);
			ps.setString(1, username);		
			
			rs = ps.executeQuery();
			
			//Assemble result
			if (rs.next()) {		
				String password = rs.getString(2);
				boolean isAdmin = rs.getBoolean(3);
				
				return new SelectResponse<User>
					(new User(username, password, isAdmin), DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
			} else {
				//System.out.println("Query failed");
				return new SelectResponse<User>
					(null, DBResponse.Status.FAILURE, DBResponse.Cause.NO_RESULT_FOUND, null);
			}
			
		} catch (SQLException se) {
			message = "Class: UserDB, ExceptionType: SQLException, Message: " + se.getMessage();
			se.printStackTrace();
		} catch (Exception ex) {
			message = "Class: UserDB, ExceptionType: Exception, Message: " + ex.getMessage();
			ex.printStackTrace();
		} finally {
			DBUtils.closeResultSet(rs);
			DBUtils.closeStatement(ps);
			DBUtils.closeConnection(connection);
		}	
		return new SelectResponse<User>
			(null, DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, message);
	}
	
	/**
	 * Search for users, excludes self
	 * @param searchString
	 * @param currentUsername
	 * @return
	 */
	public static SelectListResponse<User> searchUsers(String searchString, String currentUsername) {
		ResultSet rs = null;
		PreparedStatement ps = null;
		Connection connection = null;
		
		String sql = "SELECT user.username, user.password, user.isAdmin" +
					" FROM user " +
					" WHERE LOWER(username) LIKE ? " +
					"	AND LOWER(username) <> ?";
		connection = DBConnection.getConnection();
		
		try {
			ps = connection.prepareStatement(sql);
			ps.setString(1, "%"+searchString+"%");
			ps.setString(2, currentUsername);		//excludes current user from result
			
			rs = ps.executeQuery();

			//Assemble 
			List<User> users = new ArrayList<User>();
			int errorCount = UserDB.assembleUsers(rs, users);
			
			if(errorCount == 0) {
				return new SelectListResponse<User>
					(users, DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
			} else if (errorCount == -1){
				return new SelectListResponse<User>
					(null, DBResponse.Status.FAILURE, DBResponse.Cause.NO_RESULT_FOUND, null);
			} else	{	//For returning lists: errorCount > 0, only some objects were able to be composed
				return new SelectListResponse<User>
					(users, DBResponse.Status.PARTIAL_SUCCESS, DBResponse.Cause.PARTIAL_DATA_ACCESS_FAILURE, null);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			DBUtils.closeResultSet(rs);
			DBUtils.closeStatement(ps);
			DBUtils.closeConnection(connection);
		}	
		return new SelectListResponse<User>
			(null, DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, null);
	}
	
	/**
	 * Get all registered users
	 * @return SelectListResponse<User>, contains List<User>
	 */
	/* done */
	public static SelectListResponse<User> searchUsers(String searchString) {
		ResultSet rs = null;
		PreparedStatement ps = null;
		Connection connection = null;
		
		String sql = "SELECT LOWER(user.username) AS 'user.username', user.password, user.isAdmin" +
					" FROM user " +
					" WHERE username LIKE ?";
		connection = DBConnection.getConnection();
		
		try {
			ps = connection.prepareStatement(sql);
			ps.setString(1, "%"+searchString+"%");
			
			rs = ps.executeQuery();

			//Assemble 
			List<User> users = new ArrayList<User>();
			int errorCount = UserDB.assembleUsers(rs, users);
			
			if(errorCount == 0) {
				return new SelectListResponse<User>
					(users, DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
			} else if (errorCount == -1){
				return new SelectListResponse<User>
					(null, DBResponse.Status.FAILURE, DBResponse.Cause.NO_RESULT_FOUND, null);
			} else	{	//For returning lists: errorCount > 0, only some objects were able to be composed
				return new SelectListResponse<User>
					(users, DBResponse.Status.PARTIAL_SUCCESS, DBResponse.Cause.PARTIAL_DATA_ACCESS_FAILURE, null);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			DBUtils.closeResultSet(rs);
			DBUtils.closeStatement(ps);
			DBUtils.closeConnection(connection);
		}	
		return new SelectListResponse<User>
			(null, DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, null);
	}
	
	/**
	 * Get the specified numbers of results of a user
	 * @param username of user
	 * @param count number results 
	 * @return SelectListResponse<Result>, contains List<Quiz>
	 */
	/* done */
	public static SelectListResponse<Result> getRecentResults(String username, int minutes, int count) {
		ResultSet rs = null;
		PreparedStatement ps = null;
		Connection connection = null;
		
		String sql = "SELECT  activity.activityId, activity.date, activity.username, activity.activityType, " +
				"			result.score, result.elapsedTime," +
				"			result.quizId, quizresult.title " +
				"	FROM result" +
				"	JOIN quiz quizresult" +
				"		ON result.quizId = quizresult.quizId " +
				"	JOIN activity" +
				"		ON result.activityId = activity.activityId " +
				"	WHERE  " +
				"		activity.date BETWEEN DATE_SUB(CURRENT_TIMESTAMP(), INTERVAL ? MINUTE) AND CURRENT_TIMESTAMP()" +
				"	AND activity.username = ? " +
				"	ORDER BY activity.date DESC " +
				"	LIMIT ? ";
		connection = DBConnection.getConnection();
		
		try {
			ps = connection.prepareStatement(sql);
			ps.setInt(1, minutes);
			ps.setString(2, username);
			ps.setInt(3, count);	
	
			rs = ps.executeQuery();

			//Assemble 
			List<Result> results = new ArrayList<Result>();
			int errorCount = ActivityDB.assembleResultList(rs, results);
			
			if(errorCount == 0) {
				return new SelectListResponse<Result>
					(results, DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
			} else if (errorCount == -1){
				return new SelectListResponse<Result>
					(null, DBResponse.Status.FAILURE, DBResponse.Cause.NO_RESULT_FOUND, null);
			} else	{	//For returning lists: errorCount > 0, only some objects were able to be composed
				return new SelectListResponse<Result>
					(results, DBResponse.Status.PARTIAL_SUCCESS, DBResponse.Cause.PARTIAL_DATA_ACCESS_FAILURE, null);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			DBUtils.closeResultSet(rs);
			DBUtils.closeStatement(ps);
			DBUtils.closeConnection(connection);
		}	
		return new SelectListResponse<Result>
			(null, DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, null);
	}
	
	/**
	 * Recent result for particular quiz and user
	 * @param username
	 * @param quizId
	 * @param minutes
	 * @return
	 */
	public static SelectListResponse<Result> getRecentResults(String username, String quizId, int minutes) {
		ResultSet rs = null;
		PreparedStatement ps = null;
		Connection connection = null;
		
		String sql = "SELECT  activity.activityId, activity.date, activity.username, activity.activityType, " +
				"			result.score, result.elapsedTime," +
				"			result.quizId, quizresult.title " +
				"	FROM result" +
				"	JOIN quiz quizresult" +
				"		ON result.quizId = quizresult.quizId " +
				"	JOIN activity" +
				"		ON result.activityId = activity.activityId " +
				"	WHERE result.quizId = ? " +
				"		AND activity.date BETWEEN DATE_SUB(CURRENT_TIMESTAMP(), INTERVAL ? MINUTE) AND CURRENT_TIMESTAMP()" +
				"		AND activity.username = ? " +
				"	ORDER BY activity.date DESC ";
		connection = DBConnection.getConnection();
		
		long longQuizId;
		if(Utils.isLongNumber(quizId)) {
			longQuizId = Utils.toLong(quizId);
		} else {
			String message = "QuizId not a valid integer";
			return new SelectListResponse<Result>
				(null, DBResponse.Status.FAILURE, DBResponse.Cause.INVALID_QUERY_PARAMETER, message);
		}
		
		try {
			ps = connection.prepareStatement(sql);
			ps.setLong(1, longQuizId);
			ps.setString(2, username);
			ps.setInt(3, minutes);	
	
			rs = ps.executeQuery();

			//Assemble 
			List<Result> results = new ArrayList<Result>();
			int errorCount = ActivityDB.assembleResultList(rs, results);
			
			if(errorCount == 0) {
				return new SelectListResponse<Result>
					(results, DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
			} else if (errorCount == -1){
				return new SelectListResponse<Result>
					(null, DBResponse.Status.FAILURE, DBResponse.Cause.NO_RESULT_FOUND, null);
			} else	{	//For returning lists: errorCount > 0, only some objects were able to be composed
				return new SelectListResponse<Result>
					(results, DBResponse.Status.PARTIAL_SUCCESS, DBResponse.Cause.PARTIAL_DATA_ACCESS_FAILURE, null);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			DBUtils.closeResultSet(rs);
			DBUtils.closeStatement(ps);
			DBUtils.closeConnection(connection);
		}	
		return new SelectListResponse<Result>
			(null, DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, null);
	}
	
	/**
	 * Top result for particular quiz and user
	 * @param username
	 * @param quizId
	 * @param count
	 * @return
	 */
	public static SelectListResponse<Result> getTopResults(String username, String quizId, int count) {
		ResultSet rs = null;
		PreparedStatement ps = null;
		Connection connection = null;
		
		String sql = "SELECT  activity.activityId, activity.date, activity.username, activity.activityType, " +
				"			result.score, result.elapsedTime," +
				"			result.quizId, quizresult.title " +
				"	FROM result" +
				"	JOIN quiz quizresult" +
				"		ON result.quizId = quizresult.quizId " +
				"	JOIN activity" +
				"		ON result.activityId = activity.activityId " +
				"	WHERE result.quizId = ? " + 
				"		AND activity.username = ? " +
				"	ORDER BY result.score DESC, result.elapsedTime ASC" +
				"	LIMIT ? ";
		connection = DBConnection.getConnection();
		
		long longQuizId;
		if(Utils.isLongNumber(quizId)) {
			longQuizId = Utils.toLong(quizId);
		} else {
			String message = "QuizId not a valid integer";
			return new SelectListResponse<Result>
				(null, DBResponse.Status.FAILURE, DBResponse.Cause.INVALID_QUERY_PARAMETER, message);
		}
		
		try {
			ps = connection.prepareStatement(sql);
			ps.setLong(1, longQuizId);	
			ps.setString(2, username);
			ps.setInt(3, count);		
	
			rs = ps.executeQuery();

			//Assemble 
			List<Result> results = new ArrayList<Result>();
			int errorCount = ActivityDB.assembleResultList(rs, results);
			
			if(errorCount == 0) {
				return new SelectListResponse<Result>
					(results, DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
			} else if (errorCount == -1){
				return new SelectListResponse<Result>
					(null, DBResponse.Status.FAILURE, DBResponse.Cause.NO_RESULT_FOUND, null);
			} else	{	//For returning lists: errorCount > 0, only some objects were able to be composed
				return new SelectListResponse<Result>
					(results, DBResponse.Status.PARTIAL_SUCCESS, DBResponse.Cause.PARTIAL_DATA_ACCESS_FAILURE, null);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			DBUtils.closeResultSet(rs);
			DBUtils.closeStatement(ps);
			DBUtils.closeConnection(connection);
		}	
		return new SelectListResponse<Result>
			(null, DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, null);
	}
	
	
	
	/**
	 * Get the specified numbers of Creation by a user
	 * @param username of user
	 * @param count number of Creations 
	 * @return SelectListResponse<Creation>, contains List<Quiz>
	 */
	/* done */
	public static SelectListResponse<Creation> getRecentCreated(String username, int minutes, int count) {
		ResultSet rs = null;
		PreparedStatement ps = null;
		Connection connection = null;
		
		String sql = "SELECT  activity.activityId, activity.date, activity.username, activity.activityType, " +
				"			creation.quizId, quizcreation.title " +
				"	FROM creation" +
				"	JOIN quiz quizcreation" +
				"		ON creation.quizId = quizcreation.quizId " +
				"	JOIN activity" +
				"		ON creation.activityId = activity.activityId " +
				"	WHERE  " +
				"		activity.date BETWEEN DATE_SUB(CURRENT_TIMESTAMP(), INTERVAL ? MINUTE) AND CURRENT_TIMESTAMP()" +
				"	AND activity.username = ? " +
				"	ORDER BY activity.date DESC " +
				"	LIMIT ? ";
		connection = DBConnection.getConnection();
		
		try {
			ps = connection.prepareStatement(sql);
			ps.setInt(1, minutes);
			ps.setString(2, username);
			ps.setInt(3, count);	
	
			rs = ps.executeQuery();

			//Assemble 
			List<Creation> creations = new ArrayList<Creation>();
			int errorCount = ActivityDB.assembleCreationList(rs, creations);
			
			if(errorCount == 0) {
				return new SelectListResponse<Creation>
					(creations, DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
			} else if (errorCount == -1){
				return new SelectListResponse<Creation>
					(null, DBResponse.Status.FAILURE, DBResponse.Cause.NO_RESULT_FOUND, null);
			} else	{	//For returning lists: errorCount > 0, only some objects were able to be composed
				return new SelectListResponse<Creation>
					(creations, DBResponse.Status.PARTIAL_SUCCESS, DBResponse.Cause.PARTIAL_DATA_ACCESS_FAILURE, null);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			DBUtils.closeResultSet(rs);
			DBUtils.closeStatement(ps);
			DBUtils.closeConnection(connection);
		}	
		return new SelectListResponse<Creation>
			(null, DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, null);
	}
	
	/**
	 * Get the specified numbers of achievements of a user
	 * @param username of user
	 * @param count number of achievements returned
	 * @return SelectListResponse<Achievement>, contains List<Achievement>
	 */
	/* done */
	public static SelectListResponse<Achievement> getAchievements(String username, int count) {
		ResultSet rs = null;
		PreparedStatement ps = null;
		Connection connection = null;
		
		String sql = "SELECT  activity.activityId, activity.date, activity.username, activity.activityType, " +
				"			achievement.name, achievement.description " +
				"	FROM achievement" +
				"	JOIN activity" +
				"		ON achievement.activityId = activity.activityId " +
				"	WHERE  " +
				"			activity.username = ? " +
				"	ORDER BY activity.date DESC " +
				"	LIMIT ? ";
		connection = DBConnection.getConnection();
		
		try {
			ps = connection.prepareStatement(sql);
			ps.setString(1, username);
			ps.setInt(2, count);	
	
			rs = ps.executeQuery();

			//Assemble 
			List<Achievement> achievements = new ArrayList<Achievement>();
			int errorCount = ActivityDB.assembleAchievementList(rs, achievements);
			
			if(errorCount == 0) {
				return new SelectListResponse<Achievement>
					(achievements, DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
			} else if (errorCount == -1){
				return new SelectListResponse<Achievement>
					(null, DBResponse.Status.FAILURE, DBResponse.Cause.NO_RESULT_FOUND, null);
			} else	{	//For returning lists: errorCount > 0, only some objects were able to be composed
				return new SelectListResponse<Achievement>
					(achievements, DBResponse.Status.PARTIAL_SUCCESS, DBResponse.Cause.PARTIAL_DATA_ACCESS_FAILURE, null);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			DBUtils.closeResultSet(rs);
			DBUtils.closeStatement(ps);
			DBUtils.closeConnection(connection);
		}	
		return new SelectListResponse<Achievement>
			(null, DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, null);
	}
	
	/**
	 * Get the specified numbers of activities of a user
	 * @param username of user
	 * @param count number of activities returned
	 * @return SelectListResponse<Activity>, contains List<Activity>
	 */
	/* done */
	public static SelectListResponse<Activity> getRecentActivities(String username, int minutes, int count) {
		ResultSet rs = null;
		PreparedStatement ps = null;
		Connection connection = null;
		
		String sql = 
				" SELECT  activity.activityId, activity.date, activity.username, activity.activityType," +
				"			creation.quizId, quizcreation.title," +
				"			result.quizId, quizresult.title, result.score, result.elapsedTime," +
				"			achievement.name, achievement.description" +	
				" FROM activity" +
				" LEFT OUTER JOIN creation" +
				" 	ON activity.activityId = creation.activityId" +
				" LEFT OUTER JOIN result" +
				" 	ON activity.activityId = result.activityId" +
				" LEFT OUTER JOIN achievement" +
				"	ON activity.activityId = achievement.activityId " +
				" LEFT OUTER JOIN quiz quizcreation" +
				" 	ON creation.quizId = quizcreation.quizId" +
				" LEFT OUTER JOIN quiz quizresult" +
				"	ON result.quizId = quizresult.quizId" +
				" WHERE  activity.date BETWEEN DATE_SUB(CURRENT_TIMESTAMP(), INTERVAL ? MINUTE) AND CURRENT_TIMESTAMP() " +
				"	AND activity.username = ? " +
				" ORDER BY activity.date DESC " +
				" LIMIT ? ";
		connection = DBConnection.getConnection();
		
		try {
			ps = connection.prepareStatement(sql);
			ps.setInt(1, minutes);
			ps.setString(2, username);
			ps.setInt(3, count);	
	
			rs = ps.executeQuery();

			//Assemble 
			List<Activity> activities = new ArrayList<Activity>();
			int errorCount = ActivityDB.assembleActivityList(rs, activities);
			
			if(errorCount == 0) {
				return new SelectListResponse<Activity>
					(activities, DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
			} else if (errorCount == -1){
				return new SelectListResponse<Activity>
					(null, DBResponse.Status.FAILURE, DBResponse.Cause.NO_RESULT_FOUND, null);
			} else	{	//For returning lists: errorCount > 0, only some objects were able to be composed
				return new SelectListResponse<Activity>
					(activities, DBResponse.Status.PARTIAL_SUCCESS, DBResponse.Cause.PARTIAL_DATA_ACCESS_FAILURE, null);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			DBUtils.closeResultSet(rs);
			DBUtils.closeStatement(ps);
			DBUtils.closeConnection(connection);
		}	
		return new SelectListResponse<Activity>
			(null, DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, null);
	}
	
	
	/**
	 * Get the specified numbers of activities of a user's friends
	 * @param username of user
	 * @param count number of activities 
	 * @return SelectListResponse<Activity>, contains List<Activity>
	 */
	/* done */
	public static SelectListResponse<Activity> getRecentFriendsActivities(List<String> friends, int minutes, int count) {
		ResultSet rs = null;
		PreparedStatement ps = null;
		Connection connection = null;
		
		String sql1 = 
				" SELECT  activity.activityId, activity.date, activity.username, activity.activityType," +
				"				creation.quizId, quizcreation.title," +
				"				result.quizId, quizresult.title, result.score, result.elapsedTime," +
				"				achievement.name, achievement.description" +
				" FROM activity" +
				" LEFT OUTER JOIN creation" +
				" 	ON activity.activityId = creation.activityId" +
				" LEFT OUTER JOIN result" +
				" 	ON activity.activityId = result.activityId" +
				" LEFT OUTER JOIN achievement" +
				"	ON activity.activityId = achievement.activityId " +
				" LEFT OUTER JOIN quiz quizcreation" +
				" 	ON creation.quizId = quizcreation.quizId" +
				" LEFT OUTER JOIN quiz quizresult" +
				"	ON result.quizId = quizresult.quizId" +
				" WHERE  activity.date BETWEEN DATE_SUB(CURRENT_TIMESTAMP(), INTERVAL ? MINUTE) AND CURRENT_TIMESTAMP() ";
		String sql2 = buildFriendsQueryFilter(friends);
		String sql3 = 	
				"	ORDER BY activity.date DESC " +
				"	LIMIT ? ";
	
		connection = DBConnection.getConnection();
		try {
			ps = connection.prepareStatement(sql1 + sql2 + sql3);
			
			ps.setInt(1, minutes);
			ps.setInt(2, count);	
	
			rs = ps.executeQuery();

			//Assemble 
			List<Activity> activities = new ArrayList<Activity>();
			int errorCount = ActivityDB.assembleActivityList(rs, activities);
			
			if(errorCount == 0) {
				return new SelectListResponse<Activity>
					(activities, DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
			} else if (errorCount == -1){
				return new SelectListResponse<Activity>
					(null, DBResponse.Status.FAILURE, DBResponse.Cause.NO_RESULT_FOUND, null);
			} else	{	//For returning lists: errorCount > 0, only some objects were able to be composed
				return new SelectListResponse<Activity>
					(activities, DBResponse.Status.PARTIAL_SUCCESS, DBResponse.Cause.PARTIAL_DATA_ACCESS_FAILURE, null);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			DBUtils.closeResultSet(rs);
			DBUtils.closeStatement(ps);
			DBUtils.closeConnection(connection);
		}	
		return new SelectListResponse<Activity>
			(null, DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, null);
	}
	
	private static String buildFriendsQueryFilter(List<String> friends){
		int count = friends.size();
		
		//Trying to get: 	AND activity.username IN ("kevin","anh")
		StringBuilder sb = new StringBuilder(" AND activity.username IN (\"");
		if (count == 0){
			sb.append("\"");
		} else {
			sb.append(friends.get(0) + "\"");
			for (int i = 1; i < count; i++) {
				sb.append(", \"" + friends.get(i) + "\"");
			}
		}	
		sb.append(") ");
		return sb.toString();
	}
	
	/**
	 * Get a list of friends for a user identified by username
	 * @param username
	 * @return
	 */
	/* done */
	public static SelectListResponse<String> getFriends(String username) {
		ResultSet rs = null;
		PreparedStatement ps = null;
		Connection connection = null;
	
		String sql = "SELECT friend.friend" +
					" FROM friend" + 
					" WHERE  LOWER(friend.username) = ? ";
		connection = DBConnection.getConnection();
		
		try {
			ps = connection.prepareStatement(sql);
			ps.setString(1, username);
			
			rs = ps.executeQuery();

			//Assemble 
			List<String> friends = new ArrayList<String>();
			String friend;
			int errorCount = -1;
			while (rs.next()) {	
				friend  = (DBUtils.getString(rs, "friend.friend", null));
		
				if(friend != null) {
					if (errorCount == -1) errorCount = 0;	//Only set the first time
					friends.add(friend);
				} else {
					//For list returns
					if (errorCount == -1) errorCount = 0;  //Only set the first time
					errorCount++;
				}
			} 
			
			if(errorCount == 0) {
				return new SelectListResponse<String>
					(friends, DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
			} else if (errorCount == -1){
				return new SelectListResponse<String>
					(null, DBResponse.Status.FAILURE, DBResponse.Cause.NO_RESULT_FOUND, null);
			} else	{	//For returning lists: errorCount > 0, only some objects were able to be composed
				return new SelectListResponse<String>
					(friends, DBResponse.Status.PARTIAL_SUCCESS, DBResponse.Cause.PARTIAL_DATA_ACCESS_FAILURE, null);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			DBUtils.closeResultSet(rs);
			DBUtils.closeStatement(ps);
			DBUtils.closeConnection(connection);
		}	
		return new SelectListResponse<String>
			(null, DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, null);
	}
	
	
	/**
	 * Get a list of friends with the username matching the specified string pattern
	 * @param username
	 * @return
	 */
	public static SelectListResponse<String> searchFriends(String username, String searchString) {
		ResultSet rs = null;
		PreparedStatement ps = null;
		Connection connection = null;
	
		String sql = "SELECT friend.friend " +
					" FROM friend" + 
					" WHERE  LOWER(friend.username) = ? " +
					" 	AND LOWER(friend.friend) LIKE ?";
		connection = DBConnection.getConnection();
		
		try {
			ps = connection.prepareStatement(sql);
			ps.setString(1, username);
			ps.setString(2, "%" + searchString + "%");
			
			rs = ps.executeQuery();

			//Assemble 
			List<String> friends = new ArrayList<String>();
			String friend;
			int errorCount = -1;
			while (rs.next()) {	
				friend  = (DBUtils.getString(rs, "friend.friend", null));
		
				if(friend != null) {
					if (errorCount == -1) errorCount = 0;	//Only set the first time
					friends.add(friend);
				} else {
					//For list returns
					if (errorCount == -1) errorCount = 0;  //Only set the first time
					errorCount++;
				}
			} 
			
			if(errorCount == 0) {
				return new SelectListResponse<String>
					(friends, DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
			} else if (errorCount == -1){
				return new SelectListResponse<String>
					(null, DBResponse.Status.FAILURE, DBResponse.Cause.NO_RESULT_FOUND, null);
			} else	{	//For returning lists: errorCount > 0, only some objects were able to be composed
				return new SelectListResponse<String>
					(friends, DBResponse.Status.PARTIAL_SUCCESS, DBResponse.Cause.PARTIAL_DATA_ACCESS_FAILURE, null);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			DBUtils.closeResultSet(rs);
			DBUtils.closeStatement(ps);
			DBUtils.closeConnection(connection);
		}	
		return new SelectListResponse<String>
			(null, DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, null);
	}
	
	// **************
	// Insert methods
	// **************
	/**
	 * Insert a creation of a quiz entry
	 * @param Creation object contain quizId and username
	 * @return InsertResponse<Quiz>, also contains db auto-generated id of Creation
	 */
	/* done */
	public static InsertResponse<String> addCreated(Creation creation) {
		//ActivityType type = ActivityType.CREATION
		return ActivityDB.addActivity(creation);
	}
	
	/**
	 * Insert a achievement entry
	 * @param Achievement object contain achievement info and username
	 * @return InsertResponse<Quiz>, also contains db auto-generated id of Achievement
	 * @return InsertResponse<Achievement>
	 */
	public static InsertResponse<String> addAchievement(Achievement achievement) {
		//ActivityType type = ActivityType.ACHIEVEMENT
		return ActivityDB.addActivity(achievement);
	}
	
	/**
	 * Insert a new user account
	 * @param username of new user
	 * @param password of new user
	 * @param isAdmin true if user is an admin, false if not an admin
	 * @return InsertResponse<>
	 */
	/* Done */
	public static InsertResponse<String> addAccount(String username, String password, boolean isAdmin) {
		PreparedStatement ps = null;
		Connection connection = null;		
		String message = null;

		//Query string
		String sql ="INSERT INTO user (`username`, `password`, `isAdmin`) " + 
					" VALUES (?, ?, ?)";
		connection = DBConnection.getConnection();
		try {
			ps = connection.prepareStatement(sql);
			ps.setString(1, username);		
			ps.setString(2, password);	
			ps.setBoolean(3, isAdmin);	
			
			int rows = ps.executeUpdate();
			
			//Assemble result
			if (rows == 1) {		
				//System.out.println(rs.getString(1) + ", " + rs.getString(2));
				return new InsertResponse<String>
					(username, DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
			} else {
				//System.out.println("Query failed");
				return new InsertResponse<String>
					(null, DBResponse.Status.FAILURE, DBResponse.Cause.UNABLE_INSERT_OBJECT, null);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			DBUtils.closeStatement(ps);
			DBUtils.closeConnection(connection);
		}	
		return new InsertResponse<String>
			(null, DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, null);
	}
	
	/**
	 * Insert a new friend
	 * @param username of user that is adding a friend
	 * @param friendUsername of user that is being added as a friend
	 * @return InsertResponse<Integer>
	 */
	public static InsertResponse<Integer> addFriend(String username, String friendUsername) {
		PreparedStatement ps = null;
		Connection connection = null;		

		//Query string
		String sql ="INSERT INTO friend (username_friend, username, friend, date) " + 
					" VALUES (?, ?, ?, ?)";
		connection = DBConnection.getConnection();
		try {
			connection.setAutoCommit(false);
			
			ps = connection.prepareStatement(sql);
			
			ps.setString(1, username + "_" + friendUsername);	
			ps.setString(2, username);		
			ps.setString(3, friendUsername);	
			ps.setTimestamp(4, new Timestamp((new Date()).getTime()));
			ps.addBatch();
			
			ps.setString(1, friendUsername + "_" + username);	
			ps.setString(2, friendUsername);		
			ps.setString(3, username);	
			ps.setTimestamp(4, new Timestamp((new Date()).getTime()));
			ps.addBatch();
		
			int[] updateCounts = ps.executeBatch();
			int rowCount = updateCounts.length;
			
			//Assemble result
			if(rowCount == 2) {	
				connection.commit();
				return new InsertResponse<Integer>
					(rowCount, DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
			} else {
				throw new SQLException();
			}
		} catch (SQLException se) {
			se.printStackTrace();
			if(connection != null) {
				try {
					System.err.print("Transaction is being rolled back");
					connection.rollback();
				} catch (SQLException sqe) {
					se.printStackTrace();
				}
			}
			return new InsertResponse<Integer>
				(null, DBResponse.Status.FAILURE, DBResponse.Cause.UNABLE_INSERT_OBJECT, null);
		} finally {
			DBUtils.closeStatement(ps);
			DBUtils.closeConnection(connection);
		}	
	}
	
	
	// **************
	// Update methods
	// **************
	/**
	 * Change user account status to "admin"
	 * @param username of account to modify
	 * @return UpdateResponse
	 */
	public static UpdateResponse setUserAdmin(String username, boolean isAdmin) {
		PreparedStatement ps = null;
		Connection connection = null;		

		//Query string
		String sql ="	UPDATE user SET isAdmin = ? WHERE username = ? ";
		connection = DBConnection.getConnection();
		try {
			ps = connection.prepareStatement(sql);
			ps.setBoolean(1, isAdmin);
			ps.setString(2, username);		
			int rows = ps.executeUpdate();
			
			if(rows > 0) {
				return new UpdateResponse
					(DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
			}
			return new UpdateResponse
				(DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			DBUtils.closeStatement(ps);
			DBUtils.closeConnection(connection);
		}	
		return new UpdateResponse
			(DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, null);
	}
	
	
	// **************
	// Delete methods
	// **************
	/**
	 * Delete user account
	 * @param username of account to be deleted
	 * @return DeleteResponse
	 */
	public static DeleteResponse deleteAccount(String username) {
		PreparedStatement ps = null;
		Connection connection = null;		

		//Query string
		String sql ="DELETE FROM user WHERE username = ? ";
		connection = DBConnection.getConnection();
		try {
			ps = connection.prepareStatement(sql);
			
			ps.setString(1, username);		
			int rows = ps.executeUpdate();
			
			if(rows > 0) {
				return new DeleteResponse
					(DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
			}
			return new DeleteResponse
				(DBResponse.Status.FAILURE, DBResponse.Cause.NO_RESULT_FOUND, null);
		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			DBUtils.closeStatement(ps);
			DBUtils.closeConnection(connection);
		}	
		return new DeleteResponse
		(DBResponse.Status.FAILURE, DBResponse.Cause.DATA_ACCESS_FAILURE, null);
	}
	
	/**
	 * Delete friend
	 * @param username of user making the deletion
	 * @param friendUsername user name of friend being deleted
	 * @return DeleteResponse
	 */
	public static DeleteResponse removeFriend(String username, String friendUsername){
		PreparedStatement ps = null;
		Connection connection = null;		

		//Query string
		String sql ="DELETE FROM friend WHERE username_friend = ? ";
		connection = DBConnection.getConnection();
		try {
			connection.setAutoCommit(false);
			
			ps = connection.prepareStatement(sql);
			
			ps.setString(1, username + "_" + friendUsername);		
			ps.addBatch();
			
			ps.setString(1, friendUsername + "_" + username);	
			ps.addBatch();
		
			int[] deleteCounts = ps.executeBatch();
			int rowCount = deleteCounts.length;
			
			//Assemble result
			if(rowCount == 2) {	
				connection.commit();
				return new DeleteResponse
					(DBResponse.Status.SUCCESS, DBResponse.Cause.NONE, null);
			} else {
				throw new SQLException();
			}
		} catch (SQLException se) {
			se.printStackTrace();
			if(connection != null) {
				try {
					System.err.print("Transaction is being rolled back");
					connection.rollback();
				} catch (SQLException sqe) {
					se.printStackTrace();
				}
			}
			return new DeleteResponse
				(DBResponse.Status.FAILURE, DBResponse.Cause.NO_RESULT_FOUND, null);
		} finally {
			DBUtils.closeStatement(ps);
			DBUtils.closeConnection(connection);
		}	
	}

	
	// **************
	// Assemble methods
	// **************
	protected static int assembleUsers(ResultSet rs, List<User> users) throws SQLException {
		int errorCount = -1;
		while (rs.next()) {	
			User user= new User();
			if(setUserFields(rs, user)) {
				if (errorCount == -1) errorCount = 0;	//Only set the first time
				users.add(user);
			} else {
				if (errorCount == -1) errorCount = 0;  //Only set the first time
				errorCount++;
			}
		} 
		return errorCount;
	}
	
	
	// **************
	// Set fields methods (to compose objects)
	// **************
	private static boolean setUserFields(ResultSet rs, User user) {
		try {
			user.setUsername(DBUtils.getString(rs, "user.username", null));
			user.setHashedPassword(DBUtils.getString(rs, "user.password", null));
			user.setAdmin(DBUtils.getBoolean(rs, "isAdmin", null));
			
			return true;
		} catch (SQLException se) {
			se.printStackTrace();
		}
		return false;
	}
	
}

