package dao;

/**
 * Object store response status database UPDATE 
 *
 */
public class UpdateResponse extends DBResponse {

	public UpdateResponse(Status status, Cause cause, String message) {
		super(status, cause, message);
	}

}
