package dao;


/**
 * Basic object store response status from database requests
 *
 */
public class DBResponse {
	
	private Status status;
	private Cause cause;
	private String message;
	
	public DBResponse(Status status, Cause cause, String message) {
		this.status = status;
		this.cause = cause;
		this.message = message;
	}
	
	public Status getStatus() {
		return status;
	}
	
	public Cause getCause() {
		return cause;
	}
	
	public String getMessage() {
		return message;
	}
	
	public enum Status {
		SUCCESS,
		PARTIAL_SUCCESS,
		FAILURE;
	}
	
	public enum Cause {
		NONE,
		SECURITY_ACCESS_VIOLATION,
		PARTIAL_DATA_ACCESS_FAILURE,
		DATA_ACCESS_FAILURE,
		INVALID_QUERY_PARAMETER,
		UNABLE_TO_GET_KEY,
		NO_RESULT_FOUND,
		UNABLE_INSERT_OBJECT;
	}
	
}
