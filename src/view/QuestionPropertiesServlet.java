package view;

import model.*;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class QuestionPropertiesServlet
 */
@WebServlet("/QuestionPropertiesServlet")
public class QuestionPropertiesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public QuestionPropertiesServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		Quiz q= (Quiz)session.getAttribute("quiz");
		RequestDispatcher dispatch;
		
		String submitType = request.getParameter("submit");
		if (submitType.equals("cancel")) {
			q.removeLastQuestion();
			dispatch = request.getRequestDispatcher("question-type.jsp");
			dispatch.forward(request, response);
			return;
		} else if (submitType.equals("submit")) {
			q.removeLastQuestion();
			dispatch = getServletContext().getRequestDispatcher("/SubmitQuizServlet");
			dispatch.forward(request, response);
			return;
		}
		
		String str = request.getParameter("image");
		if (str != null) {
			request.setAttribute("image", true);
		}
		int count = Integer.valueOf(request.getParameter("count"));
		request.setAttribute("count", count);
		switch(q.getLastQuestion().getType()) {
		
		case QUESTION_RESPONSE:
			QuestionResponse qrQuestion = (QuestionResponse)q.getLastQuestion();
			int numToAccept1 = Integer.valueOf(request.getParameter("numToAccept"));
			if (numToAccept1 > count) {
				request.setAttribute("fail", "a");
				request.setAttribute("type", "qr");
				dispatch = request.getRequestDispatcher("question-properties.jsp");
				dispatch.forward(request, response);
				return;
			}
			qrQuestion.setNumAnswers(numToAccept1);
			dispatch = request.getRequestDispatcher("create-qr.jsp");
			break;
		case FILL_IN_BLANK:
			FillInBlank fibQuestion = (FillInBlank)q.getLastQuestion();
			int numToAccept2 = Integer.valueOf(request.getParameter("numToAccept"));
			if (numToAccept2 > count) {
				request.setAttribute("fail", "a");
				request.setAttribute("type", "fib");
				dispatch = request.getRequestDispatcher("question-properties.jsp");
				dispatch.forward(request, response);
				return;
			}
			fibQuestion.setNumAnswers(numToAccept2);
			dispatch = request.getRequestDispatcher("create-fib.jsp");
			break;
		case MULTIPLE_CHOICE:
			MultipleChoice mcQuestion = (MultipleChoice)q.getLastQuestion();
			mcQuestion.setNumAnswers(count);
			if (request.getParameter("multi-answer") != null) {
				mcQuestion.setSingleAnswer(false);
			} else mcQuestion.setSingleAnswer(true);
			dispatch = request.getRequestDispatcher("create-mc.jsp");
			break;
		default: 
			return;
		}
		dispatch.forward(request, response);
		
	}

}
