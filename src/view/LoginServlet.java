package view;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.AccountManager;
import model.User;
import utils.Utils;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username = (String)request.getParameter("username");
		String password = (String)request.getParameter("password");
		
		User user = AccountManager.logIn(username, password);
		if (user == null) {
			// If username/password combination is not found
			RequestDispatcher dispatch = request.getRequestDispatcher("try-again.html");
			dispatch.forward(request, response);
		}
		else {
			HttpSession session = request.getSession();
			session.setAttribute("User", user);
			
			//Create cookie and send to user
			Cookie[] quizzle_cookies = request.getCookies();
			
			boolean hascookie = hasCookie(quizzle_cookies, username, password);
			if(!hascookie) { addCookie(username, password, response); };
			
			RequestDispatcher dispatch = request.getRequestDispatcher("home.jsp");
			dispatch.forward(request, response);
		}
	}

	private boolean hasCookie(Cookie[] cookies, String username, String password) {
		if(Utils.getCookieValue(cookies, "quizzle_username") == null) {
			return false;
		}
		return true;
	}
	private void addCookie(String username, String password, HttpServletResponse response) {
		Cookie usernameCookie = new Cookie("quizzle_username", username);
		Cookie passwordCookie = new Cookie("quizzle_password", password);
		
		usernameCookie.setMaxAge(60*60*24*365*1); //age of 1 year
		usernameCookie.setPath("/");	//Accessible to entire application
		response.addCookie(usernameCookie);
		
		passwordCookie.setMaxAge(60*60*24*365*1); //age of 1 year
		passwordCookie.setPath("/");	//Accessible to entire application
		response.addCookie(passwordCookie);
	}
}
