package view;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.*;

/**
 * Servlet implementation class TakeQuizServlet
 */
@WebServlet("/TakeQuizServlet")
public class TakeQuizServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Quiz q = (Quiz)request.getSession().getAttribute("Quiz");
		Question question = q.getCurrent();
		
		if (question.getType() == Question.QuestionType.FILL_IN_BLANK) {
			FillInBlank f = (FillInBlank)question;
			String[] attempts = request.getParameterValues(f.getQuestionID());
			request.getSession().setAttribute(f.getQuestionID(), attempts);
			f.correct(attempts);
		}
		else if (question.getType() == Question.QuestionType.MULTIPLE_CHOICE) {
			MultipleChoice mc = (MultipleChoice)question;
			List<Integer> attempts = new LinkedList<Integer>();
			String[] values = request.getParameterValues(mc.getQuestionID());
			request.getSession().setAttribute(mc.getQuestionID(), values);
			if (values != null) {
				for (int j = 0; j < values.length; j++) {
					attempts.add(Integer.parseInt(values[j]));
				}
			}
			mc.correct(attempts);
		}
		else {
			QuestionResponse qr = (QuestionResponse)question;
			String[] attempts = request.getParameterValues(qr.getQuestionID());
			request.getSession().setAttribute(qr.getQuestionID(), attempts);
			qr.correct(attempts);
		}
		
		if (q.isLastQuestion()) {
			q.finish();
			double score = q.grade();
			request.getSession().setAttribute("Score", score);
			RequestDispatcher dispatch = request.getRequestDispatcher("result.jsp");
			dispatch.forward(request, response);
		}
		else {
			q.nextQuestion();
			RequestDispatcher dispatch = request.getRequestDispatcher("quiz-question.jsp");
			dispatch.forward(request, response);
		}
	}
}
