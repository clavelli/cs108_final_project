package view;

import model.*;

import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 * Servlet implementation class CreateQuestionServlet
 */
@WebServlet("/CreateQuestionServlet")
public class CreateQuestionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreateQuestionServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		Quiz q = (Quiz)session.getAttribute("quiz");
		String submitType = request.getParameter("submit");
		RequestDispatcher dispatch;
		
		boolean cancel = (submitType.charAt(0) == 'x');
		boolean submit = (submitType.length() == 2);
		
		if (cancel) q.removeLastQuestion();
		else {
			
			String pictureURL = request.getParameter("image");
			int count = Integer.valueOf(request.getParameter("count"));
			String ordered = request.getParameter("ordered");
			
			switch(q.getLastQuestion().getType()) {
			case MULTIPLE_CHOICE:
				MultipleChoice mcQuestion = (MultipleChoice)q.getLastQuestion();
				mcQuestion.setPictureUrl(pictureURL);
				String text = request.getParameter("text");
				mcQuestion.setText(text);
				if (request.getParameter("ordered") != null) mcQuestion.setRandomized(false);
				else mcQuestion.setRandomized(true);
				if (mcQuestion.isSingleAnswer()) {
					int correctNumber = Integer.valueOf(request.getParameter("correct"));
					List<Choice> choices = new LinkedList<Choice>();
					for (int i = 0; i < count; i++) {
						boolean correct = false;
						if (i == correctNumber) correct = true;
						Choice choice = new Choice(request.getParameter("choice" + i), correct);
						choice.setChoiceNumber(i);
						choices.add(choice);
					}
					mcQuestion.setChoices(choices);
				} else {
					List<Choice> choices = new LinkedList<Choice>();
					for (int i = 0; i < count; i++) {
						boolean correct = false;
						if (request.getParameter("correct" + i) != null) correct = true;
						Choice choice = new Choice(request.getParameter("choice" + i), correct);
						choice.setChoiceNumber(i);
						choices.add(choice);
					}
					mcQuestion.setChoices(choices);
				}
				break;
			case FILL_IN_BLANK:
				FillInBlank fibQuestion = (FillInBlank)q.getLastQuestion();
				fibQuestion.setPictureUrl(pictureURL);
				if (ordered != null) fibQuestion.setOrdered(true);
				else fibQuestion.setOrdered(false);
				String text1 = request.getParameter("text1");
				fibQuestion.setText1(text1);
				String text2 = request.getParameter("text2");
				fibQuestion.setText2(text2);
				for (int i = 0; i < count; i++) {
					Answer answer = new Answer();
					answer.setAnswerNumber(i);
					String[] variations = parseVariations((String)request.getParameter("choice" + i));
					answer.addVariation(variations);
					fibQuestion.addAnswer(answer);
				}
				break;
			case QUESTION_RESPONSE:
				QuestionResponse qrQuestion = (QuestionResponse)q.getLastQuestion();
				qrQuestion.setPictureUrl(pictureURL);
				if (ordered != null) qrQuestion.setOrdered(true);
				else qrQuestion.setOrdered(false);
				String qrText = request.getParameter("text");
				qrQuestion.setText(qrText);
				for (int i = 0; i < count; i++) {
					Answer answer = new Answer();
					answer.setAnswerNumber(i);
					String[] variations = parseVariations((String)request.getParameter("choice" + i));
					answer.addVariation(variations);
					qrQuestion.addAnswer(answer);
				}
				break;
			default:
				break;
			}
		}
		if (submit) {
			dispatch = getServletContext().getRequestDispatcher("/SubmitQuizServlet");
			dispatch.forward(request, response);
			return;
		} else {
			dispatch = request.getRequestDispatcher("question-type.jsp");
			try {
				dispatch.forward(request, response);
			} catch (Exception e) {}
		}
	}
	
	private String[] parseVariations(String str) {
		String[] variations = str.split("[*]");
		for (int i = 0; i < variations.length; i++) {
			variations[i] = variations[i].trim();
		}
		return variations;
	}

}
