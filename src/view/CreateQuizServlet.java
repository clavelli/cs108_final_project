package view;
import model.Quiz;
import model.QuizManager;
import model.User;
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class CreateQuizServlet
 */
@WebServlet("/CreateQuizServlet")
public class CreateQuizServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreateQuizServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Quiz q = new Quiz();
		String title = request.getParameter("title");
		if (title.isEmpty()) {
			fail("title", request, response);
			return;
		}
		q.setTitle(title);
		String description = request.getParameter("description");
		if (description.isEmpty()) {
			fail("description", request, response);
			return;
		}
		q.setDescription(description);
		String randomized = request.getParameter("random");
		if (randomized != null) q.setRandom(true);
		else q.setRandom(false);
		
		String type = request.getParameter("type");
		if (type == null) {
			fail("Quiz Type", request, response);
			return;
		}
		if (type.equals("s")) {
			q.setSinglePage(true);
			q.setImmCorrection(false);
		} else {
			q.setSinglePage(false);
			if (type.equals("mi")) q.setImmCorrection(true);
			else q.setImmCorrection(false);
		}
		HttpSession session = request.getSession();
		User u = (User)session.getAttribute("User");
		String username = u.getUsername();
		q.setCreatorName(username);
		session.setAttribute("quiz", q);
		RequestDispatcher dispatch = request.getRequestDispatcher("question-type.jsp");
		try {
			dispatch.forward(request, response);
		} catch (Exception e) {}
	}	
	
	
	private void fail(String reason, HttpServletRequest request, HttpServletResponse response) {
		request.setAttribute("fail", reason);
		RequestDispatcher dispatch = request.getRequestDispatcher("create-quiz.jsp");
		try {
			dispatch.forward(request, response);
		} catch(Exception e) {}
	}

}
