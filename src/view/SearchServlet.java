package view;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Quiz;
import model.User;
import utils.Helper.SearchOptions;
import dao.DBResponse;
import dao.SelectListResponse;
import dao.business.QuizDB;
import dao.business.UserDB;

/**
 * Servlet implementation class SearchServlet
 */
@WebServlet("/SearchServlet")
public class SearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Retrieve search string from request
		String searchString = (String) request.getParameter("searchString").trim().toLowerCase();		
		//String selection = (String) request.getParameter("searchType").trim();
		String[] selection = request.getParameterValues("searchType");
	
		if((searchString!=null) && (!searchString.isEmpty())) {
			//Current user
			HttpSession session = request.getSession();
			User user = (User) session.getAttribute("User");
			String username =  user.getUsername().toLowerCase();
			
			
			switch(SearchOptions.valueOf(selection[0])) {
			//switch(SearchOptions.valueOf(selection)) {
			case FRIENDS:
				SelectListResponse<String> friendsResult = UserDB.searchFriends(username, searchString);
				if(friendsResult.getStatus() != DBResponse.Status.SUCCESS) {
					//System.out.println("Failure in retrieving friends");
				}
				List<String> friends = friendsResult.getDTOList();
				
				request.setAttribute("friends", friends);
				request.setAttribute("searchType", SearchOptions.FRIENDS);
				break;
			case QUIZZES:
				SelectListResponse<Quiz> quizzesResult = QuizDB.searchQuizzes(searchString);
				if(quizzesResult.getStatus() != DBResponse.Status.SUCCESS) {
					//Error handling
				}
				List<Quiz> quizzes = quizzesResult.getDTOList();
				
				request.setAttribute("quizzes", quizzes);
				request.setAttribute("searchType", SearchOptions.QUIZZES);
				
				break;
			case USERS:
				SelectListResponse<User> usersResult = UserDB.searchUsers(searchString, username);
				if(usersResult.getStatus() != DBResponse.Status.SUCCESS) {
					//Error handling
				}
				List<User> users = usersResult.getDTOList();
				
				request.setAttribute("users", users);
				request.setAttribute("searchType", SearchOptions.USERS);
				
				break;
			default:
				break;
			}
			
			RequestDispatcher dispatch = request.getRequestDispatcher("search_results.jsp");
			dispatch.forward(request, response);
		} else {
			response.sendRedirect("home.jsp");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
