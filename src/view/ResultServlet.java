package view;

import java.util.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.business.QuizDB;

import model.*;

/**
 * Servlet implementation class ResultServlet
 */
@WebServlet("/ResultServlet")
public class ResultServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Quiz q = (Quiz)request.getSession().getAttribute("Quiz");
		User u = (User)request.getSession().getAttribute("User");
		q.finish();
		
		List<Question> questions = q.getQuestions();
		
		for (int i = 0; i < questions.size(); i++) {
			Question question = questions.get(i);
			
			if (question.getType() == Question.QuestionType.FILL_IN_BLANK) {
				FillInBlank f = (FillInBlank)question;
				String[] attempts = request.getParameterValues(f.getQuestionID());
				f.correct(attempts);
			}
			else if (question.getType() == Question.QuestionType.MULTIPLE_CHOICE) {
				MultipleChoice mc = (MultipleChoice)question;
				List<Integer> attempts = new LinkedList<Integer>();
				String[] values = request.getParameterValues(mc.getQuestionID());
				if (values == null) {
					values = new String[0];
				}
				
				for (int j = 0; j < values.length; j++) {
					attempts.add(Integer.parseInt(values[j]));
				}
				mc.correct(attempts);
			}
			else {
				QuestionResponse qr = (QuestionResponse)question;
				String[] attempts = request.getParameterValues(qr.getQuestionID());
				qr.correct(attempts);
			}
		}
		
		double score = q.grade();
		request.getSession().setAttribute("Score", score);
		
		RequestDispatcher dispatch = request.getRequestDispatcher("result.jsp");
		dispatch.forward(request, response);
	}
}
