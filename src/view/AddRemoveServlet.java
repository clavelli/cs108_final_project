package view;

import model.*;
import java.io.IOException;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.business.UserDB;

/**
 * Servlet implementation class AddRemoveServlet
 */
@WebServlet("/AddRemoveServlet")
public class AddRemoveServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddRemoveServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		User user = (User)session.getAttribute("User");
		String submitType = request.getParameter("submit");
		String pageUsername = request.getParameter("pageUsername");
		if (submitType.equals("add")) {
			Message message = new Message(user.getUsername(), pageUsername);
			message.setType(Message.MessageType.FRIEND_REQUEST);
			message.setRead(false);
			message.send();
			
		} else if (submitType.equals("remove")) {
			UserDB.removeFriend(user.getUsername(), pageUsername);
			UserDB.removeFriend(pageUsername, user.getUsername());
		} else if (submitType.equals("delete")){ 
			UserDB.deleteAccount(user.getUsername());
		} else if (submitType.equals("promote")) {
			UserDB.setUserAdmin(pageUsername, true);
		}
		RequestDispatcher dispatch = request.getRequestDispatcher("home.jsp");
		dispatch.forward(request, response);
	}

}
