package model;

public class Challenge extends Message {
	private String quizID;
	
	public Challenge()  {
		// TODO Auto-generated constructor stub
		super(MessageType.CHALLENGE);
	}

	public void setQuizID(String quizID) {
		this.quizID = quizID;
	}

	public String getQuizID() {
		return quizID;
	}
	
	
}
