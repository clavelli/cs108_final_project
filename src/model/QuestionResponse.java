package model;

import java.util.LinkedList;
import java.util.List;

public class QuestionResponse extends Question {
	private String text;
	private List<Answer> answers;
	// not necessarily the size of the answers List. This is the number of answers to accept.
	// So name 5 states would have numAnswers = 5, but answers.size() = 50.
	private int numAnswers;
	
	// Specifies whether the answers need to be in their given order (name the first 
	// five presidents in order vs name the fifty states).
	private boolean ordered;
	
	public QuestionResponse() {
		super(QuestionType.QUESTION_RESPONSE);
		answers = new LinkedList<Answer>();	//anh, needs initialize
	}
	
	public QuestionResponse(String pictureURL, String questionID) {
		this();
		this.pictureURL = pictureURL;
		this.questionID = questionID;
	}
	
	public QuestionResponse(String pictureURL, String questionID, String text, List<Answer> answers, boolean ordered, int numAnswers) {
		this(pictureURL, questionID);
		this.text = text;
		this.answers = new LinkedList<Answer>(answers);
		this.ordered = ordered;
		this.numAnswers = numAnswers;
	}
	
	public String getText() { return text; }
	public List<Answer> getAnswers() { return answers; }
	public boolean isOrdered() { return ordered; }
	public int getNumAnswers() { return numAnswers; }
	
	public void setText(String text) { this.text = text; }
	public void setOrdered(boolean ordered) { this.ordered = ordered; }
	public void setAnswers(List<Answer> answers) { this.answers.addAll(answers); }
	public void addAnswer(Answer a) { this.answers.add(a); }
	public void setNumAnswers(int numAnswers) { this.numAnswers = numAnswers; }
	
	
	
	// ----- Quiz Taking ----- //
	public boolean correct(String attempt) {
		addPossiblePoint();
		if (answers.get(0).isValidAnswer(attempt)) {
			addPoint();
			return true;
		}
		return false;
	}
	
	// Returns true if the user got all of the possible points on this question.
	public boolean correct(String[] attempt) {
		if (ordered) {
			return correctOrdered(attempt);
		}
		else {
			return correctUnordered(attempt);
		}
	}
	
	public boolean correctOrdered(String[] attempt) {
		// The size of attempt (number of textfields at the front end) should not be larger than the number
		// of possible answers. The size of attempt is the number of possible points, as specified in the 
		// assignment handout.
		assert(answers.size() >= attempt.length);
		for (int i = 0; i < attempt.length; i++) {
			addPossiblePoint();
			Answer a = answers.get(i);
			String currentAttempt = attempt[i];
			if (a.isValidAnswer(currentAttempt)) {
				// If they're ordered we don't have to worry about marking use.
				addPoint();
			}
		}
		// Returns true if the user got all of the possible points. We might not use this, but I was thinking I 
		// might as well return true or false just in case we want to highlight green if they got everything correct
		// and red otherwise, or something like that. We can worry about this later.
		if (getPointsEarned() == getPossiblePoints()) {
			return true;
		}
		return false;
	}
	
	// Corrects the question in the case that the answers are not ordered.
	public boolean correctUnordered(String[] attempt) {
		for (int i = 0; i < attempt.length; i++) {
			addPossiblePoint();
			for (int j = 0; j < answers.size(); j++) {
				if (answers.get(j).isValidAnswer(attempt[i])) {
					answers.get(j).markUsed(); // So we don't use the same answer twice.
					addPoint();
					break;
				}
			}
		}
		if (pointsEarned == possiblePoints) {
			return true;
		}
		return false;
	}
	
	@Override
	public String toString() {  //for testing
		return super.toString() + 
			", Question: " + text;
	}
}
