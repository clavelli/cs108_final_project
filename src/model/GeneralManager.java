package model;

import java.util.List;
import dao.*;
import java.util.Date;

import dao.business.MessageDB;

public abstract class GeneralManager {
	
	public static final int NUM_TO_GET = 5;
	
	
	public static List<Announcement> getRecentAnnouncements() {
		SelectListResponse<Announcement> response = MessageDB.getAnnoucements(3);
		if (response.getStatus() != DBResponse.Status.SUCCESS) {
			return null;
		}
		return response.getDTOList();
	}
	
	
	
	
}
