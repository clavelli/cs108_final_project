package model;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import dao.DBResponse;
import dao.DeleteResponse;
import dao.InsertResponse;
import dao.SelectListResponse;
import dao.SelectResponse;
import dao.UpdateResponse;
import dao.business.UserDB;

public abstract class AccountManager {

	public static final String firstHalfOfURL = "user-page.jsp?username=";
	
	/* 
	 * Returns the appropriate user if one is found, returns NULL otherwise.
	 */
	public static User logIn(String username, String password) {
		SelectResponse<User> user_response = UserDB.getUser(username);
		if (user_response.getStatus() != DBResponse.Status.SUCCESS) {
			return null;
		}
		User user = user_response.getDTO();
		String hashedPassword = hashPassword(password);
		if (!hashedPassword.equals(user.getHashedPassword())) {
			return null;
		}
		return user;
	}
	
	public static void addFriends(String username, String friendName) {
		InsertResponse<Integer> response = UserDB.addFriend(username, friendName);
		if (response.getStatus() != DBResponse.Status.SUCCESS) {
			// ERROR
		}
	}
	
	public static boolean areFriends(User u, String friendName) {
		List<String> friends = u.getFriendNames();
		if (friends != null && friends.contains(friendName)) return true;
		return false;
	}
	
	/* 
	 * Creates the account by adding it to the database.
	 * If the username is taken, returns null and doesn't change anything in the db.
	 * Otherwise, it adds the user to the database, and returns that user.
	 */
	public static User createAccount(String username, String password) {
		SelectResponse<User> response1 = UserDB.getUser(username);
		if (response1.getStatus() == DBResponse.Status.SUCCESS) {
			return null;
		}
		String hashedPassword = hashPassword(password);
		InsertResponse<String> response2 = UserDB.addAccount(username, hashedPassword, false);
		if (response2.getStatus() != DBResponse.Status.SUCCESS) {
			return null;
		}
		SelectResponse<User> response3 = UserDB.getUser(username);
		return response3.getDTO();
	}
	
	
	public static void removeAccount(String username) {
		DeleteResponse response = UserDB.deleteAccount(username);
		if (response.getStatus() != DBResponse.Status.SUCCESS) {
			// something went wrong
		}
	}
	
	
	public static void makeAdmin(String username) {
		UpdateResponse response = UserDB.setUserAdmin(username, true);
		if (response.getStatus() != DBResponse.Status.SUCCESS) {
			// ERROR
		}
	}
	
	public static void searchUsers(String searchString) {
		SelectListResponse<User> response = UserDB.searchUsers(searchString);
		if (response.getStatus() != DBResponse.Status.SUCCESS) {
			// something went wrong
		}
		List<User> users = response.getDTOList();
	}
	
	
	public static void searchFriends(String username, String searchString) {
		SelectListResponse<String> response = UserDB.searchFriends(username, searchString);
		if (response.getStatus() != DBResponse.Status.SUCCESS) {
			// something went wrong
		}
		List<String> users = response.getDTOList();
	}
	
	
	private static String hashPassword(String password) {
		MessageDigest md;
		try {
			 md = MessageDigest.getInstance("SHA");
		} catch (NoSuchAlgorithmException e) {
			return null;
		}
		md.update(password.getBytes());
		byte[] bytes = md.digest();
		String result = hexToString(bytes);
		
		return result;
	}
	
	private static String hexToString(byte[] bytes) {
		StringBuffer buff = new StringBuffer();
		for (int i = 0; i < bytes.length; i++) {
			int val = bytes[i];
			val = val & 0xff;
			if (val < 16) buff.append('0');
			buff.append(Integer.toString(val, 16));
		}
		return buff.toString();
	}
	
	public static String getURL(String username) {
		return firstHalfOfURL + username;
	}
}
