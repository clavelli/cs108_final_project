
package model;

import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import dao.business.QuizDB;
import dao.business.UserDB;
import dao.*;

public class Quiz {

	// db variables
	private String quizID;
	private String creatorName;
	private String description;
	private String title;
	private boolean random;
	private boolean singlePage;
	private boolean immCorrection;
	private List<Question> questions;
	private long hits;
	private long likes;
	private Date createDate;
	
	// model variables
	private int curQuestion;
	private long startTime;
	private long endTime;
	
	
	public Quiz(String quizID, String creatorName, String description, String title, boolean random,
			boolean singlePage, boolean immCorrection, long hits, long likes) {
		this.quizID = quizID;
		this.creatorName = creatorName;
		this.description = description;
		this.random = random;
		this.singlePage = singlePage;
		this.immCorrection = immCorrection;
		this.title = title;
		this.questions = new LinkedList<Question>();
		this.curQuestion = 0;
		this.hits = hits;
		this.likes = likes;
	}
	
	
	public Quiz(String quizID) {
		this.quizID = quizID;
		this.questions = new LinkedList<Question>();
		this.curQuestion = 0;
	}
	
	public Quiz() {
		this.questions = new LinkedList<Question>();
		this.curQuestion = 0;
	}

	
	
	// ----- Setters ----- //
	
	public void setQuizID(String quizID) { this.quizID = quizID; }
	public void setCreatorName(String creatorName) { this.creatorName = creatorName; }
	public void setDescription(String description) {this.description = description; }
	public void setTitle(String title) { this.title = title; }
	public void setRandom(boolean random) { this.random = random;}
	public void setSinglePage(boolean singlePage) { this.singlePage = singlePage; }
	public void setImmCorrection(boolean immCorrection) { this.immCorrection = immCorrection; }
	public void setQuestions(List<Question> questions) { this.questions.addAll(questions); }
	public void addQuestion(Question question) { this.questions.add(question); }
	public void setNumHits(long hits) { this.hits = hits; }
	public void setNumLikes(long likes) { this.likes = likes; }
	public void setCreateDate(Date date) { this.createDate = date; }
	public void setCreateDate() { setCreateDate(new Date()); }
	
	
	
	// ----- Getters ----- //

	public String getQuizID() { return quizID; }	
	public String getCreatorName() { return creatorName; }
	public String getDescription() { return description; }
	public String getTitle() { return title; }
	public boolean getRandom() { return random; }
	public boolean getSinglePage() { return singlePage; }
	public boolean getImmCorretion() { return immCorrection; }
	public List<Question> getQuestions() { return questions; }
	public long getHits() { return hits; }
	public long getLikes() { return likes; }
	public Question getLastQuestion() { return questions.get(questions.size() - 1); } //helps when creating the quiz.
	public Date getCreateDate() { return createDate; }

	
	
	
	// ----- Update ----- //
	public void start() { randomize(); startTime = System.currentTimeMillis(); }
	public void finish() { endTime = System.currentTimeMillis(); }
	public long getElapsedTime() { return endTime - startTime; }
	public Question getCurrent() { return questions.get(curQuestion); }
	public void removeLastQuestion() { questions.remove(questions.size() - 1); }
	public void addHit() { QuizDB.addHit(quizID); }
	public void addLike() { QuizDB.addLike(quizID); }
	
	public boolean nextQuestion() {
		if (curQuestion == questions.size() - 1) return false;
		curQuestion++;
		return true;
	}
	
	public boolean prevQuestion() {
		if (curQuestion == 0) return false;
		curQuestion--;
		return true;
	}
	
	public int getCurrentNumber() {
		return curQuestion;
	}
	
	public Question getQuestion(int num) {
		if (num < 0 || num >= questions.size()) return null;
		curQuestion = num;
		return questions.get(num);
	}
	
	public boolean isLastQuestion() {
		return curQuestion == questions.size() - 1;
	}
	
	// before this is called, correct() should have been called on each of the questions of the quiz.
	// returns the percentage grade.
	public double grade() {
		int pointsEarned = 0;
		int possiblePoints = 0;
		for (int i = 0; i < questions.size(); i++) {
			Question q = questions.get(i);
			pointsEarned += q.getPointsEarned();
			possiblePoints += q.getPossiblePoints(); 
		}
		return ((double)pointsEarned) / possiblePoints;
	}
	
	public int getPointsEarned() {
		int pointsEarned = 0;
		for (int i = 0; i < questions.size(); i++) {
			Question q = questions.get(i);
			pointsEarned += q.getPointsEarned(); 
		}
		return pointsEarned;
	}
	
	public int getPossiblePoints() {
		int pointsPossible = 0;
		for (int i = 0; i < questions.size(); i++) {
			Question q = questions.get(i);
			pointsPossible += q.getPossiblePoints(); 
		}
		return pointsPossible;
	}
	
	private void randomize() {
		if (random) {
			Collections.shuffle(questions);
		}
		for (int i = 0; i < questions.size(); i++) {
			questions.get(i).setLocalID(i);
		}
	}
	
	
	
	
	
	
	// ----- Retrieval ----- //
	
	public List<Result> getTopRecentResults() {
		SelectListResponse<Result> response = QuizDB.getTopRecentResults(quizID, QuizManager.MINUTES_PREVIOUS, QuizManager.NUM_TO_GET);
		if (response.getStatus() != DBResponse.Status.SUCCESS) {
			return null;
		}
		return response.getDTOList();
	}
	
	public List<Result> getTopResults() {
		SelectListResponse<Result> response = QuizDB.getTopResults(quizID, QuizManager.NUM_TO_GET);
		if (response.getStatus() != DBResponse.Status.SUCCESS) {
			return null;
		}
		return response.getDTOList();
	}
	
	public List<Result> getRecentResults() {
		SelectListResponse<Result> response = QuizDB.getRecentResults(quizID, QuizManager.MINUTES_PREVIOUS, QuizManager.NUM_TO_GET);
		if (response.getStatus() != DBResponse.Status.SUCCESS) {
			return null;
		}
		return response.getDTOList();
	}
	
	public List<Result> getTopResultsForUser(String username) {
		SelectListResponse<Result> response = UserDB.getTopResults(username, quizID, QuizManager.NUM_TO_GET);
		if (response.getStatus() != DBResponse.Status.SUCCESS) {
			return null;
		}
		return response.getDTOList();
	}
	
	public List<Result> getRecentResultsForUser(String username) {
		SelectListResponse<Result> response = UserDB.getTopResults(username, quizID, QuizManager.NUM_TO_GET);
		if (response.getStatus() != DBResponse.Status.SUCCESS) {
			return null;
		}
		return response.getDTOList();
	}
		
	
	/**
	 * @author anh
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("Id: " + quizID);
		sb.append(", Creator: " + creatorName);
		sb.append(", Description: " + description);
		sb.append(", Title: " + title);
		sb.append(", isRandom: " + random);
		sb.append(", isSinglePage: " + singlePage);
		sb.append(", isImmediateCorrect: " + immCorrection);
		sb.append(", hits: " + hits);
		sb.append(", likes: " + likes);
		
		return sb.toString();
	}
}