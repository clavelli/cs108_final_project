package model;
import java.util.ArrayList;
import java.util.List;

public class Answer {
	private String answerId;
	private List<String> variations;
	private boolean used;
	private int answerNumber;
	
	public Answer() {
		used = false;
		this.variations = new ArrayList<String>();
	}
	
	public Answer(List<String> variations) {
		this();
		this.variations.addAll(variations);
	}
 
	public Answer(String answer) {
		this();
		this.variations.add(answer);
	}
	
	// ----- Getters ----- //
	public String getAnswerId() { return answerId; }
	public List<String> getVariations() { return variations; }
	public int getAnswerNumber() { return answerNumber; }
	
	// ----- Setters ----- //
	public void setAnswerId(String answerId) { this.answerId = answerId; }
	public void addVariation(List<String> variations) { this.variations.addAll(variations); }
	public void addVariation(String variation) { this.variations.add(variation); }
	public void setAnswerNumber(int answerNumber) { this.answerNumber = answerNumber; }
	public void addVariation(String []variation) {
		for (int i = 0; i < variation.length; i++) {
			addVariation(variation[i]);
		}
	}
	
	// ----- Update ----- //
	// I don't think we'll ever use this...
	public void setUsed(boolean used) { this.used = used; }
	
	
	// We don't want the user to correctly answer Washington and George Washington (or to answer Washington twice for 
	// that matter), so we return false if this answer has been used already. Then, of course, we return true if 
	// the entered answer is one of the variations, and return false otherwise.
	public boolean isValidAnswer(String attempt) {
		if (used) return false;
		for (int i = 0; i < variations.size(); i++) {
			if (variations.get(i).equalsIgnoreCase(attempt)) return true;
		}
		return false;
	}
	
	// Marks the answer as used. There shouldn't be any reason to mark an Answer as unused.
	public void markUsed() {
		used = true;
	}

	public String getVariationsString() {
		StringBuilder str = new StringBuilder();
		for (int i = 0; i < variations.size(); i++) {
			str.append(variations.get(i));
			if (i != variations.size() - 1) {
				str.append(" / ");
			}
		}
		return str.toString();
	}
}
