package model;

public class Achievement extends Activity {
	
	
	private String name; // short name for the Achievement, such as "Quiz King". 
	private String description; // description of the achievement, such as "Took 100 quizzes in one week!".

	public Achievement(String username, String name, String description) {
		super(ActivityType.ACHIEVEMENT);
		this.username = username;
		this.name = name;
		this.description = description;
	}
	
	public Achievement() {
		super(ActivityType.ACHIEVEMENT);
	}
	
	// ----- Getters ----- //
	
	public String getText() { return name; }
	public String getDescription() { return description; }
	
	// ----- Setters ----- //
	
	public void setName(String name) { this.name = name; }
	public void setDescription(String description) { this.description = description; }
}
