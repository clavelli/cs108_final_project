package model;

import java.util.Date;

public class Creation extends Activity {
	
	private String quizID;
	private String quizTitle;
	

	public Creation(String quizID, String quizTitle, Date date, String username) {
		super(ActivityType.CREATION);
		this.username = username;
		this.quizID = quizID;
		this.quizTitle = quizTitle;
		this.date = date;
	}
	
	public Creation() {
		super(ActivityType.CREATION);
	}
	
	public String getQuizID() { return quizID; }
	public Date getDate() { return date; }
	public String getQuizTitle() { return quizTitle; }
	
	
	public void setQuizID(String quizID) { this.quizID = quizID; }
	public void setQuizTitle(String quizTitle) { this.quizTitle = quizTitle; }
	public void setDate(Date date) { this.date = date; }
}
