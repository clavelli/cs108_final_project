package model;

import java.util.Date;

public class Activity {
	public enum ActivityType { RESULT, ACHIEVEMENT, CREATION };
	protected String activityID;
	protected String username;
	protected Date date;
	protected ActivityType type;
	
	public Activity() {}
	
	public Activity(ActivityType type) {
		this.type = type;
	}
	
	public String getUsername() { return username; }
	public Date getDate() { return date; }
	public ActivityType getType() { return type; }
	public String getActivityId() { return activityID; }
	
	public void setUsername(String username) { this.username = username; }
	public void setDate(Date date) { this.date = date; }
	public void initDate() { this.date = new Date(); }
	public void setActivityID(String activityID) { this.activityID = activityID; }
	public void setActivityType(ActivityType type) { this.type = type; }
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("ActivityID: " + activityID);
		sb.append(", username: " + username);
		sb.append(", date: " + date.toString());
		
		return sb.toString();
	}
}
