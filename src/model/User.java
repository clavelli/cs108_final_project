package model;

import java.util.ArrayList;
import java.util.List;

import dao.DBResponse;
import dao.SelectListResponse;
import dao.SelectResponse;
import dao.business.MessageDB;
import dao.business.UserDB;

public class User {

	private String username;
	private String hashedPassword;
	private boolean isAdmin;
	
	private static final int NUM_TO_GET = 10;
	public static final int MINUTES_PREVIOUS = 60*24*10;
	
	/**
	 * Author:  Anh.  Need to store database result
	 * @param username
	 * @param hashedPassword
	 * @param isAdmin
	 */
	
	// ----- Constructors ----- //
	
	public User() {
		this.isAdmin = false;
	}
	
	public User(String username) {
		this.username = username;
		this.isAdmin = false;
	}
	
	public User(String username, String hashedPassword) {
		this(username);
		this.hashedPassword = hashedPassword;
	}
	
	public User(String username, String hashedPassword, boolean isAdmin) {
		this(username, hashedPassword);
		this.isAdmin = isAdmin;
	}
	
	
	
	// ----- Getters ----- //
	
	public String getUsername() { return username; }
	public String getHashedPassword() { return hashedPassword; }
	public boolean isAdmin() { return isAdmin; }
	
	
	// ----- Setters ----- //
	
	public void setUsername(String username) { this.username = username; }
	public void setHashedPassword(String hashedPassword) { this.hashedPassword = hashedPassword; }
	public void setAdmin(boolean isAdmin) { this.isAdmin = isAdmin; }

	
	
	// ----- Other ----- //
	
	public List<Activity> getRecentActivities() {
		SelectListResponse<Activity> response = UserDB.getRecentActivities(username, MINUTES_PREVIOUS, NUM_TO_GET);
		if (response.getStatus() != DBResponse.Status.SUCCESS) {
			// ERROR
		}
		return response.getDTOList();
	}
	
	public List<Activity> getRecentActivities(int numToGet, int minutes) {
		SelectListResponse<Activity> response = UserDB.getRecentActivities(username, minutes, numToGet);
		if (response.getStatus() != DBResponse.Status.SUCCESS) {
			// ERROR
		}
		return response.getDTOList();
	}
	
	public List<Achievement> getAchievements() {
		SelectListResponse<Achievement> response = UserDB.getAchievements(username, NUM_TO_GET);
		if (response.getStatus() != DBResponse.Status.SUCCESS) {
			// ERROR
		}
		return response.getDTOList();
	}
	
	public List<Result> getRecentQuizTaken() {
		SelectListResponse<Result> response = UserDB.getRecentResults(username, MINUTES_PREVIOUS, NUM_TO_GET);
		if (response.getStatus() != DBResponse.Status.SUCCESS) {
			return null;
			// ERROR
		}
		return response.getDTOList();
	}
	
	public List<Creation> getRecentCreated() {
		SelectListResponse<Creation> response = UserDB.getRecentCreated(username, MINUTES_PREVIOUS, NUM_TO_GET);
		if (response.getStatus() != DBResponse.Status.SUCCESS) {
			// ERROR
		}
		return response.getDTOList();
	}
	
	public List<Activity> getRecentFriendsActivities() {
		SelectListResponse<String> friendsResponse = UserDB.getFriends(username);
		if (friendsResponse.getStatus() != DBResponse.Status.SUCCESS) {
			// ERROR
		}
		
		List<String> friends = friendsResponse.getDTOList();
		if (friends == null) {
			return null;
		}
		List<String> names = new ArrayList<String>();
		for (int i = 0; i < friends.size(); i++) {
			names.add(friends.get(i));
		}
		SelectListResponse<Activity> response = UserDB.getRecentFriendsActivities(names,MINUTES_PREVIOUS, NUM_TO_GET);
		return response.getDTOList();
	}
	
	public List<String> getFriendNames() {
		SelectListResponse<String> friendsResponse = UserDB.getFriends(username);
		if (friendsResponse.getStatus() != DBResponse.Status.SUCCESS){
			// ERROR
		}
		return friendsResponse.getDTOList();
	}
	
	public static List<Message> getMessages(String username) {
		SelectListResponse<Message> messageResponse = MessageDB.getMessages(username);
		if (messageResponse.getStatus() != DBResponse.Status.SUCCESS){
			// ERROR
		}
		return messageResponse.getDTOList();
	}
	
	public static List<Message> getUnreadMessages(String username) {
		SelectListResponse<Message> messageResponse = MessageDB.getUnreadMessages(username);
		if (messageResponse.getStatus() != DBResponse.Status.SUCCESS){
			// ERROR
		}
		return messageResponse.getDTOList();
	}
}
