package model;

public class Question {
	public enum QuestionType { QUESTION_RESPONSE, FILL_IN_BLANK, MULTIPLE_CHOICE }
	
	// db variables
	protected QuestionType type;
	protected String pictureURL;
	protected String questionID;
	protected String questionNumber;
	
	// model variables
	
	protected int pointsEarned;
	protected int possiblePoints;
	// See the Choice class for an explanation of what localID does. It essentially handles randomization in the same
	// localID does in the Choice class.
	private int localID; // zero-indexed btw.
	
	
	
	public Question(QuestionType type) {
		this.type = type;
	}

	
	// ----- Getters ----- //
	public QuestionType getType() { return type; }
	public String getPictureURL() { return pictureURL; }
	public String getQuestionID() { return questionID; }
	public String getQuestionNumber() { return questionNumber; }
	public int getPossiblePoints() { return possiblePoints; }
	public int getPointsEarned() { return pointsEarned; }
	public int getLocalID() { return localID; }
	
	// ----- Setters ----- //
	public void setPossiblePoints(int possiblePoints) { this.possiblePoints = possiblePoints; }
	public void setPictureUrl(String pictureURL) { this.pictureURL = pictureURL; }
	public void setQuestionID(String questionID) { this.questionID = questionID; }
	public void setQuestionNumber(String questionNumber) { this.questionNumber = questionNumber; }
	public void setLocalID(int localID) { this.localID = localID; } 
	public void setType (QuestionType type) { this.type = type; }
	
	// ----- Update ----- //
	public void addPoint() {
		pointsEarned++;
	}
	// after each graded point, whether or not it was answered correctly, we call this so we
	// can keep track of the number of possible points.
	public void addPossiblePoint() {
		possiblePoints++;
	}

	
	@Override
	public String toString() {
		return "Id: " + questionID + "Type: " + type + ", picUrl: " + pictureURL +
			", qNumber: " + questionNumber;
	}
}
