package model;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.List;

import dao.DBResponse;
import dao.DeleteResponse;
import dao.InsertResponse;
import dao.SelectListResponse;
import dao.SelectResponse;
import dao.business.QuizDB;

public abstract class QuizManager {
	
	public static final String firstHalfOfURL = "quiz-home.jsp?quizID=";
	public static final int NUM_TO_GET = 5;
	public static final int MINUTES_PREVIOUS = 60*24;
	
	// ----- Select ----- //
	
	// Given a quizID, gets the Quiz object with that quizID from the database.
	public static Quiz getQuiz(String quizID) {
		SelectResponse<Quiz> response = QuizDB.getQuiz(quizID);
		if (response.getStatus() != DBResponse.Status.SUCCESS) {
			// ERROR
			return null;
		}
		return response.getDTO();
	}
	
	public static List<Quiz> getQuizzesByHits() {
		SelectListResponse<Quiz> response = QuizDB.getQuizzesByHits(NUM_TO_GET);
		if (response.getStatus() != DBResponse.Status.SUCCESS) {
			// ERROR
			return null;
		}
		return response.getDTOList();
	}
	
	public static List<Quiz> getQuizzesByLikes() {
		SelectListResponse<Quiz> response = QuizDB.getQuizzesByLikes(NUM_TO_GET);
		if (response.getStatus() != DBResponse.Status.SUCCESS) {
			// ERROR
			return null;
		}
		return response.getDTOList();
	}
	
	public static List<Quiz> getRecentQuizzes() {
		SelectListResponse<Quiz> response = QuizDB.getRecentQuizzes(MINUTES_PREVIOUS, NUM_TO_GET);
		if (response.getStatus() != DBResponse.Status.SUCCESS) {
			// ERROR
			return null;
		}
		return response.getDTOList();
	}
	
	public static List<Quiz> searchQuizzes(String searchString) {
		SelectListResponse<Quiz> response = QuizDB.searchQuizzes(searchString);
		if (response.getStatus() != DBResponse.Status.SUCCESS) {
			// ERROR
			return null;
		}
		return response.getDTOList();
	}
	
	
	// ----- Insert ----- //
	
	// This we be the method to call when a user creates a quiz. So this won't involve anything about the 
	// questions, just add the quiz with all of it's properties to the DB.
	// Returns the quizID that is assigned to this quiz.
	public static String createQuiz(String creatorID, String description, String title, boolean random, 
			boolean singlePage, boolean immCorrection) {
		
		InsertResponse<String> response = QuizDB.addQuiz(creatorID, description, title, random, singlePage, immCorrection);
		if (response.getStatus() != DBResponse.Status.SUCCESS) {
			// ERROR
			return null;
		}
		return response.getDTO();
	}
	
	public static String createQuiz(Quiz q) {
		InsertResponse<String> response = QuizDB.addQuiz(q);
		if (response.getStatus() != DBResponse.Status.SUCCESS) {
			// ERROR
			return null;
		}
		return response.getDTO();
	}
	
	
	
	
	// ----- Delete ----- //
	
	public static boolean deleteQuiz(String quizID) {
		DeleteResponse response = QuizDB.deleteQuiz(quizID);
		if (response.getStatus() != DBResponse.Status.SUCCESS) {
			// ERROR
			return false;
		}
		return true;
	}
	
	
	
	
	// ----- Update ----- //
	
	public static void addLike(String quizID) {
		QuizDB.addLike(quizID);
	}
	
	public static void addHit(String quizID) {
		QuizDB.addHit(quizID);
	}
	
	public static void reset(String quizID) {
		QuizDB.resetHits(quizID);
		QuizDB.resetLikes(quizID);
	}
	
	// Should this be different from reset?
	public static void clearHistory(String quizID) {
		QuizDB.resetHits(quizID);
		QuizDB.resetLikes(quizID);
	}
	
	
	
	// ----- Other ----- //
	
	public static String getAverageScoreStr(String quizID) {
		SelectResponse<Double> response = QuizDB.getAverageScore(quizID);
		if (response.getStatus() != DBResponse.Status.SUCCESS) {
			// ERROR
			return null;
		}
		Double score = response.getDTO();
		return QuizManager.formattedScore(QuizManager.getQuiz(quizID), score);
	}
	
	public static String getAverageTimeStr(String quizID) {
		SelectResponse<BigDecimal> response = QuizDB.getAverageTime(quizID);
		if (response.getStatus() != DBResponse.Status.SUCCESS) {
			// ERROR
			return null;
		}
		BigDecimal b = response.getDTO();
		return getTimeStr(b.longValue());
	}
		
	public static String getURL(String quizID) {
		return firstHalfOfURL + quizID;
	}
	
	public static String getTimeStr(long time) { 
		int hr = (int)((time / 3600000));
		int min = (int)((time / 60000));
		int sec = (int)((time / 1000) % 60);
		
		DecimalFormat df = new DecimalFormat("#00");
		
		if (time < 60 * 1000) {
			return "00:" + df.format(sec);
		}
		else if (time < 60 * 60 * 1000) {
			return df.format(min) + ":" + df.format(sec);
		}
		
		return df.format(hr) + ":" + df.format(min) + ":" + df.format(sec);
	}
	
	public static String timeSince(long time) {
		double n = System.currentTimeMillis();
		return otherGetTimeStr(System.currentTimeMillis() - time);
	}
	
	public static String otherGetTimeStr(long time) {
		int hr = (int)((time / 3600000));
		int min = (int)((time / 60000));
		int sec = (int)((time / 1000));
		
		if (time < 60 * 1000) {
			return String.valueOf(sec) + " seconds ago";
		}
		else if (time < 60 * 60 * 1000) {
			return String.valueOf(min) + " minutes ago";
		}
		return String.valueOf(hr) + " hours ago";
	}
	
	
	public static String formattedScore(Quiz q, double score) {
		score *= 100.0;
		DecimalFormat format = new DecimalFormat("#0.0");
		return format.format(score);
	}
}
