package model;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class MultipleChoice extends Question {
	private String text;
	private List<Choice> choices;
	private boolean singleAnswer;
	private boolean randomized; // Determines whether the choices appear in a specified order.
	private int numAnswers;
	
	public MultipleChoice() {
		super(Question.QuestionType.MULTIPLE_CHOICE);
		this.choices = new LinkedList<Choice>();
	}
	
	public MultipleChoice(int possiblePoints, String pictureURL, String questionID, String text,
				List<Choice> choices, boolean singleAnswer, boolean randomized, int numAnswers) {
		this();
		this.possiblePoints = possiblePoints;
		this.pictureURL = pictureURL;
		this.questionID = questionID;
		this.text = text;
		this.choices = new LinkedList<Choice>(choices);
		this.singleAnswer = singleAnswer;
		this.randomized = randomized;
		this.numAnswers = numAnswers;
		randomize();
	}
	
	// ----- Setters ----- //
	// I put a randomize() call in both setChoices and setRandomized, so that whoever is calling these setters
	// does not have to call these in a particular order. There is no performance issue with this; regardless of the chosen
	// order, the first of the two calls to randomize will do nothing (because either randomize will be false or choices
	// will be empty).
	public void setChoices(List<Choice> choices) { this.choices.addAll(choices); randomize(); }
	public void setSingleAnswer(boolean singleAnswer) { this.singleAnswer = singleAnswer; }
	public void setRandomized(boolean randomized) { this.randomized = randomized; randomize(); }
	public void setNumAnswers(int numAnswers) { this.numAnswers = numAnswers; }
	public void setText(String text) { this.text = text; }  

	// ----- Getters ----- //
	public List<Choice> getChoices() { return choices; }
	public boolean isSingleAnswer() { return singleAnswer; }
	public boolean isRandomized() { return randomized; }
	public int getNumAnswers() { return numAnswers; }
	public String getText() { return text; }
	
	
	// ----- Update ----- //
	public boolean correct(List<Integer> attempt) {
		addPossiblePoint();
		if (attempt.size() == 0) {
			return false;
		}

		for (int i = 0; i < choices.size(); i++) {
			if (choices.get(i).isCorrect() && !attempt.contains(i) || 
									(!choices.get(i).isCorrect() && attempt.contains(i))) { 
				return false;
			}
		}
		addPoint();
		return true;
	}
	
	
	// Assigns questionIDs 
	private void randomize() {
		if (randomized) {
			Collections.shuffle(choices, new Random(System.currentTimeMillis()));
		}
		for (int i = 0; i < choices.size(); i++) {
			choices.get(i).setLocalID(i);
		}
	}
	
	@Override
	public String toString() {	//for testing
		return super.toString() + 
			", Question: " + text + ", isSingleAnswer: " + singleAnswer +
			", isRandom: " + randomized + ", NumAnswers: " + numAnswers;
	}
}
