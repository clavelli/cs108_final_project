package model;

import java.text.DecimalFormat;
import java.util.Date;

public class Result extends Activity {

	private String quizID;
	private String quizTitle;
	private long elapsedTime;
	private double score;
	
	public Result(String quizID, String username, Date date, long elapsedTime, double score) {
		super(ActivityType.RESULT);
		this.username = username;
		this.quizID = quizID;
		this.elapsedTime = elapsedTime;
		this.score = score;
		this.date = date;
	}
	
	public Result() {
		super(ActivityType.RESULT);
	}

	// ----- Getters ----- //
	public String getQuizID() { return quizID; }
	public String getQuizTitle() { return quizTitle; }
	public long getElapsedTime() { return elapsedTime; }
	public double getScore() { return score; }
	public String getFormattedScore() { return QuizManager.formattedScore(QuizManager.getQuiz(quizID), score); };
	
	
	// ----- Setters ----- //
	public void setQuizID(String quizID) { this.quizID = quizID; }
	public void setQuizTitle(String quizTitle) { this.quizTitle = quizTitle; }
	public void setElapsedTime(long elapsedTime) { this.elapsedTime = elapsedTime; } 
	public void setScore(double score) { this.score = score; }
	
	
	public String getTimeStr() { 
		long time = getElapsedTime();
		
		int hr = (int)((time / 3600000));
		int min = (int)((time / 60000));
		int sec = (int)((time / 1000) % 60);
		
		DecimalFormat df = new DecimalFormat("#00");
		
		if (time < 60 * 1000) {
			return "00:" + df.format(sec);
		}
		else if (time < 60 * 60 * 1000) {
			return df.format(min) + ":" + df.format(sec);
		}
		
		return df.format(hr) + ":" + df.format(min) + ":" + df.format(sec);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append(", QuizId: " + quizID);
		sb.append(", quizTitle: " + quizTitle);
		sb.append(", elapsedTime: " + elapsedTime);
		sb.append(", score: " + score);
		
		return sb.toString();
	}
}
