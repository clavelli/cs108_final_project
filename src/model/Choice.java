package model;

public class Choice {
	
	// localID has nothing to do with the DB. If the user wants the choices in a randomized order, 
	// this is used to reorder the choices. localID = 0 will correspond with the choice that has in the first spot for its
	// question, localID = 1 the second spot, etc, from the front end perspective.
	
	// choiceID is for the database to identify Choices. From what I understand it is a global id (so no two choices will
	// ever have the same choiceID).
		
	// choiceNumber, at the DB level, stores the number of the choice, as entered by the user (the first choice he enters
	// will have choiceNumber 0). Since we are passing Choices around as part of a list, I was thinking that this field 
	// wasn't necessary, because Kevin could just determine the order by the placement in the list, and Anh could use this
	// order to store a value for choiceNumber in the DB. If you guys prefer, we can have it as a field in the Choice class
	// instead. It really doesn't matter from my perspective. I'll leave it in here for now, and we'll see whether we want 
	// to keep it.
	
	// Just for a little sanity check to make sure we're all on the same page here, the maximum choiceNumber for a question
	// and the maximum localID for a question should be the same (assuming both are zero-indexed). choiceID should not.
	
	private int localID;
	private String choiceID;
	private int choiceNumber;	//anh: used to order choices before putting in choices list (you could also implement comparable interface on this if you don't want to depend on db method to order.)
	private String text;
	private boolean correct;
	
	public Choice() {
		
	}
	
	public Choice(String text, boolean correct) {
		this.text = text;
		this.correct = correct;
	}
	
	// ----- Setters ----- //
	public void setText(String text) { this.text = text; }
	public void setCorrect(boolean correct) { this.correct = correct; }
	public void setLocalID(int localID) { this.localID = localID; }
	public void setChoiceID(String choiceID) { this.choiceID = choiceID; }
	public void setChoiceNumber(int choiceNumber) { this.choiceNumber = choiceNumber; }
	
	// ----- Getters ----- //
	public String getText() { return text; }
	public boolean isCorrect() { return correct; }
	public int getLocalID() { return localID; }
	public String getChoiceID() { return choiceID; }
	public int getChoiceNumber() { return choiceNumber; }
	
}
