package model;

import java.util.Date;

import dao.business.MessageDB;



public class Message {
	
	public Message() {
		date = new Date();
		// hello
	}
	
	public Message(MessageType type) {
		this();
		this.type = type;
		this.date = new Date();
	}
	
	public Message(String senderName, String receiverName) {
		this();
		this.senderName = senderName;
		this.receiverName = receiverName;
		this.date = new Date();
	}
	
	public enum MessageType {NOTE, FRIEND_REQUEST, CHALLENGE};
	
	private String messageID;
	private String senderName;
	private String receiverName;
	private String body;
	private MessageType type;
	private boolean read;
	private Date date;
	
	public String getMessageID() { return messageID; }
	public String getSenderName() { return senderName; }
	public String getReceiverName() { return receiverName; }
	public String getBody() { return body; }
	public MessageType getType() { return type; }
	public boolean getRead() { return read; }
	public Date getDate() { return date; }
	
	public void setMessageID(String messageID) { this.messageID = messageID; }
	public void setSenderName(String senderName) { this.senderName = senderName; }
	public void setReceiverName(String receiverName) { this.receiverName = receiverName; }
	public void setBody(String body) { this.body = body; }
	public void setType(MessageType type) { this.type = type; }
	public void setRead(boolean read) { this.read = read; }
	public void setDate(Date date) { this.date = date; }
	
	public String getTypeStr() {
		if (type == MessageType.CHALLENGE) { 
			return "Challenge";
		}
		else if (type == MessageType.FRIEND_REQUEST) { 
			return "Friend Request";
		}
		return "Note";
	}
	
	public boolean send() {
		MessageDB.addMessage(this);
		return true;
	}
}
