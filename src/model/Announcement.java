package model;

import java.util.Date;

public class Announcement {
	
	private String text;
	private String announcementID;
	private String creatorID;
	private Date date;

	// ----- Constructors ----- //
	public Announcement() {
		this.date = new Date();
	}
	
	public Announcement(String text, String announcementID, String creatorID) {
		this.date = new Date(); 
		this.text = text;
		this.announcementID = announcementID;
		this.creatorID = creatorID;
	}
	
	
	
	// ----- Setters ----- //
	public void setText(String text) { this.text = text; }
	public void setCreatorID(String creatorID) { this.creatorID = creatorID; }
	public void setAnnouncementID(String announcementID) { this.announcementID = announcementID; }
	public void setDate(Date date) { this.date = date; }
	
	// ----- Getters ----- //
	public String getText() { return text; }
	public Date getDate() { return date; }
	public String getCreatorID() { return creatorID; }
	public String getAnnouncementID() { return announcementID; }
	
	
	
	
}
