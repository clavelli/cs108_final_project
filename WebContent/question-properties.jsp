<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="model.*" %>
<%@ page import="java.util.*" %>
<%@ page import="utils.Helper" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="style.css" />
<script type="text/javascript" src="js/script.js"></script> 
<title>Question Properties</title>
</head>
<body>
<%= Helper.banner() %>
<%= Helper.searchBar() %>
<div id="wrap">
<%
Quiz q = (Quiz) session.getAttribute("quiz");
out.println("<h3>Question #" + (q.getQuestions().size() + 1) + "</h3>");
%>
<form action="QuestionPropertiesServlet" method="post">
<input type="checkbox" value="image" name="image"> Include an image?<br>
<%
String str = request.getParameter("type");
if (str == null) str = (String)request.getAttribute("type");
Question question;
if (str.equals("mc")) {
	question = new MultipleChoice();
	out.println("<input type=\"checkbox\" name=\"multi-answer\" value=\"\"> Allow user to select multiple answers?<br><br>");
	out.println("<input type=\"text\" name=\"count\"> Number of choices?<br>");
} else {
	if (((String)request.getAttribute("fail")) != null) out.println("You cannot ask for more questions than there are answers, bro.");
	out.println("<br><input type=\"text\" name=\"count\"> Number of correct answers? (\"Name 5 states\" would have 50 correct answers, not 5)<br>");
	out.println("<input type=\"text\" name=\"numToAccept\"> Number of answers to ask for? (\"Name 5 states\" would ask for 5 answers)<br>");
	if (str.equals("fib")) {
		question = new FillInBlank();
	} else question = new QuestionResponse();
}
question.setQuestionNumber(String.valueOf(q.getQuestions().size() + 1));
q.addQuestion(question);

%>  
<br>
<button type="submit" name="submit" value="create">Create Question</button>  
<%
// If we are working on the first question, don't display this.
if (q.getQuestions().size() != 1) {
	out.println("<button type=\"submit\" name=\"submit\" value=\"submit\">Cancel Question and Submit Quiz</button>  ");
}
%>
<button type="submit" name="submit" value="cancel">Cancel Question and Continue</button>
</form>
</div>
</body>
</html>