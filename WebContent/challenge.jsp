<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="model.*" %>
<%@ page import="utils.Helper" %>
<%@ page import="java.util.Date" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="style.css" />
<script type="text/javascript" src="js/script.js"></script> 
<title>Challenge</title>
<%
	User user = (User)request.getSession().getAttribute("User");
%>
</head>
<body>
<%= Helper.banner() %>

<div id="wrap">
<%
	String otherName = request.getParameter("challenge");
	String quizID = request.getParameter("quizID");
	if (AccountManager.areFriends(user, otherName)) {
		Challenge c = new Challenge();
		c.setQuizID(quizID);
		c.setSenderName(user.getUsername());
		c.setReceiverName(otherName);
		c.setRead(false);
		c.send();
		out.println("<h2>Challenge sent successfully!</h2>");
	}
	else {
		out.println("<h2>Challenge failed to send due to invalid username.</h2>");
	}
	out.println("<a href=\"quiz-home.jsp?quizID=" + quizID + "\">Back To Quiz</a>");
%>
</div>
</body>
</html>