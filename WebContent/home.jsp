<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="model.*"%>
<%@ page import="utils.Helper"%>
<%@ page import="java.util.*"%>
<%@ page import="dao.business.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="style.css" />
<script type="text/javascript" src="js/script.js"></script> 
<title>Home</title>
</head>
<body>
<%= Helper.banner() %>
<%= Helper.searchBar() %>

<div id="wrap">
<h2><a href="quizzes.jsp">Quizzes</a></h2>
<div class="box1out">
<h4>Popular</h4>
<div class="box2">
<table>
<%
	final int NUM_RECENT = 3;

	User user = (User)request.getSession().getAttribute("User");
	if (user == null) {
		out.println("<h1>You have been signed out.</h1>");
		out.println("<h4><a href=\"home.jsp\">Home</a></h4>");
	}
	
	List<Quiz> popularQuizzes = QuizManager.getQuizzesByHits();
	int count = 0;
	if (popularQuizzes != null && popularQuizzes.size() != 0) {
		out.println("<tr class=\"tableHead\"><td>Quiz Name</td><td>Quiz Creator</td><td>Number of Hits</td><td>Number of Likes</td></tr>");
		for (int i = 0; i < popularQuizzes.size(); i++) {
			count++;
			Quiz q = popularQuizzes.get(i);
			out.println("<tr>");
			out.println("<td><a href=\"" + QuizManager.getURL(q.getQuizID()) + "\">" + q.getTitle() + "</a></td>");
			out.println("<td><a href=\"" + AccountManager.getURL(q.getCreatorName()) + "\">" + q.getCreatorName() + "</a></td>");
			out.println("<td>" + q.getHits() + "</td>");
			out.println("<td>" + q.getLikes() + "</td>");
			out.println("</tr>");
		}
	}
	if (count == 0) out.println("It appears that there aren't any popular quizzes.");
	
	
%>
</table>
</div>
<h4>Recent</h4>
<div class="box2">
<table>
<%
	List<Quiz> recentQuizzes = QuizManager.getRecentQuizzes();
	count = 0;
	if (recentQuizzes != null && recentQuizzes.size() != 0) {
		out.println("<tr class=\"tableHead\"><td>Quiz Name</td><td>Created by</td><td>How Long Ago</td>");
		for (int i = 0; i < recentQuizzes.size() && i < NUM_RECENT; i++) {
			count++;
			Quiz q = recentQuizzes.get(i);
			out.println("<tr>");
			out.println("<td><a href=\"" + QuizManager.getURL(q.getQuizID()) + "\">" + q.getTitle() + "</a></td>");
			out.println("<td><a href=\"" + AccountManager.getURL(q.getCreatorName()) + "\">" + q.getCreatorName() + "</a></td>");
			out.println("<td>" + QuizManager.timeSince(q.getCreateDate().getTime()) + "</td>");
			out.println("</tr>");
		}
	}
	if (count == 0) {
		out.println("There aren't any recent quizzes!");
	}
%>
</table>
</div>
<p><a href="create-quiz.jsp">Create your own quiz!</a></p>
</div>
<br>


<h3>You</h3>
<div class="box1out">
<h4>Your Quizzes</h4>
<div class="box2">
<table>
<%
	List<Creation> quizCreated = user.getRecentCreated();
	count = 0;
	if (quizCreated != null && quizCreated.size() != 0) {
		out.println("<tr class=\"tableHead\"><td>Quiz Name</td><td>How Long Ago</td></tr>");
		for (int i = 0; i < quizCreated.size() && i < NUM_RECENT; i++) {
			count++;
			Creation c = quizCreated.get(i);
			out.println("<tr>");
			out.println("<td><a href=\"" + QuizManager.getURL(c.getQuizID()) + "\">" + c.getQuizTitle() + "</a></td>");
			out.println("<td>" + QuizManager.timeSince(c.getDate().getTime()) + "</td>");
			out.println("</tr>");
		}
	}
	if (count == 0) out.println("You haven't created any quizzes recently.");
%>
</table>
</div>
<h4>Your Results</h4>
<div class="box2">
<table>
<%
	List<Result> quizTaken = user.getRecentQuizTaken();
	count = 0;
	if (quizTaken != null && quizTaken.size() != 0) {
		
		for (int i = 0; i < quizTaken.size() && i < NUM_RECENT; i++) {
			count++;
			Result res = quizTaken.get(i);
			out.println("<tr>");
			out.println("<td>" + res.getFormattedScore() + "% on " + "<a href=\"" + QuizManager.getURL(res.getQuizID())
					+ "\">" + res.getQuizTitle() + "</a>" + " in " + res.getTimeStr() + ", " 
					+ QuizManager.timeSince(res.getDate().getTime()) + "</td>");
			out.println("</tr>");
		}
	}
	if (count == 0) out.println("You haven't taken any quizzes recently.");
	
%>
</table>
</div>
<h4>Your Achievements</h4>
<div class="box2">
<table>
<%
	List<Achievement> achs = user.getAchievements();
	count = 0;
	if (achs != null) {
		for (int i = 0; i < achs.size() && i < NUM_RECENT; i++) {
			count++;
			Achievement a = achs.get(i);
			out.println("<tr>");
			out.println("<td>You unlocked " + a.getText() + "!</td>");
			out.println("</tr>");
		}
	}
	if (count == 0) out.println("You don't have any achievements.");
%>
</table>
</div>
<p><a href="history.jsp">See your whole history</a></p>
</div>
<br>

<h3>Your Friends</h3>
<div class="box1out">
<h4>Messages</h4>
<div class="box2">
<table>
<%
 	String username = user.getUsername();
	List<Message> unread = User.getUnreadMessages(username);
	int numUnread = 0;
	if (unread != null) {
		numUnread = unread.size();
	}
	if (numUnread == 1) {
		out.println("You have <a href=\"inbox.jsp\">1</a> message in your inbox.");
	} else {
		out.println("You have <a href=\"inbox.jsp\">" + numUnread + "</a> messages in your inbox");
	}
%>
</table>
</div>
<br>

<h4>Friends' Activity</h4>
<div class="box2">
<table>
<%
	//Need to get the userID from user...
	List<Activity> friendsActivities = user.getRecentFriendsActivities();
	count = 0;
	if (friendsActivities != null) {
		for (int i = 0; i < friendsActivities.size() && i < NUM_RECENT; i++) {
			Activity act = friendsActivities.get(i);
			String actUser = act.getUsername();
			String userURL = AccountManager.getURL(actUser);
			out.println("<tr>");
			if (act.getType() == Activity.ActivityType.RESULT) {
				//Quiz q = QuizManager.getQuiz(act.getUsername()); I think these are incorrect...
				Result res = (Result)act;
				Quiz q = QuizManager.getQuiz(res.getQuizID());
				out.println("<td>" + "<a href=\"" + userURL + "\">"+ actUser + "</a> scored " + res.getFormattedScore() + 
						"% on " + "<a href=\"" + QuizManager.getURL(q.getQuizID()) + "\">" + q.getTitle() + "</a></td>");
			}
			else if (act.getType() == Activity.ActivityType.CREATION) {
				//Quiz q = QuizManager.getQuiz(act.getUsername());
				Creation cr = (Creation)act;
				Quiz q = QuizManager.getQuiz(cr.getQuizID());
				out.println("<td>" + "<a href=\"" + userURL + "\">"+ actUser + "</a> created " + "<a href=\"" +
					QuizManager.getURL(q.getQuizID()) + "\">" + q.getTitle() + "</a></td>");
			}
			else {
				Achievement ach = (Achievement)act;
				out.println("<td>" + "<a href=\"" + userURL + "\">"+ actUser + "</a> unlocked the achivement \""
					+ ach.getText() + "\"!</td>");
			}
			out.println("</tr>");
			count++;
		}
	}
	if (count == 0) out.println("Your friends don't have any recent activities.");
%>
</table>
</div>
</div>
<br>

<h3>Announcements</h3>
<div class="box1out"><br>
<%
	List<Announcement> announcements = GeneralManager.getRecentAnnouncements();
	if (announcements == null || announcements.size() == 0) {
		out.println("<p>We apparently haven't made any announcements yet!</p>");
	} else {
		for (int i = 0; i < announcements.size() && i < NUM_RECENT; i++) {
			out.println("<div class=\"box2\">");
			Announcement a = announcements.get(i);
			out.println(a.getDate().toString() + "<br><br>" + a.getText());
			out.println("</div>");
		}
	}
	if (user.isAdmin()) {
		out.println("<br><br><a href=\"make-announcement.jsp\">Make an Announcement</a>");
	}
	
%>
</div>
</div>
</body>
</html>