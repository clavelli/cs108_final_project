<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ page import="java.util.*"%>
<%@ page import="utils.Helper.*" %>
<%@ page import="utils.Helper" %>
<%@ page import="model.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="style.css" />
<script type="text/javascript" src="js/script.js"></script> 
<title>Search Result</title>
</head>
<body>
<%= Helper.banner() %>
<div id="wrap">
	<h2>Search Result(s)</h2>
	<u>
	<%
	
	SearchOptions searchType = (SearchOptions)request.getAttribute("searchType");
	String url;
	
	switch(searchType) {
	case FRIENDS:
		List<String> friends = (List<String>)request.getAttribute("friends");
		if(friends != null) {
			for (String s: friends) {
				url = AccountManager.getURL(s);
				//out.println("<li><a href=\"someUrl\">myfriend</a></li>");
				out.println("<li><a href=\"" + url +"\">" + s + "</a></li>");
			}
		} else {
			out.println("<h2>0 Results found</h2>");
		}
		break;
	case QUIZZES:
		List<Quiz> quizzes = (List<Quiz>)request.getAttribute("quizzes");
		if(quizzes != null) {
			for (Quiz q: quizzes) {
				url = QuizManager.getURL(q.getQuizID());
				out.println("<li><a href=\"" + url + 
						"\">" + q.getTitle() + "</a></li>");
			}
		} else {
			out.println("<h2>0 Results found</h2>");
		}
		break;
	case USERS:
		List<User> users = (List<User>)request.getAttribute("users");
		String username;
		if(users != null) {
			for (User u: users) {
				username = u.getUsername();
				url = AccountManager.getURL(username);
				out.println("<li><a href=\"" + url +"\">" + username + "</a></li>");
			}
		} else {
			out.println("<h2>0 Results found</h2>");
		}
		break;
	default:
		break;
	}
	
	%>
	</u>
</div>
</body>
</html>