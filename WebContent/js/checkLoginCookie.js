var cookies;

function init() {
	//console.log("Initializing");
	
	//Get form
	var form = document.getElementById("loginForm");
	
	//Retrieve cookie
	cookies = getCookies();
	
	//Set form element id to cookie values
	var username = getValue("quizzle_username");
	var password = getValue("quizzle_password");
	
	console.log("QueryString: " + getQueryStrings());
	
	//Check if logout was request.  If yes, don't resubmit form
	var queryString = getQueryStrings();
	var logOut = queryString["logout"]; 
	
	if(logOut != "true") {
		if((username != null) && (password != null)) {
			document.getElementById("username").value = username;
			document.getElementById("password").value = password;
			
			form.submit();
		}
	} else {
		//clear cookies
		deleteCookies();
	}
}

function deleteCookies() {
	var mydate = new Date();
	mydate.setTime(mydate.getTime() - 1);
	document.cookie = "quizzle_username=; expires=" + mydate.toGMTString();
	document.cookie = "quizzle_password=; expires=" + mydate.toGMTString();
}

function getCookies() {
	var cookies = {};			//object of cookies
	var all = document.cookie;	//all cookies in one string
	
	if (all ==="") return cookies; 	//empty cookie
	
	var list = all.split("; ");	//split into name=value pairs
	for(var i = 0; i < list.length; i++) {
		var cookie = list[i];
		var p= cookie.indexOf("=");
		var name = cookie.substring(0,p);	//get cookie name
		var value = cookie.substring(p+1);	//get cookie value
		value = decodeURIComponent(value); 	//decode the value
		cookies[name] = value;				//store name/value in object
	}
	return cookies;
}

function getValue(name) {
	return cookies[name] || null;
}

function getQueryStrings() { 
	  var assoc  = {};
	  var decode = function (s) { 
		  return decodeURIComponent(s.replace(/\+/g, " ")); 
	  };
	  
	  var queryString = location.search.substring(1); 
	  var keyValues = queryString.split('&'); 

	  for(var i in keyValues) { 
	    var key = keyValues[i].split('=');
	    if (key.length > 1) {
	      assoc[decode(key[0])] = decode(key[1]);
	    }
	  } 

	  return assoc; 
} 
