function validateSearchString(form) {
	var searchString = document.getElementById("searchString").value;
	
	if(hasText(searchString)) {
		form.submit();
	}
}

function hasText(text) {
	if (text.length > 0) return true;
	else return false;
}

function callSearchServlet(searchString) {
	 var xmlHttp = null;
	 
	 var e = document.getElementById("searchType");
	 var selectedType = e.options[e.selectedIndex].value;
	 
	 var url = "SearchServlet" + "?searchType=" + selectedType + "&searchString="+searchString
	 
   xmlHttp = new XMLHttpRequest();
   xmlHttp.open( "GET", url, true );
   xmlHttp.send( null );
   return xmlHttp.responseText;
}