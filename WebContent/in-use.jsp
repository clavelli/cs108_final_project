<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="style.css" />
<script type="text/javascript" src="js/script.js"></script> 
<title>Create Account</title>
</head>
<body>
<div id="header"><span id="logo">Quizzle</span></div>
<div id="wrap">
<h1>The name "<%= request.getParameter("username") %>" is already in use</h1>
Please enter another proposed name and password.

<form action="CreateAccountServlet" method="post">
<p>User Name:<input type="text" name="username" /></p>
<p>Password:<input type="password" name="password" /> <input type="submit" value="Submit" /></p>
</form>
<a href="index.jsp">Login</a>
</div>
</body>
</html>