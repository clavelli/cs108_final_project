<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="model.*" %>
<%@ page import="utils.Helper" %>
<%@ page import="java.util.*" %>
<%@ page import="java.lang.StringBuilder" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="style.css" />
<script type="text/javascript" src="js/script.js"></script> 
<%
	User user = (User)request.getSession().getAttribute("User");
	String username = user.getUsername();
	
	String quizID = request.getParameter("quizID");
	if (quizID == null) quizID = (String) request.getAttribute("quizID");	

	if (request.getParameter("like") != null) {
		QuizManager.addLike(quizID);
	}

	Quiz q = QuizManager.getQuiz(quizID);
	
	if (q == null) {
		out.println("<h1>Quiz Not Found!</h1>");
		out.println("<h4><a href=\"home.jsp\">Home</a></h4>");
		return;
	}
	
	request.getSession().setAttribute("Quiz", q);
%>
<title><%= q.getTitle() %></title>
</head>
<body>
<%= Helper.banner() %>
<div id="wrap">
<h1><%= q.getTitle() %></h1>
<h3>
<%
	String creatorName = q.getCreatorName();
	String url = "Created by: <a href=\"" + AccountManager.getURL(creatorName) + "\">" + creatorName + "</a>";
	out.println(url);
%>
</h3>

<br>
<h3>Description</h3>
<div class="box1">
<%= q.getDescription() %>
</div>
<br>

<h3>Stats</h3>
<div class="box1">
<%
	String avg = QuizManager.getAverageScoreStr(quizID);
	String time = QuizManager.getAverageTimeStr(quizID);
	if (avg == null) {
		avg = "0.0";
	}
	if (time == null) {
		time = "00:00";
	}
%>
<div>Average Score: <%= avg %>%</div>
<div>Average Time: <%= time %></div>
<div>Total Hits: <%= q.getHits() %></div>
<div>Likes: <%= q.getLikes() %></div>
</div>
<br>

<%!
	String getResultsHTML(List<Result> results) {
		StringBuilder html = new StringBuilder();
		if (results == null || results.size() == 0) {
			html.append("<p>No results yet!</p>");
		}
		else {
			html.append("<tr class=\"tableHead\"><td>User</td><td>Score</td><td>Time</td></tr>");
			for (int i = 0; i < results.size(); i++) {
				Result res = results.get(i);
				String username = res.getUsername();
				html.append("<tr>\n"
					+ "<td><a href=\"" + AccountManager.getURL(username) + "\">" + res.getUsername() + "</a></td>\n"
					+ "<td>" + res.getFormattedScore() + "%</td>\n"
					+ "<td>" + res.getTimeStr() + "</td>\n"
					+ "</tr>");
			}
		}
		
		return html.toString();
	}
%>

<h3>Top Scores</h3>
<div class="box1out">
<h4>Today</h4>
<div class="box2">
<table>
<%
	List<Result> topRecentResults = q.getTopRecentResults();
	out.println(getResultsHTML(topRecentResults));
%>
</table>
</div>
<h4>All-Time</h4>
<div class="box2">
<table>
<%
	List<Result> topResults = q.getTopResults();
	out.println(getResultsHTML(topResults));
%>
</table>
</div>
</div>
<br>

<form action="challenge.jsp" method="post">
Challenge a Friend: <input type="text" name="challenge" /><input type="submit" value="Challenge" />
<input type="hidden" name="quizID" value="<%= quizID %>" />
</form>
<br><br>

<h3>Recent Results</h3>
<div class="box1">
<table>
<%
	List<Result> recentResults = q.getRecentResults();
	out.println(getResultsHTML(recentResults));
%>
</table>
</div>
<br>

<h3>Your Results</h3>
<div class="box1">
<table>
<%
	List<Result> yourResults = q.getRecentResultsForUser(user.getUsername());
	out.println(getResultsHTML(yourResults));
%>
</table>
</div>
<br><br>

<form action="StartQuizServlet" method="post" class="formFormat">
<input type="submit" value="Start" />
</form>
<form action="quiz-home.jsp" method="post" class="formFormat">
<input type="hidden" name="quizID" value="<%= quizID %>" /> 
<input type="submit" value="Like" />
<input type="hidden" name="like" />
</form>

<%
	if (user.isAdmin()) {
		/*
		out.print("<form action=\"DeleteQuizServlet\" method=\"post\" class=\"formFormat\">");
		out.print("<input type=\"submit\" value=\"Delete\" />");
		out.print("</form>");*/
		out.print("<form action=\"ClearQuizServlet\" method=\"post\" class=\"formFormat\">");
		out.print("<input type=\"submit\" value=\"Clear Quiz History\" />");
		out.println("</form>");
	}
%>
</div>
</body>


</html>