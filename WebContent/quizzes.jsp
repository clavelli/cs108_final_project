<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="model.*" %>
<%@ page import="utils.Helper" %>
<%@ page import="java.util.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="style.css" />
<script type="text/javascript" src="js/script.js"></script> 
<title>Quizzes</title>
</head>
<body>
<%= Helper.banner() %>
<%= Helper.searchBar() %>
<div id="wrap">

<h3>Most Liked</h3>
<div class="box1">
<table>
<%
	List<Quiz> mostLiked = QuizManager.getQuizzesByLikes();
	if (mostLiked != null) {
		out.println("<tr class=\"tableHead\"><td>Quiz Name</td><td>Quiz Creator</td><td>Likes</td></tr>");
		for (int i = 0; i < mostLiked.size(); i++) {
			Quiz q = mostLiked.get(i);
			out.println("<tr>");
			out.println("<td><a href=\"" + QuizManager.getURL(q.getQuizID()) + "\">" + q.getTitle() + "</a></td>");
			out.println("<td><a href=\"" + AccountManager.getURL(q.getCreatorName()) + "\">" + q.getCreatorName() + "</a></td>");
			out.println("<td>" + q.getLikes() + "</td>");
			out.println("</tr>");
		}
	}
%>
</table>
</div>
<br>

<h3>Most Taken</h3>
<div class="box1">
<table>
<%
	List<Quiz> mostTaken = QuizManager.getQuizzesByHits();
	if (mostTaken != null) {
		out.println("<tr class=\"tableHead\"><td>Quiz Name</td><td>Quiz Creator</td><td>Hits</td></tr>");
		for (int i = 0; i < mostTaken.size(); i++) {
			Quiz q = mostTaken.get(i);
			out.println("<tr>");
			out.println("<td><a href=\"" + QuizManager.getURL(q.getQuizID()) + "\">" + q.getTitle() + "</a></td>");
			out.println("<td><a href=\"" + AccountManager.getURL(q.getCreatorName()) + "\">" + q.getCreatorName() + "</a></td>");
			out.println("<td>" + q.getHits() + "</td>");
			out.println("</tr>");
		}
	}
%>
</table>
</div>
<br>

<h3>Recently Created</h3>
<div class="box1">
<table>
<%
	List<Quiz> recentQuizzes = QuizManager.getRecentQuizzes();
	if (recentQuizzes != null) {
		out.println("<tr class=\"tableHead\"><td>Quiz Name</td><td>Quiz Creator</td><td>How Long Ago</td></tr>");
		for (int i = 0; i < recentQuizzes.size(); i++) {
			Quiz q = recentQuizzes.get(i);
			out.println("<tr>");
			out.println("<td><a href=\"" + QuizManager.getURL(q.getQuizID()) + "\">" + q.getTitle() + "</a></td>");
			out.println("<td><a href=\"" + AccountManager.getURL(q.getCreatorName()) + "\">" + q.getCreatorName() + "</a></td>");
			out.println("<td>" + QuizManager.otherGetTimeStr(System.currentTimeMillis() - q.getCreateDate().getTime()) + "</td>");
			out.println("</tr>");
		}
	}
%>
</table>
</div>
</div>
</body>
</html>