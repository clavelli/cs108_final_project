<%@page import="utils.*"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*" %>
<%@ page trimDirectiveWhitespaces="true" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link rel="stylesheet" type="text/css" href="style.css" />
	<script type="text/javascript" src="js/script.js"></script> 
	<script type="text/javascript" src="js/checkLoginCookie.js"></script> 
	<title>Login</title>
</head>


<body onload="init();">
<div id="header"><span id="logo">Quizzle</span></div>

<div id="wrap">
	<h1>Welcome to Quizzle!</h1>
	Please log in.
	
	<form action="LoginServlet" method="post" id="loginForm">
	<p>User Name: <input type="text" name="username" id="username"/></p>
	<p>Password: <input type="password" name="password" id="password"/> <input type="submit" value="Login" /></p>
	</form>
	
	<a href="create-account.html">Create New Account</a>
	
		
	<%

	%>
	
</div>
</body>

</html>