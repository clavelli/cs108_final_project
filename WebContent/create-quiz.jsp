<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="model.*"%>
<%@ page import="utils.Helper"%>
<%@ page import="java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="style.css" />
<script type="text/javascript" src="js/script.js"></script> 
<title>Quiz Creation</title>
</head>
<body>
<%= Helper.banner() %>
<%= Helper.searchBar() %>

<div id="wrap">
<h2>Create a quiz!</h2>
<%
String str = (String) request.getAttribute("fail");
if (str != null) {
	out.println("<font color=\"red\">You must fill out the " + str + " field.</font>");
}
%>

<form action="CreateQuizServlet" method="post">
Title: <br>
<textarea name="title" rows="1" cols="40"></textarea><br>
Description: <br>
<textarea name="description" rows="5" cols="40"></textarea><br><br> 
<input name="random" type="checkbox" value="r"> Randomly order questions? <br><br><br>
Quiz Type: <br>
<table>
<tr><td><input type="radio" name="type" value="s" checked> Single-page quiz</td></tr>
<tr><td><input type="radio" name="type" value="mi"> Multiple-page quiz, immediate correction</td></tr>
<tr><td><input type="radio" name="type" value="m"> Multiple-page quiz, delayed correction</td></tr>
</table>
<br>
<button type="submit">Create Quiz</button>
</form>
<br>
</div>
</body>
</html>