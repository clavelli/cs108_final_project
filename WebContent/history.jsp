<%@page import="dao.business.UserDB"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="model.*"%>
<%@ page import="java.util.*" %>
<%@ page import="utils.Helper" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="style.css" />
<script type="text/javascript" src="js/script.js"></script> 
<%
	User user = (User)request.getSession().getAttribute("User");
	String username = user.getUsername();
	final int NUM_TO_GET = 100;
	final int NUM_MINUTES = (int)System.currentTimeMillis() / 60000;
%>

<title>Your History</title>
</head>
<body>
<%= Helper.banner() %>
<div id="wrap">
<h1>Your History</h1>
<%
	List<Activity> activities = user.getRecentActivities(NUM_MINUTES, NUM_TO_GET);
	if (activities == null) activities = new java.util.ArrayList<Activity>();
	for (int i = activities.size() - 1; i >= 0; i--) {
		Activity a = activities.get(i);
		out.println("<div class=\"box1out\">");
		if (a.getType() == Activity.ActivityType.ACHIEVEMENT) {
			out.println("You unlocked " + ((Achievement)a).getText() + "!<br>");
		}
		else if (a.getType() == Activity.ActivityType.CREATION) {
			Creation c = (Creation)a;
			Quiz q = QuizManager.getQuiz(c.getQuizID());
			out.println("You created the quiz " + q.getTitle() + "!");
		}
		else {
			Result r = (Result) a;
			Quiz q = QuizManager.getQuiz(r.getQuizID());
			out.println("You scored " + r.getFormattedScore() + " on the quiz " + r.getQuizTitle() + "!");
		}
		out.println("</div>");
	}
%>
</div>
</body>
</html>


