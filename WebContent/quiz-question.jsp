<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="model.*" %>
<%@ page import="utils.Helper" %>
<%@ page import="java.util.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="style.css" />
<script type="text/javascript" src="js/script.js"></script> 
<%
	Quiz q = (Quiz)request.getSession().getAttribute("Quiz");
%>
<title><%= q.getTitle() %></title>
</head>
<body>
<%= Helper.banner() %>
<div id="wrap">
<h1><%= q.getTitle() %></h1>

<%
	if (q.getImmCorretion()) {
		out.println("<form action=\"question-feedback.jsp\" method=\"post\">");	
	}
	else {
		out.println("<form action=\"TakeQuizServlet\" method=\"post\">");
	}

	Question question = q.getCurrent();
	out.println("<h3>Question " + (q.getCurrentNumber() + 1) + "</h3>");
	out.println("<div class=\"box1\">");
	String pictureURL = question.getPictureURL();
	if (pictureURL != null) {
		out.println("<img src=\"" + pictureURL + "\" />");
	}
	
	if (question.getType() == Question.QuestionType.FILL_IN_BLANK) {
		FillInBlank f = (FillInBlank)question;
		
		if (f.getText1() != null) {
			out.println("<p>" + f.getText1() + " ");
		}
		else {
			out.println("<p>");
		}
		int numAnswers = f.getNumAnswers();
		for (int j = 0; j < numAnswers; j++) {
			out.print("<input type=\"text\" name=\"" + f.getQuestionID() + "\" />");
			if (j != numAnswers - 1) {
				out.print(", ");
			}
		}
		if (f.getText2() != null) {
			out.println(" " + f.getText2() + "</p>");
		}
		else {
			out.println("</p>");
		}
	}
	else if (question.getType() == Question.QuestionType.MULTIPLE_CHOICE) {
		MultipleChoice mc = (MultipleChoice)question;
		
		out.println("<p>" + mc.getText() + "</p>");
		List<Choice> choices = mc.getChoices();
		String inputType = "checkbox";
		if (mc.isSingleAnswer()) {
			inputType = "radio";
		}
		out.println("<ul style=\"list-style-type: none;\">");
		for (int j = 0; j < choices.size(); j++) {
			out.println("<li><input type=\"" + inputType + "\" name=\"" + mc.getQuestionID() + "\" value=\"" 
				+ j + "\" /> " + choices.get(j).getText() + "</li>"); // May need to fix to use localID
		}
		out.println("</ul>");
	}
	else {
		QuestionResponse qr = (QuestionResponse)question;
		
		out.println("<p>" + qr.getText() + "</p>");
		out.println("<ul style=\"list-style-type: none;\">");
		for (int j = 0; j < qr.getNumAnswers(); j++) {
			out.println("<li><input type=\"text\" name=\"" + qr.getQuestionID() + "\" /></li>");
		}
		out.println("</ul>");
	}

	if (!q.getImmCorretion()) {
		out.println("<input type=\"submit\" value=\"Next Question\" />");
	}
	else {
		out.println("<input type=\"submit\" value=\"Grade\" />");
	}
	out.println("</div>");
	out.println("</form>");
%>
</div>
</body>
</html>