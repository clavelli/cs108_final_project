<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ page import="model.*" %>
<%@ page import="java.util.*" %>
<%@ page import="utils.Helper" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="style.css" />
<script type="text/javascript" src="js/script.js"></script> 
<title>Create Question</title>
</head>
<body>
<%= Helper.banner() %>
<%= Helper.searchBar() %>
<div id="wrap">
<%
Quiz q = (Quiz)session.getAttribute("quiz");
MultipleChoice question = (MultipleChoice)q.getLastQuestion();
out.println("<h3>Question #" + question.getQuestionNumber() + "</h3>");
%>
<form action="CreateQuestionServlet" method="post">
Question:
<br>
<textarea name="text" cols="50"></textarea><br>

<%
if (request.getAttribute("image") != null) out.println("Image url:<br><input type=\"text\" name=\"image\"><br>");
int count = Integer.valueOf(request.getParameter("count"));
out.println("<input type=\"hidden\" name=\"count\" value=\"" + count + "\"><br>");
if (count > 1) {
	out.println("<input type=\"checkbox\" name=\"ordered\"> The choices should always appear in the same order<br><br>");
}
String str1 = "";
String str2 = "is";
String boxType = "checkbox";
out.println("<table>");
if (question.isSingleAnswer()) {
	out.println("Check the answer that is correct.<br>");
	String str = " checked "; 
	for (int i = 0; i < count; i++) {
		out.println("<tr><td><input type=\"radio\" name=\"correct\" value=\"" + i + "\"" + str + ">");
		str = "";
		out.println("<input type=\"text\" name=\"choice" + i + "\"></td></tr>");
	}
} else {
	out.println("Check the answers that are correct.<br>");
	for (int i = 0; i < count; i++) {
		out.println("<tr><td><input type=\"checkbox\" name=\"correct" + i + "\">");
		out.println("<input type=\"text\" name=\"choice" + i + "\"></td></tr>");
	}
}
out.println("</table>");
%>
<button type="submit" name="submit" value="c">Create Question</button>  
<button type="submit" name="submit" value="x">Cancel Question</button>  
<button type="submit" name="submit" value="cs">Create Question and Submit Quiz</button>  
<button type="submit" name="submit" value="xs">Cancel Question and Submit Quiz</button>
</form>
</div>
</body>
</html>