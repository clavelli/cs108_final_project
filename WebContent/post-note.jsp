<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="model.*" %>
<%@ page import="utils.Helper" %>
<%@ page import="java.util.Date" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="style.css" />
<script type="text/javascript" src="js/script.js"></script> 
<title>Note</title>
<%
	User user = (User)request.getSession().getAttribute("User");
%>
</head>
<body>
<%= Helper.banner() %>

<div id="wrap">
<%
	String otherName = request.getParameter("receiver");
	if (AccountManager.areFriends(user, otherName)) {
		String body = request.getParameter("body");
		Message m = new Message(Message.MessageType.NOTE);
		m.setSenderName(user.getUsername());
		m.setReceiverName(otherName);
		m.setBody(body);
		m.setRead(false);
		m.send();
		out.println("<h2>Note sent successfully to " + otherName + "!</h2>");
	}
	else {
		out.println("<h2>Note failed to send! " + otherName + " is not one of your friends.</h2>");
	}
	out.println("<a href=\"inbox.jsp\">Back To Inbox</a>");
%>
</div>
</body>
</html>