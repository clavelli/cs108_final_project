<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="model.*" %>
<%@ page import="java.util.*" %>
<%@ page import="utils.Helper" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="style.css" />
<script type="text/javascript" src="js/script.js"></script> 
<title>Question Type</title>
</head>
<body>
<%= Helper.banner() %>
<%= Helper.searchBar() %>
<div id="wrap">
<%
Quiz q = (Quiz) session.getAttribute("quiz");
out.println("<h3>Question #" + (q.getQuestions().size() + 1) + "</h3>");
%>
<form action="question-properties.jsp">
Select Question Type:<br><br>
<select name="type">
<option value="qr">Question Response</option>
<option value="fib">Fill in the Blank</option>
<option value="mc">Multiple Choice</option>
</select>
<br><br>
<button type="submit" name="submit" value="c">Create</button>
</form>

<%
if (q.getQuestions().size() != 0) {
	out.println("<form action=\"SubmitQuizServlet\" method=\"post\" class=\"formFormat\">");
	out.println("<button type=\"submit\" name=\"submit\" value=\"x\">Cancel Question and Submit Quiz</button>");
	out.println("</form>");
}
%>

<br>
</div>
</body>
</html>