<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="model.*"%>
<%@ page import="java.util.*" %>
<%@ page import="utils.Helper" %>
<%@ page import="dao.business.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="style.css" />
<script type="text/javascript" src="js/script.js"></script> 
<%
	User user = (User)request.getSession().getAttribute("User");
	String username = user.getUsername();
	final int NUM_TO_GET = 10;
%>

<title>Inbox</title>
</head>
<body>
<%= Helper.banner() %>
<div id="wrap">
<h1>Inbox</h1>

<form action="make-note.jsp">
<input type="submit" value="Create New Note" /><br><br>
</form>

<%
	List<Message> messages = User.getMessages(username);
	if (messages == null) {
		return;
	}
	
	for (int i = messages.size() - 1; i >= 0; i--) {
		Message m = messages.get(i);
		MessageDB.markMessageRead(m.getMessageID());
		request.setAttribute("Message", m);
		out.println("<div class=\"box1out\">");
		if (m.getRead()) {
			out.println("<p>" + m.getTypeStr() + " from " + m.getSenderName() + "</p>");
		} else {
			out.println("<h4>" + m.getTypeStr() + " from " + m.getSenderName() + "</h4>");
		}
		out.println("<div class=\"box2\">");
		if (m.getType() == Message.MessageType.FRIEND_REQUEST) {
			out.println(m.getSenderName() + " sent you a friend request.<br><br>");
			if (!AccountManager.areFriends(user, m.getSenderName())) {
				out.println("<form action=\"FriendRequestServlet\" method=\"post\">");
				out.println("<input type=\"submit\" name=\"request\" value=\"Accept\" /> ");
				//out.println("<button type=\"submit\" name=\"request\" value=\"Decline\"></button>");
				out.println("<input type=\"hidden\" name=\"sender\" value=\"" + m.getSenderName() + "\" />");
				out.println("</form>");
			}
			else {
				out.println("<br>You and " + m.getSenderName() + " are friends!");
			}
		}
		else if (m.getType() == Message.MessageType.CHALLENGE) {
			Challenge c = (Challenge)m;
			Quiz q = QuizManager.getQuiz(c.getQuizID());
			out.println(c.getSenderName() + " challenged you to take the quiz \"" + q.getTitle() + "\"!<br>");
			List<Result> userResults = q.getTopResultsForUser(c.getSenderName());
			if (userResults == null || userResults.size() == 0) {
				out.println(c.getSenderName() + " has not taken this quiz yet.<br><br>");
			}
			else {
				out.println(c.getSenderName() + "'s highest score is " + userResults.get(0).getFormattedScore() + "%<br><br>");
			}
			out.println("Here is the link to the quiz: <a href=\"quiz-home.jsp?quizID=" + c.getQuizID() + "\">" + q.getTitle() + "</a>");
		}
		else {
			out.println("<p>" + m.getBody() + "</p>");
		}
		out.println("</div>");
		out.println("</div>");
		m.setRead(true);
	}
%>
</div>
</body>
</html>