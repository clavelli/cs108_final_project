<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ page import="model.*" %>
<%@ page import="java.util.*" %>
<%@ page import="utils.Helper" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="style.css" />
<script type="text/javascript" src="js/script.js"></script> 
<title>Create Question</title>
</head>
<body>
<%= Helper.banner() %>
<%= Helper.searchBar() %>
<div id="wrap">
<%
Quiz q = (Quiz)session.getAttribute("quiz");
FillInBlank question = (FillInBlank)q.getLastQuestion();
out.println("<h3>Question #" + question.getQuestionNumber() + "</h3>");
%>
<form action="CreateQuestionServlet" method="post">
Question Part 1 (before the blank):
<br>
<textarea name="text1" cols="50"></textarea><br>
Question Part 2 (after the blank):
<br>
<textarea name="text2" cols="50"></textarea><br>

<%
if (request.getAttribute("image") != null) out.println("Image url:<br><input type=\"text\" name=\"image\"><br>");
int count = Integer.valueOf(request.getParameter("count"));
out.println("<input type=\"hidden\" name=\"count\" value=\"" + count + "\"><br>");
if (count > 1) {
	out.println("<input type=\"checkbox\" name=\"ordered\"> The quiz taker is required to enter answers in order<br><br>");
}
out.println("Separate variations of the same answer with a *. (i.e. Washington * George Washington).");
out.println("Don't worry about caps.<br><br>");
for (int i = 0; i < count; i++) {
	out.println("Answer #" + (i + 1) + "<br>");
	out.println("<input type=\"text\" name=\"choice" + i + "\"><br>");
}
%>
<br>
<button type="submit" name="submit" value="c">Create Question</button>  
<button type="submit" name="submit" value="x">Cancel Question</button>  
<button type="submit" name="submit" value="cs">Create Question and Submit Quiz</button>  
<button type="submit" name="submit" value="xs">Cancel Question and Submit Quiz</button>
</form>
</div>
</body>
</html>