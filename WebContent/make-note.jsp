<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="utils.Helper" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="style.css" />
<script type="text/javascript" src="js/script.js"></script> 
<title>Make Note</title>

</head>
<body>
<%= Helper.banner() %>

<div id="wrap">
<h2>Make Note</h2>
<form action="post-note.jsp" method="post">
<h4>To: <input type="text" name="receiver" /></h4>
<h4>Note body:</h4>
<textarea rows="8" cols="60" name="body"></textarea>
<button type="submit">Send Note</button>
</form>
</div>

</body>
</html>