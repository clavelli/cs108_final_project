<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="model.*" %>
<%@ page import="dao.business.*" %>
<%@ page import="dao.*" %>
<%@ page import="java.util.*" %>
<%@ page import="utils.Helper" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="style.css" />
<script type="text/javascript" src="js/script.js"></script> 
<%
User user = (User)request.getSession().getAttribute("User");
// username is the name of the user whose page we are looking at, not the name of user.
String username = request.getParameter("username");
out.println("<title>" + username + "</title>");
SelectResponse<User> userResponse = UserDB.getUser(username);
User pageUser = userResponse.getDTO();
%>
</head>

<body>
<%= Helper.banner() %>
<%= Helper.searchBar() %>
<div id="wrap">
<%
final int NUM_TO_SHOW = 3;
out.println("<h1>" + username + "</h1>");

String AddRemove = "Add";
String addRemove = "add";
List<String> friendNames = user.getFriendNames();
if (friendNames != null && friendNames.contains(username)) {
	AddRemove = "Remove";
	addRemove = "remove";
}
%>
<br>
<form action="AddRemoveServlet" method="post">
<%
out.println("<input type=\"hidden\" name=\"pageUsername\" value=\"" + username + "\" />");
if (!user.getUsername().equals(username)) {
	out.println("<button type=\"submit\" name=\"submit\" value=\"" + addRemove + "\">" + AddRemove + " " + username + "</button><br>");
}

if (user.isAdmin()) {
	/*out.println("<button type=\"submit\" name=\"submit\" value=\"delete\">Delete Account</button>");*/
	if (!pageUser.isAdmin()) {
		out.println("<button type=\"submit\" name=\"submit\" value=\"promote\">Promote user to Admin</button>");
	}
}
%>
<br>

</form>
<h3><%= username %>'s Quizzes</h3>
<div class="box1out">
<h4>Recent Results</h4>
<div class="box2">
<table>
<%
	List<Result> recentResults = pageUser.getRecentQuizTaken();
	int count = 0;
	if (recentResults != null) {
		for (int i = 0; i < NUM_TO_SHOW && i < recentResults.size(); i++) {
			count++;
			Result result = recentResults.get(i);
			out.println("<tr>");
			out.println("<td>" + username + " scored " + result.getFormattedScore() + "% on " + result.getQuizTitle() + "</td>");
			out.println("</tr>");
		}
	}
	if (count == 0) out.println(username + " hasn't taken any quizzes recently.");
%>
</table>
</div>
<h4>Recently Created</h4>
<div class="box2">
<table>
<%
	List<Creation> recentCreated = pageUser.getRecentCreated();
	count = 0;
	if (recentCreated != null && recentCreated.size() != 0) {
		out.println("<tr class=\"tableHead\"><td>Quiz Name</td><td>How Long Ago</td></tr>");
		for (int i = 0; i < recentCreated.size() && i < NUM_TO_SHOW; i++) {
			count++;
			Creation c = recentCreated.get(i);
			out.println("<tr>");
			out.println("<td><a href=\"" + QuizManager.getURL(c.getQuizID()) + "\">" + c.getQuizTitle() + "</a></td>");
			out.println("<td>" + QuizManager.timeSince(c.getDate().getTime()) + "</td>");
			out.println("</tr>");
		}
	}
	if (count == 0) out.println(username + " hasn't created any quizzes.");
	
%>
</table>
</div>
</div>
<br>

<h3><%= username %>'s Achievements</h3>
<div class="box1">
<table>
<%
	List<Achievement> achs = user.getAchievements();
	count = 0;
	if (achs != null) {
		for (int i = 0; i < achs.size(); i++) {
			count++;
			Achievement a = achs.get(i);
			out.println("<tr>");
			out.println("<td>You unlocked " + a.getText() + "!</td>");
			out.println("</tr>");
		}
	}
	if (count == 0) out.println(username + " doesn't have any achievements.");
%>
</table>
</div>
</div>
</body>
</html>