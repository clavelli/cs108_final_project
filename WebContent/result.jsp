<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="model.*"%>
<%@ page import="utils.Helper" %>
<%@ page import="java.util.*" %>
<%@ page import="dao.business.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="style.css" />
<script type="text/javascript" src="js/script.js"></script> 
<%
	Quiz q = (Quiz)request.getSession().getAttribute("Quiz");
%>
<title><%= q.getTitle() %></title>
</head>
<body>
<%= Helper.banner() %>
<div id="wrap">
<h1><%= q.getTitle() %></h1>
<h3>
<%
	String creatorName = q.getCreatorName();
	String url = "Created by: <a href=\"" + AccountManager.getURL(creatorName) + "\">" + creatorName + "</a>";
	out.println(url);
	
	double score = (Double)request.getSession().getAttribute("Score");
%>
</h3>
<br><br>

<h3>Result:</h3>
<div class="box1">
<p>You scored: <%= QuizManager.formattedScore(q, score) %>% in <%= QuizManager.getTimeStr(q.getElapsedTime()) %>!</p>
</div>

<br><br>
<%
	User u = (User)request.getSession().getAttribute("User");
	Result result = new Result(q.getQuizID(), u.getUsername(), new Date(), q.getElapsedTime(), score);
	QuizDB.addResult(result);
	q.addHit();

	List<Question> questions = q.getQuestions();

	for (int i = 0; i < questions.size(); i++) {
		Question question = questions.get(i);
		
		out.println("<h3>Question " + (i + 1) + "</h3>");
		out.println("<div class=\"box1\">");
		String pictureURL = question.getPictureURL();
		if (pictureURL != null) {
			out.println("<img src=\"" + pictureURL + "\" />");
		}
		
		if (question.getType() == Question.QuestionType.FILL_IN_BLANK) {
			FillInBlank f = (FillInBlank)question;
			String[] attempts;
			if (q.getSinglePage()) {
				attempts = request.getParameterValues(f.getQuestionID());
			}
			else {
				attempts = (String[])request.getSession().getAttribute(f.getQuestionID());
			}
			
			if (f.getText1() != null) {
				out.println("<p>" + f.getText1() + " ");
			}
			else {
				out.println("<p>");
			}
			for (int j = 0; j < attempts.length; j++) {
				if (attempts != null) {
					out.print("<input type=\"text\" name=\"" + f.getQuestionID() + "\" value=\"" + attempts[j] + "\"/>");
				}
				else {
					out.print("<input type=\"text\" name=\"" + f.getQuestionID() + "\"/>");
				}
				
				if (j != attempts.length - 1) {
					out.print(", ");
				}
			}
			if (f.getText2() != null) {
				out.println(" " + f.getText2() + "</p>");
			}
			else {
				out.println("</p>");
			}
		}
		else if (question.getType() == Question.QuestionType.MULTIPLE_CHOICE) {
			MultipleChoice mc = (MultipleChoice)question;
			String qID = mc.getQuestionID();
			
			out.println("<p>" + mc.getText() + "</p>");
			List<Choice> choices = mc.getChoices();
			String inputType = "checkbox";
			if (mc.isSingleAnswer()) {
				inputType = "radio";
			}
			
			String[] values;
			if (q.getSinglePage()) {
				values = request.getParameterValues(mc.getQuestionID());
			}
			else {
				values = (String[])request.getSession().getAttribute(mc.getQuestionID());
			}
			
			if (values != null) {
				List<String> valuesList = Arrays.asList(values);
				out.println("<ul style=\"list-style-type: none;\">");
				for (int j = 0; j < choices.size(); j++) {
					if (valuesList.contains(String.valueOf(j))) {
						out.println("<li><input type=\"" + inputType + "\" name=\"" + qID + "\" value=\"" 
							+ j + "\" checked/> " + choices.get(j).getText() + "</li>");
					}
					else {
						out.println("<li><input type=\"" + inputType + "\" name=\"" + qID + "\" value=\"" 
							+ j + "\" /> " + choices.get(j).getText() + "</li>");
					}
				}
				out.println("</ul>");
			}
			else {
				out.println("<ul style=\"list-style-type: none;\">");
				for (int j = 0; j < choices.size(); j++) {
					out.println("<li><input type=\"" + inputType + "\" name=\"" + qID + "\" value=\"" 
						+ j + "\" /> " + choices.get(j).getText() + "</li>");
				}
				out.println("</ul>");
			}
		}
		else {
			QuestionResponse qr = (QuestionResponse)question;
			String[] attempts;
			if (q.getSinglePage()) {
				attempts = request.getParameterValues(qr.getQuestionID());
			}
			else {
				attempts = (String[])request.getSession().getAttribute(qr.getQuestionID());
			}
			
			out.println("<p>" + qr.getText() + "</p>");
			out.println("<ul style=\"list-style-type: none;\">");
			for (int j = 0; j < attempts.length; j++) {
				if (attempts != null) {
					out.println("<li><input type=\"text\" name=\"" + qr.getQuestionID() + "\" value=\"" + attempts[j] + "\" /></li>");
				}
				else {
					out.println("<li><input type=\"text\" name=\"" + qr.getQuestionID() + "\" /></li>");
				}
			}
			out.println("</ul>");
		}
		out.println("<br>");
		int pointsEarned = question.getPointsEarned();
		int totalPoints = question.getPossiblePoints();
		
		if (pointsEarned == totalPoints) {
			out.println("<p><span class=\"green\">Correct</span>! You earned all " + totalPoints + 
					" points on this question.</p>");
		}
		else {
			out.println("<p><span class=\"red\">Incorrect</span>. You earned " + pointsEarned + 
					" out of " + totalPoints + " points on this question.</p>");
			if (question.getType() == Question.QuestionType.FILL_IN_BLANK || question.getType() == Question.QuestionType.QUESTION_RESPONSE) {
				List<Answer> answers;
				if (question.getType() == Question.QuestionType.FILL_IN_BLANK) {
					answers = ((FillInBlank)question).getAnswers();
				}
				else {
					answers = ((QuestionResponse)question).getAnswers();
				}
	
				if (answers.size() == 1) {
					out.println("The correct answer is: ");
				}
				else {
					out.println("The correct answers are: ");
				}
				out.println("<ul>");
				for (int j = 0; j < answers.size(); j++) {
					out.println("<li>" + answers.get(j).getVariationsString() + "</li>");
				}
				out.println("</ul>");
			}
			else {
				MultipleChoice mc = (MultipleChoice)question;
				List<Choice> choices = mc.getChoices();
				if (mc.isSingleAnswer()) {
					out.println("The correct choice is: ");
				}
				else {
					out.println("The correct choices are: ");
				}
				
				out.println("<ul>");
				for (int j = 0; j < choices.size(); j++) {
					if (choices.get(j).isCorrect()) {
						out.println("<li>" + choices.get(j).getText() + "</li>");
					}
				}
				out.println("</ul>");
			}
		}
		out.println("</div>");
		out.println("<br><br>");
	}
%>
<a href="<%= QuizManager.getURL(q.getQuizID()) %>">Back To Quiz</a>
</div>
</body>
</html>