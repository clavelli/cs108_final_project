<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="model.*"%>
<%@ page import="utils.Helper"%>
<%@ page import="java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="style.css" />
<script type="text/javascript" src="js/script.js"></script> 
<title>Home</title>
</head>
<body>
<%= Helper.banner() %>

<div id="wrap">
<h3>Announcement text:</h3>
<form action="MakeAnnouncementServlet" method="post">
<textarea rows="8" cols="60" name="text"></textarea>
<button type="submit">Make Announcement</button>
</form>
</div>

</body>
</html>