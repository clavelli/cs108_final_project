<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="model.*" %>
<%@ page import="utils.Helper" %>
<%@ page import="java.util.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="style.css" />
<script type="text/javascript" src="js/script.js"></script> 
<%
	Quiz q = (Quiz)request.getSession().getAttribute("Quiz");
%>
<title><%= q.getTitle() %></title>
</head>
<body>
<%= Helper.banner() %>
<div id="wrap">
<h1><%= q.getTitle() %></h1>

<%
	Question question = q.getCurrent();
	out.println("<h3>Question " + question.getQuestionNumber() + "</h3>");
	out.println("<div class=\"box1\">");
	String pictureURL = question.getPictureURL();
	if (pictureURL != null) {
		out.println("<img src=\"" + pictureURL + "\" />");
	}
	
	if (question.getType() == Question.QuestionType.FILL_IN_BLANK) {
		FillInBlank f = (FillInBlank)question;
		String[] attempts = request.getParameterValues(f.getQuestionID());
		request.getSession().setAttribute(f.getQuestionID(), attempts);
		
		if (f.getText1() != null) {
			out.println("<p>" + f.getText1() + " ");
		}
		else {
			out.println("<p>");
		}
		for (int j = 0; j < attempts.length; j++) {
			if (attempts != null) {
				out.print("<input type=\"text\" name=\"" + f.getQuestionID() + "\" value=\"" + attempts[j] + "\"/>");
			}
			else {
				out.print("<input type=\"text\" name=\"" + f.getQuestionID() + "\"/>");
			}
			
			if (j != attempts.length - 1) {
				out.print(", ");
			}
		}
		if (f.getText2() != null) {
			out.println(" " + f.getText2() + "</p>");
		}
		else {
			out.println("</p>");
		}
		f.correct(attempts);
	}
	else if (question.getType() == Question.QuestionType.MULTIPLE_CHOICE) {
		MultipleChoice mc = (MultipleChoice)question;
		String qID = mc.getQuestionID();
		
		List<Integer> attempts = new LinkedList<Integer>();
		String[] responseValues = request.getParameterValues(qID);
		request.getSession().setAttribute(mc.getQuestionID(), responseValues);
		
		if (responseValues != null) {
			for (int j = 0; j < responseValues.length; j++) {
				attempts.add(Integer.parseInt(responseValues[j]));
			}
		}
		mc.correct(attempts);
		out.println("<p>" + mc.getText() + "</p>");
		List<Choice> choices = mc.getChoices();
		String inputType = "checkbox";
		if (mc.isSingleAnswer()) {
			inputType = "radio";
		}
		
		String[] values = (String[])request.getParameterValues(mc.getQuestionID());
		
		out.println("<ul style=\"list-style-type: none;\">");
		if (values != null) {
			List<String> valuesList = Arrays.asList(values);
			for (int j = 0; j < choices.size(); j++) {
				if (valuesList.contains(String.valueOf(j))) {
					out.println("<li><input type=\"" + inputType + "\" name=\"" + qID + "\" value=\"" 
						+ j + "\" checked/> " + choices.get(j).getText() + "</li>");
				}
				else {
					out.println("<li><input type=\"" + inputType + "\" name=\"" + qID + "\" value=\"" 
						+ j + "\" /> " + choices.get(j).getText() + "</li>");
				}
			}
		}
		else {
			for (int j = 0; j < choices.size(); j++) {
				out.println("<li><input type=\"" + inputType + "\" name=\"" + qID + "\" value=\"" 
					+ j + "\" /> " + choices.get(j).getText() + "</li>");
			}
		}
		out.println("</ul>");
	}
	else {
		QuestionResponse qr = (QuestionResponse)question;
		String[] attempts = request.getParameterValues(qr.getQuestionID());
		request.getSession().setAttribute(qr.getQuestionID(), attempts);
		
		out.println("<p>" + qr.getText() + "</p>");
		out.println("<ul style=\"list-style-type: none;\">");
		for (int j = 0; j < attempts.length; j++) {
			if (attempts != null) {
				out.println("<li><input type=\"text\" name=\"" + qr.getQuestionID() + "\" value=\"" + attempts[j] + "\" /></li>");
			}
			else {
				out.println("<li><input type=\"text\" name=\"" + qr.getQuestionID() + "\" /></li>");
			}
		}
		out.println("</ul>");
		qr.correct(attempts);
	}
	
	int pointsEarned = question.getPointsEarned();
	int totalPoints = question.getPossiblePoints();
	
	if (pointsEarned == totalPoints) {
		out.println("<p><span style=\"font-color:#2DA620\">Correct</span>! You earned all " + totalPoints + 
				" points on this question.</p>");
	}
	else {
		out.println("<p><span style=\"font-color:#D72D2D\">Incorrect</pan>. You earned " + pointsEarned + 
				" out of " + totalPoints + " points on this question.</p>");
		if (question.getType() == Question.QuestionType.FILL_IN_BLANK || question.getType() == Question.QuestionType.QUESTION_RESPONSE) {
			List<Answer> answers;
			if (question.getType() == Question.QuestionType.FILL_IN_BLANK) {
				answers = ((FillInBlank)question).getAnswers();
			}
			else {
				answers = ((QuestionResponse)question).getAnswers();
			}

			if (answers.size() == 1) {
				out.println("The correct answer is: ");
			}
			else {
				out.println("The correct answers are: ");
			}
			out.println("<ul>");
			for (int i = 0; i < answers.size(); i++) {
				out.println("<li>" + answers.get(i).getVariationsString() + "</li>");
			}
			out.println("</ul>");
		}
		else {
			MultipleChoice mc = (MultipleChoice)question;
			List<Choice> choices = mc.getChoices();
			if (mc.isSingleAnswer()) {
				out.println("The correct choice is: ");
			}
			else {
				out.println("The correct choices are: ");
			}
			
			out.println("<ul>");
			for (int i = 0; i < choices.size(); i++) {
				if (choices.get(i).isCorrect()) {
					out.println("<li>" + choices.get(i).getText() + "</li>");
				}
			}
			out.println("</ul>");
		}
	}
	out.println("</div>");
	
	if (q.isLastQuestion()) {
		q.finish();
		Double score = q.grade();
		request.getSession().setAttribute("Score", score);
		out.println("<br><form action=\"result.jsp\" method=\"post\"><input type=\"submit\" value=\"Finish\" /></form>");
	}
	else {
		q.nextQuestion();
		out.println("<br><form action=\"quiz-question.jsp\" method=\"post\"><input type=\"submit\" value=\"Next Question\" /></form>");
	}
%>
</div>
</body>
</html>